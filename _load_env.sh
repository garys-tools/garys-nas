# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
ENV_PATH=./conf/env.vars

if [ -f $ENV_PATH ]; then
  . $ENV_PATH
else
  echo "$ENV_PATH not found"
fi
