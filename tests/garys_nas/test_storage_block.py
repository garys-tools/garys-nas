# See fixture comments
# def test_st_dev__integration(mounted_storage_block):
#     """
#     As this test relies on a physical device for proper testing (otherwise
#     if we mock the stat function then the test is not really adding that
#     much value considering the simplicity of the property).
#
#     This test might if there are multiple mounts on the destination
#     directory (I have not added a feature of blocking mounted_directory
#     if it is mounted on top of)
#     """
#     maj_min = mounted_storage_block.maj_min
#     assert maj_min == mounted_storage_block.mounted_directory.maj_min
