# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from zoneinfo import ZoneInfo

from garys_nas.time_conversion import create_time_aware


def test_create_time_aware__normal():
    dt = create_time_aware(2021, 12, 28, 8, 0, 0, "Australia/Brisbane")
    assert "2021-12-28T08:00:00+10:00" == dt.isoformat()


def test_time_conversion__spot_check():
    """
    This is just to verify time conversion in python 3.9 which is where
    Django has dropped pytz in favor of the new ZoneInfo model.  Previously,
    time conversions would be unstable.
    """
    dt = create_time_aware(2021, 12, 28, 8, 0, 0, "UTC")
    assert "2021-12-28T08:00:00+00:00" == dt.isoformat()
    assert (
        "2021-12-28T18:00:00+10:00"
        == dt.astimezone(ZoneInfo("Australia/Brisbane")).isoformat()
    )
