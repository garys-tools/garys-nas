# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from unittest.mock import MagicMock, patch

import pytest

from garys_nas.models import StorageBlock, Directory
from nas_sensors import BlockNode, collect_data_nodes, BlockSubType


@pytest.fixture
def any_mounted_block_node() -> BlockNode:
    """
    Retrieves a random block node that has a disk mount that is not
    root (since this helps other tests).
    """
    for n in collect_data_nodes():
        if "/" in [x.name for x in n.mounts]:
            continue
        elif n.sub_type == BlockSubType.ext4 and n.mounts:
            return n


@pytest.fixture
def mounted_storage_block(any_mounted_block_node) -> StorageBlock:
    """
    This has gotten overly complicated with some recent refactoring.
    The related tests need to be broken into smaller units.
    """
    dirs = [
        (d.name, Directory(relative_path="/")) for d in any_mounted_block_node.mounts
    ]

    # Need to mock exists, first, count, and all
    directory_set_mock = MagicMock()
    directory_set_mock.exists.return_value = True
    directory_set_mock.first.return_value = dirs[0][1]
    directory_set_mock.get.return_value = dirs[0][1]
    directory_set_mock.count.return_value = len(dirs)
    directory_set_mock.all.return_value = [x[1] for x in dirs]

    os_paths_mock = MagicMock()
    os_paths_mock.all.return_value = [x[0] for x in dirs]

    with patch.object(StorageBlock, "directory_set", new=directory_set_mock) as _:
        with patch.object(Directory, "os_paths", new=os_paths_mock) as _:
            b = StorageBlock.create_from_block_node(
                node=any_mounted_block_node, present=True
            )
            for d in dirs:
                d[1].storage_block = b

            def get(**kwargs):
                nonlocal b
                if "maj_min" in kwargs:
                    if kwargs["maj_min"] != b.maj_min:
                        raise NotImplementedError
                return b

            object_manager_mock = MagicMock()
            object_manager_mock.get = get

            with patch.object(StorageBlock, "objects", new=object_manager_mock) as _:
                yield b
