# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
import pytest

from garys_nas.models import BackupStorage, BackupOperation, Directory
from garys_nas.models.backup_operation import TarBz2Operation, RsyncOperation


@pytest.fixture
def backup_operation(mounted_storage_block):
    backup_storage = BackupStorage(
        storage=mounted_storage_block,
        description="Fixture",
    )

    no_real_dir = Directory("/some/fake/dir")

    backup_op = BackupOperation(
        backup_storage=backup_storage,
        name="Fixture op",
        description="Op",
        source=no_real_dir,
        local_destination="/backup.tar.bz2",
    )
    return backup_op


# See mounted_storage_block fixture
# def test_as_path__normal(backup_operation):
#     """
#     Testing just the mounted directory.  This test is very close of
#     just replicated the logic being tests... not a good test at all.
#     I would prefer to hardcode but then this test will be dependent on
#     the pc this is running on...  so really, I should mock those interfaces
#     as this use case is not about integration (whereas the fixtures I am reusing
#     are intended as part of more detailed integration testing).
#     """
#     rel_local = backup_operation.local_destination.lstrip("/")
#     expected = (
#         backup_operation.backup_storage.storage.mounted_root_directory.os_path()
#         / rel_local
#     )
#     assert expected == backup_operation.as_path


def test_tar_bz2_filter_return__common_errors():
    # noinspection PyTypeChecker
    tar_bz2_with_common_errors = TarBz2Operation(None)
    tar_bz2_with_common_errors.stdout = ""
    # noinspection SpellCheckingInspection
    tar_bz2_with_common_errors.stderr = (
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/8: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/10: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/11: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/12: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/9266: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/9267: socket ignored\n"
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/9294: socket ignored\n"
        "tar: /var/lib/docker/volumes/zm-db/_data/ib_logfile0: file changed as we read it\n"
        "tar: /var/spool/postfix/private/tlsmgr: socket ignored\n"
        "tar: /var/spool/postfix/private/rewrite: socket ignored\n"
        "tar: /var/spool/postfix/private/bounce: socket ignored\n"
        "tar: /var/spool/postfix/private/defer: socket ignored\n"
        "tar: /var/spool/postfix/private/trace: socket ignored\n"
        "tar: /var/spool/postfix/private/verify: socket ignored\n"
        "tar: /var/spool/postfix/private/proxymap: socket ignored\n"
        "tar: /var/spool/postfix/private/proxywrite: socket ignored\n"
        "tar: /var/spool/postfix/private/smtp: socket ignored\n"
        "tar: /var/spool/postfix/private/relay: socket ignored\n"
        "tar: /var/spool/postfix/private/error: socket ignored\n"
        "tar: /var/spool/postfix/private/retry: socket ignored\n"
        "tar: /var/spool/postfix/private/discard: socket ignored\n"
        "tar: /var/spool/postfix/private/local: socket ignored\n"
        "tar: /var/spool/postfix/private/virtual: socket ignored\n"
        "tar: /var/spool/postfix/private/lmtp: socket ignored\n"
        "tar: /var/spool/postfix/private/anvil: socket ignored\n"
        "tar: /var/spool/postfix/private/scache: socket ignored\n"
        "tar: /var/spool/postfix/public/pickup: socket ignored\n"
        "tar: /var/spool/postfix/public/cleanup: socket ignored\n"
        "tar: /var/spool/postfix/public/qmgr: socket ignored\n"
        "tar: /var/spool/postfix/public/flush: socket ignored\n"
        "tar: /var/spool/postfix/public/showq: socket ignored\n"
        "tar: /var/spool/postfix/public/postlog: socket ignored\n"
    )
    assert 0 == tar_bz2_with_common_errors._filter_return(1)


def test_tar_bz2_filter_return__no_stderr():
    # noinspection PyTypeChecker
    tar_bz2_with_common_errors = TarBz2Operation(None)
    tar_bz2_with_common_errors.stdout = ""
    tar_bz2_with_common_errors.stderr = ""
    assert 1 == tar_bz2_with_common_errors._filter_return(1)


def test_tar_bz2_filter_return__bad_errors():
    # noinspection PyTypeChecker
    tar_bz2_with_common_errors = TarBz2Operation(None)
    tar_bz2_with_common_errors.stdout = ""
    # noinspection SpellCheckingInspection
    tar_bz2_with_common_errors.stderr = (
        "tar: /var/lib/docker/volumes/samba-lib/_data/private/msg.sock/9294: socket ignored\n"
        "tar: /var/lib/docker/volumes/zm-db/_data/ib_logfile0: file changed as we read it\n"
        "tar: /var/spool/postfix/private/tlsmgr: socket ignored\n"
        "tar: /var/spool/postfix/public/postlog: Unknown Error\n"
    )
    assert 1 == tar_bz2_with_common_errors._filter_return(1)


def test_rsync_filter_return__common_errors():
    # noinspection PyTypeChecker
    rsync_with_common_errors = RsyncOperation(None)
    rsync_with_common_errors.stdout = ""
    # noinspection SpellCheckingInspection
    rsync_with_common_errors.stderr = (
        'file has vanished: "<file_path>.pdf"\n'
        'file has vanished: "<file_path>.pdf"\n'
        'file has vanished: "<file_path>.pdf"\n'
        'file has vanished: "<file_path>.pdf"\n'
        "rsync warning: some files vanished before they could be "
        "transferred (code 24) at main.c(1189) [sender=3.1.3]\n"
    )
    assert 0 == rsync_with_common_errors._filter_return(24)


def test_rsync_filter_return__no_stderr():
    # noinspection PyTypeChecker
    rsync_with_common_errors = RsyncOperation(None)
    rsync_with_common_errors.stdout = ""
    rsync_with_common_errors.stderr = ""
    assert 1 == rsync_with_common_errors._filter_return(1)


def test_rsync_filter_return__bad_errors():
    # noinspection PyTypeChecker
    rsync_with_common_errors = RsyncOperation(None)
    rsync_with_common_errors.stdout = ""
    # noinspection SpellCheckingInspection
    rsync_with_common_errors.stderr = (
        'file has vanished: "<file_path>.pdf"\n'
        'file really bad error: "<file_path>.pdf"\n'
        'file has vanished: "<file_path>.pdf"\n'
        'file has vanished: "<file_path>.pdf"\n'
        "rsync warning: some files vanished before they could be "
        "transferred (code 24) at main.c(1189) [sender=3.1.3]\n"
    )
    assert 1 == rsync_with_common_errors._filter_return(1)
