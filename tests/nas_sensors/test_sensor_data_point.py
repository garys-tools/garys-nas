# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
import decimal

from nas_sensors.constants import Units
from nas_sensors.sensor_data_point import choose_size_units, bytes_as_units


def test_choose_size_units_normal_b():
    assert Units.B == choose_size_units(500)
    assert Units.B == choose_size_units(1000)
    assert Units.B == choose_size_units(1500)
    assert Units.B == choose_size_units(2000)


def test_choose_size_units_normal_kib():
    assert Units.KiB == choose_size_units(2500)
    assert Units.KiB == choose_size_units(1048576)
    assert Units.KiB == choose_size_units(2097151)


def test_choose_size_units_normal_mib():
    assert Units.MiB == choose_size_units(2500 * 1024)
    assert Units.MiB == choose_size_units(1048576 * 1024)
    assert Units.MiB == choose_size_units(2097151 * 1024)


def test_choose_size_units_normal_gib():
    assert Units.GiB == choose_size_units(2500 * 1024 * 1024)
    assert Units.GiB == choose_size_units(1048576 * 1024 * 1024)
    assert Units.GiB == choose_size_units(2097151 * 1024 * 1024)


def test_choose_size_units_normal_tib():
    assert Units.TiB == choose_size_units(2500 * 1024 * 1024 * 1024)


def test_bytes_as_units_normal_b():
    assert decimal.Decimal("1649267441664") == bytes_as_units(1649267441664, Units.B)


def test_bytes_as_units_normal_kib():
    assert decimal.Decimal("1610612736") == bytes_as_units(1649267441664, Units.KiB)


def test_bytes_as_units_normal_mib():
    assert decimal.Decimal("1572864") == bytes_as_units(1649267441664, Units.MiB)


def test_bytes_as_units_normal_gib():
    assert decimal.Decimal("1536") == bytes_as_units(1649267441664, Units.GiB)


def test_bytes_as_units_normal_tib():
    assert decimal.Decimal("1.5") == bytes_as_units(1649267441664, Units.TiB)
