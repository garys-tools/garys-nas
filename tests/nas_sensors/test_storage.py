import pathlib
import pytest

from nas_sensors.storage import udev_info


@pytest.fixture
def block_path() -> str:
    block_devices = list(
        filter(
            lambda x: x.name[:4] == "nvme" or x.name[:2] == "sd",
            pathlib.Path("/sys/block").glob("*"),
        )
    )

    return f"/dev/{block_devices[0].name}"


def test_udev_info__integration_spot_check(block_path):
    """
    Checks the performance of udev info.  This should be reasonably
    stable across multiple systems.
    """
    data = udev_info(block_path)
    assert "ID_PART_TABLE_TYPE" in data
    assert "ID_PART_TABLE_UUID" in data
