# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from nas_sensors.utilities import st_dev_to_maj_min


def test_st_dev_to_maj_min__spot_check():
    assert "8:33" == st_dev_to_maj_min(2081)
    assert "259:4" == st_dev_to_maj_min(66308)
    assert "253:0" == st_dev_to_maj_min(64768)
