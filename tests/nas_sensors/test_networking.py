# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from nas_sensors import is_tcp_port_open


def test_is_tcp_port_open__normal_integration():
    """
    This test does depend on the internet working, and so is an integration
    test of this component and external components.
    """
    assert is_tcp_port_open("www.google.com", 80) is True


def test_is_tcp_port_open__fails_integration():
    assert is_tcp_port_open("8.8.8.8", 80) is False
