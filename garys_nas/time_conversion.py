# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import datetime
import time

import django.utils.timezone
import zoneinfo

EPOCH = datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)
HOURS_24_IN_MS = 24 * 60 * 60 * 1000


def create_time_aware(
    year=0, month=0, day=0, hour=0, minute=0, second=0, zone_string="UTC"
):
    tz = zoneinfo.ZoneInfo(zone_string)
    dt = datetime.datetime(
        year=year,
        month=month,
        day=day,
        hour=hour,
        minute=minute,
        second=second,
        tzinfo=tz,
    )
    return dt


def now_epoch_ms() -> int:
    """
    Returns ms for the current time
    """
    return int(time.time() * 1000)


def now_datetime() -> datetime.datetime:
    """
    Returns ms for the current time
    """
    return epoch_ms_to_datetime(now_epoch_ms())


def datetime_to_epoch_ms(v: datetime.datetime) -> int:
    """
    Returns ms for the time aware datetime provided.
    """
    assert django.utils.timezone.is_aware(v)
    total_seconds = (v - EPOCH).total_seconds()
    return int(total_seconds * 1000)


def epoch_ms_to_datetime(epoch_ms: int) -> datetime.datetime:
    tz = django.utils.timezone.get_current_timezone()
    dt = EPOCH + datetime.timedelta(milliseconds=epoch_ms)
    return dt.astimezone(tz)


def datetime_to_string(v: datetime.datetime) -> str:
    """
    This must only be used for display purposes, not data transfer.  I'm
    really only adding this for expediency instead of interpreting the ms
    value in the front end (which needs an extension to Fields Schema)
    """
    assert django.utils.timezone.is_aware(v)
    if v.tzinfo.tzname(v):
        return v.strftime("%Y-%m-%d %H:%M:%S %Z")
    else:
        return v.strftime("%Y-%m-%d %H:%M:%S %z")
