import pathlib
import re


def _get_version():
    project_file = pathlib.Path(__file__).parent.parent / "pyproject.toml"
    with project_file.open("r") as f:
        content = f.read()
    version_match = next(
        re.finditer(r"version = \"(?P<v>.+)\"\n", content)
    ).groupdict()["v"]
    return version_match


__version__ = _get_version()
