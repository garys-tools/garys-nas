# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import signal

logger = logging.getLogger(__name__)

delete_models = []


# noinspection PyUnusedLocal
def clean_up(signum, frame):
    logger.info("Processing signal %s", signum)
    for m in delete_models:
        try:
            m.delete()
        except Exception as e:
            logger.error("Unable to delete model %s, %s", m, e)
    logger.info("Finished processing signal %s", signum)
    exit()


signal.signal(signal.SIGABRT, clean_up)
signal.signal(signal.SIGINT, clean_up)
signal.signal(signal.SIGSEGV, clean_up)
signal.signal(signal.SIGTERM, clean_up)
