# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import subprocess

from django.core.management.base import BaseCommand

from garys_nas.logic import sync_storage_blocks
from garys_nas.logic.repair_block_data import (
    update_uuids_by_serial_number,
    update_file_system_uuids,
    check_mixed_uuids,
)
from garys_nas.models import PhysicalDevice

logger = logging.getLogger(__name__)

FAMILY_KEY = "Model Family"
MODEL_KEY = "Device Model"
MODEL_KEY2 = "Model Number"
SERIAL_KEY = "Serial Number"


def validate_serial_numbers():
    """
    Some USB docks mask disk data.  This function will update and lock parameters
    """
    for p in PhysicalDevice.objects.filter(lock_values=False, block__present=True):
        dev_path = p.block.dev_path
        assert dev_path, f"Dev path for {p} is expected to exist"
        # noinspection SpellCheckingInspection
        ret = subprocess.run(
            ["smartctl", "-i", str(dev_path)], check=False, stdout=subprocess.PIPE
        )
        output = ret.stdout.decode()

        if ret.returncode == 0:
            ret_lines = output.split("\n")
            ret_data = {}
            for line in ret_lines:
                if ":" in line:
                    key, value = line.split(":", maxsplit=1)
                    ret_data[key.strip()] = value.strip()
                else:
                    continue
            if ret_data[SERIAL_KEY]:
                if FAMILY_KEY in ret_data:
                    p.model = f"{ret_data[FAMILY_KEY]}  {ret_data[MODEL_KEY]}"
                elif MODEL_KEY in ret_data:
                    p.model = ret_data[MODEL_KEY]
                else:
                    p.model = ret_data[MODEL_KEY2]
                p.serial_number = ret_data[SERIAL_KEY]
                p.lock_values = True
                p.save()
            else:
                pass
        else:
            logger.info(
                "Skipping serial number scan for %s due to return code %i",
                dev_path,
                ret.returncode,
            )


class Command(BaseCommand):
    help = (
        "This command must be run as a super user and will perform a detailed scan "
        "to extract accurate serial number, model, and uuid data."
    )

    def handle(self, *args, **options):
        check_mixed_uuids()
        update_uuids_by_serial_number()
        update_file_system_uuids()
        try:
            sync_storage_blocks()
        except AssertionError:
            # Sometimes inconsistencies require duplicate executions of this command
            sync_storage_blocks()
        validate_serial_numbers()
