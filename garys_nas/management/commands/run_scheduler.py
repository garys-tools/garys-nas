# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import schedule
import time
from django.core.management.base import BaseCommand

from garys_nas.events.cause_and_effect import (
    on_scheduler_poll,
    on_root_task_poll,
    on_fast_root_poll,
    on_add_device,
    on_rem_device,
)
from garys_nas.logic import sync_storage_blocks
from garys_nas.models import StorageBlock, PendingOperation
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


def trigger_add_remove_device():
    """
    This function is an internal trigger for adding or removing devices.
    This works through polling but ideally won't be needed if this app is
    integrated with udev.
    """
    logger.info("trigger_add_remove_device started")
    initial_absent_storage = set(
        StorageBlock.objects.filter(present=False).values_list("id", flat=True)
    )
    initial_present_storage = set(
        StorageBlock.objects.filter(present=True).values_list("id", flat=True)
    )

    sync_storage_blocks()

    # Trigger events
    final_present_storage = set(
        StorageBlock.objects.filter(present=True).values_list("id", flat=True)
    )
    final_absent_storage = set(
        StorageBlock.objects.filter(present=False).values_list("id", flat=True)
    )

    reconnected_storage = initial_absent_storage & final_present_storage
    removed_storage = initial_present_storage & final_absent_storage

    # for o in StorageBlock.objects.filter(id__in=reconnected_storage):
    if reconnected_storage:
        on_add_device()
    elif removed_storage:
        on_rem_device()
    logger.info("trigger_add_remove_device completed")


def fast_poll():
    """
    The fast poll looks for user initiated requests and executes them
    """
    logger.debug("fast_poll started")
    new_fast_user_requests = PendingOperation.objects.filter(
        operation_type__in=(
            PendingOperation.TYPE_EJECT,
            PendingOperation.TYPE_LOCK,
            PendingOperation.TYPE_UNLOCK,
        ),
        started_time__isnull=True,
        completed_time__isnull=True,
    )
    if new_fast_user_requests.exists():
        on_fast_root_poll()
    logger.debug("fast_poll completed")


class Command(BaseCommand):
    help = "Schedule various maintenance tasks and is intended to be run as a superuser"

    @may_need_root
    def handle(self, *args, **options):
        logger.info("Starting scheduler")
        # Frequent check of block devices
        schedule.every(5).minutes.do(trigger_add_remove_device)
        schedule.every(2).minutes.do(on_scheduler_poll)
        schedule.every(5).minutes.do(on_root_task_poll)
        schedule.every(3).seconds.do(fast_poll)

        while True:
            schedule.run_pending()
            time.sleep(1)
