# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from django.core.management.base import BaseCommand

from garys_nas.models import Directory, BackupOperation

logger = logging.getLogger(__name__)


def report_on_directory_sync():
    issues = False
    for d in Directory.objects.all():
        if not d.is_in_sync():
            logger.debug("Directory %s is not in sync.", d)
            issues = True

    if issues:
        print("Directory sync is not okay")
    else:
        print("Directory sync is okay")


def report_on_backup_validation():
    issues = False
    for b in BackupOperation.objects.all():
        try:
            b.validate_storage_required()
        except Exception as e:
            logger.debug("Backup %s is not availabl: %s", b, e)
            issues = True

    if issues:
        print("Issues exist with the backups")
    else:
        print("Issues exist with the backups")


class Command(BaseCommand):
    help = "This management command is used to report on system synchronisation"

    def handle(self, *args, **options):
        report_on_directory_sync()
        report_on_backup_validation()
