# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
from django.core.management.base import BaseCommand
from garys_nas.models import DBLock

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Clear Locks"

    def add_arguments(self, parser):
        parser.add_argument(
            "--backup",
            action="store_true",
        )

    def handle(self, *args, **options):
        if options["backup"]:
            DBLock.objects.filter(name=DBLock.LOCK_BACKUP).delete()
