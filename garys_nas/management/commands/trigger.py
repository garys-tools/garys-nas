# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
from django.core.management.base import BaseCommand

from garys_nas.events.cause_and_effect import (
    on_root_task_poll,
    on_add_device,
    on_rem_device,
    on_scheduler_poll,
)
from garys_nas.logic import (
    sync_storage_blocks,
    produce_due_backup_operations,
    create_pending_operation_for_backup,
    sync_directories,
    produce_auto_eject_operations,
)
from garys_nas.logic.clean_up_bad_removal import clean_up_bad_removal
from garys_nas.logic.consume_backup_operations import consume_backup_operations
from garys_nas.models import BackupOperation, BackupStorage

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Trigger an event or action within the application."

    def add_arguments(self, parser):
        parser.add_argument(
            "--sync_blocks",
            help="Synchronise storage blocks",
            action="store_true",
        )
        parser.add_argument(
            "--root_tasks",
            help="Execute all tasks that require root access",
            action="store_true",
        )
        parser.add_argument(
            "--add_device",
            help="Trigger the add device event",
            action="store_true",
        )
        parser.add_argument(
            "--rem_device",
            help="Trigger the remove device event",
            action="store_true",
        )
        parser.add_argument(
            "--force_backup",
            help="Execute all available backups irrespective of whether they are due",
            action="store_true",
        )
        parser.add_argument(
            "--produce_due_backup_operations",
            action="store_true",
        )
        parser.add_argument(
            "--produce_auto_eject_operations",
            action="store_true",
        )
        parser.add_argument(
            "--create_pending_operation_for_backup",
            action="store_true",
        )
        parser.add_argument(
            "--sync_directories",
            action="store_true",
        )
        parser.add_argument(
            "--on_scheduler_poll",
            action="store_true",
        )
        parser.add_argument(
            "--clean_up_bad_removal",
            action="store_true",
        )

    def handle(self, *args, **options):
        if options["sync_blocks"]:
            sync_storage_blocks()
        if options["root_tasks"]:
            on_root_task_poll()
        if options["add_device"]:
            on_add_device()
        if options["rem_device"]:
            on_rem_device()
        if options["force_backup"]:
            consume_backup_operations(force=True)
        if options["produce_due_backup_operations"]:
            produce_due_backup_operations()
        if options["create_pending_operation_for_backup"]:
            for b in BackupOperation.objects.all():
                create_pending_operation_for_backup(b)
        if options["produce_auto_eject_operations"]:
            for b in BackupStorage.objects.all():
                produce_auto_eject_operations(b)
        if options["sync_directories"]:
            sync_directories()
        if options["on_scheduler_poll"]:
            on_scheduler_poll()
        if options["clean_up_bad_removal"]:
            clean_up_bad_removal()
