# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from django.db import models


class NullableCharField(models.CharField):
    empty_values = ("", [], (), {})

    def clean(self, value, model_instance):
        """
        modified the parent clean code to support null values so that
        NULl and blank can be treated independently.
        """
        value = self.to_python(value)
        self.validate(value, model_instance)
        self.run_validators(value or "")
        return value
