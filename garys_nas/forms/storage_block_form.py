# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from django import forms
from django.core.exceptions import ValidationError

from garys_nas.events.cause_and_effect import (
    on_user_request_format,
    on_user_eject_device,
    on_user_lock_device,
    on_user_unlock_device,
)
from garys_nas.models import (
    StorageBlock,
)
from nas_actions import StorageTemplate


class StorageBlockForm(forms.ModelForm):
    """
    This form was intended to format devices, but I have realised that
    root access is needed.  Instead, it will apply a flag for formatting
    to be processed by the storage_operation class
    """

    format_device = forms.ChoiceField(
        required=False,
        choices=(
            ("", "--------"),
            (
                StorageTemplate.TEMPLATE_FLAT_EXT4,
                "Format as an ext4 device",
            ),
            (
                StorageTemplate.TEMPLATE_FLAT_LUKS,
                "Format as an encrypted ext4 device",
            ),
            (
                StorageTemplate.TEMPLATE_LUKS_PARTITION,
                "Format partition with luks -> ext4",
            ),
        ),
        help_text="Each template requires device labels to be applied.  See template docs.",
    )

    device_labels = forms.CharField(
        required=False, max_length=1000, widget=forms.Textarea
    )

    eject_device = forms.BooleanField(
        required=False,
        help_text="A dirt hack to trigger an eject operation",
    )

    lock_device = forms.BooleanField(
        required=False,
        help_text="A dirt hack to trigger a lock operation",
    )

    unlock_device = forms.BooleanField(
        required=False,
        help_text="A dirt hack to trigger an unlock operation",
    )

    class Meta:
        model = StorageBlock
        fields = [
            # "file_system_uuid",
            # "partition_uuid",
            "root",
            "parents",
            "name",
            "kernel_name",
            "maj_min",
            # "label",
            "type",
            "sub_type",
            "detected_errors",
            "auto_mount",
            "auto_mount_location",
        ]
        widgets = {
            # "file_system_uuid": forms.TextInput(attrs={"readonly": "readonly"}),
            # "partition_uuid": forms.TextInput(attrs={"readonly": "readonly"}),
            "parents": forms.SelectMultiple(attrs={"disabled": "disabled"}),
            "name": forms.TextInput(attrs={"readonly": "readonly"}),
            "kernel_name": forms.TextInput(attrs={"readonly": "readonly"}),
            "maj_min": forms.TextInput(attrs={"readonly": "readonly"}),
            # "label": forms.TextInput(attrs={"readonly": "readonly"}),
            "type": forms.TextInput(attrs={"readonly": "readonly"}),
            "sub_type": forms.TextInput(attrs={"readonly": "readonly"}),
        }

    def clean(self):
        cleaned_data = super().clean()
        v = cleaned_data["format_device"]
        if v and not self.instance.available_to_format:
            raise ValidationError("The device is not available to format")
        elif v and not self.cleaned_data["device_labels"]:
            raise ValidationError(
                {"device_labels": "Device labels also need to be specified"}
            )
        return cleaned_data

    def clean_parent(self):
        """
        Because the parent field is a disabled field, it gets reset
        This is a band-aid to that problem.  This should be avoided.
        """
        return self.instance.parents.all()

    def save(self, commit=True):
        ret = super().save(commit=commit)

        format_selection = self.cleaned_data["format_device"]
        dev_labels = self.cleaned_data["device_labels"]

        # A format operation needs to be raised with the correct
        if format_selection:
            on_user_request_format(
                format_selection=format_selection,
                device_labels=dev_labels,
                storage_block=self.instance,
            )
        if self.cleaned_data["eject_device"]:
            on_user_eject_device(self.instance)
        if self.cleaned_data["lock_device"]:
            on_user_lock_device(self.instance)
        if self.cleaned_data["unlock_device"]:
            on_user_unlock_device(self.instance)

        return ret
