# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import abc
import datetime
import logging
import pathlib
import subprocess
from typing import Dict, Type, Optional

from django.db import models

from garys_nas.events.signals import (
    backup_operation_created,
)
from garys_nas.models.backup_storage import BackupStorage
from garys_nas.models.directory import Directory, get_one_file_system_listing
from garys_nas.time_conversion import now_epoch_ms, epoch_ms_to_datetime, HOURS_24_IN_MS
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


class BaseOperation(abc.ABC):
    command = ""
    estimate_compression_factor: Optional[float] = None

    def __init__(self, operation: "BackupOperation"):
        self.operation = operation
        self.start_time_ms = 0
        self.duration_ms = 0
        self.transfer_size = 0
        self.backup_disk_usage = 0
        self.stderr = ""
        self.stdout = ""
        self.ret_code = None

    @may_need_root
    def pre_backup_checks(self):
        """
        Child classes can override this to perform pre-backup checks
        """
        pass

    # noinspection PyMethodMayBeStatic
    def _filter_return(self, ret_code: int) -> int:
        """
        Child classes can override this to alter return codes.
        self.stdout and self.stderr are populated.
        """
        return ret_code

    @may_need_root
    def _execute_backup(self):
        """
        Performs the actual backup and update self.ret, self.stderr and stdout.
        """
        if not self.command:
            raise NotImplementedError(f"Class for {self} has not specified a command")
        cmd = [
            x
            % {
                "dest": str(self.operation.as_path),
                "src": str(self.operation.source.os_path().as_path),
            }
            for x in self.command.split(" ")
        ]

        results = subprocess.run(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        self.stderr = results.stderr.decode()
        self.stdout = results.stdout.decode()
        self.ret_code = self._filter_return(results.returncode)

    @may_need_root
    def _estimate_destination_size(self) -> int:
        """
        Calculates disk usage by the backup once completed.  The default
        behaviour is similar to
        """
        manifest = get_one_file_system_listing(self.operation.as_path)
        total_fs_usage = sum(x[4] for x in manifest)
        return total_fs_usage

    @abc.abstractmethod
    def _estimate_transfer_size(self) -> int:
        """
        How much data was transferred by the operation.  Some tools,
        like rsync, will report this.
        """

    @may_need_root
    def execute_backup(self):
        self.pre_backup_checks()
        t1 = now_epoch_ms()
        self._execute_backup()
        t2 = now_epoch_ms()
        self.start_time_ms = t1
        self.duration_ms = t2 - t1
        self.transfer_size = self._estimate_transfer_size()
        self.backup_disk_usage = self._estimate_destination_size()


class RsyncOperation(BaseOperation):

    command = "rsync -ax --delete --delete-after --stats %(src)s %(dest)s"

    @may_need_root
    def pre_backup_checks(self):
        dest = self.operation.as_path
        if not dest.exists():
            logger.info("Creating directory %s for rsync backup", dest)
            dest.mkdir(parents=True)

    def _estimate_transfer_size(self) -> int:
        if self.stdout:
            for row in self.stdout.split("\n"):
                if row.startswith("Total bytes sent: "):
                    return int(row[18:].replace(",", ""))
        else:
            return 0

    def _filter_return(self, ret_code: int) -> int:
        """
        Ignore the return code if there are only minor errors.
        """
        minor_errors = []
        major_errors = []

        file_vanished_text = "file has vanished"
        summary_line = "rsync warning:"

        for line in self.stderr.split("\n"):
            line = line.strip()
            if not line:
                continue
            elif line[: len(file_vanished_text)] == file_vanished_text:
                minor_errors.append(line)
            elif line[: len(summary_line)] == summary_line:
                pass
            else:
                major_errors.append(line)

        if not minor_errors and not major_errors:
            return ret_code
        elif not major_errors:
            return 0
        else:
            return ret_code


class TarBz2Operation(BaseOperation):

    command = "tar --one-file-system -cjPf %(dest)s %(src)s"

    estimate_compression_factor = (
        0.8  # This is just a guess but will always be based on disk usage
    )

    @may_need_root
    def pre_backup_checks(self):
        dest = self.operation.as_path
        if not dest.parent.exists():
            logger.info("Creating directory %s for tar backup", dest.parent)
            dest.parent.mkdir(parents=True)

    @may_need_root
    def _estimate_destination_size(self) -> int:
        p = self.operation.as_path
        if p.exists():
            blocks = self.operation.as_path.stat().st_blocks
            return blocks * 512
        else:
            return 0

    @may_need_root
    def _estimate_transfer_size(self) -> int:
        return self._estimate_destination_size()

    def _filter_return(self, ret_code: int) -> int:
        """
        Ignore the return code if there are only minor errors.
        """
        minor_errors = []
        major_errors = []

        socket_ignored_text = "socket ignored"
        socket_ignored_pos = -1 * len(socket_ignored_text)
        file_changed_text = "file changed as we read it"
        file_changed_pos = -1 * len(file_changed_text)

        for line in self.stderr.split("\n"):
            if not line:
                continue
            elif line[socket_ignored_pos:] == socket_ignored_text:
                minor_errors.append(line)
            elif line[file_changed_pos:] == file_changed_text:
                minor_errors.append(line)
            else:
                major_errors.append(line)

        if not minor_errors and not major_errors:
            return ret_code
        elif not major_errors:
            return 0
        else:
            return ret_code


class BackupOperation(models.Model):
    """
    The scope of this model is to manage a single backup operation.
    """

    # Constants
    TYPE_RSYNC = "rsync"
    TYPE_TAR_BZ2 = "tar_bz2"
    OPERATION_TYPES = ((TYPE_RSYNC, TYPE_RSYNC), (TYPE_TAR_BZ2, TYPE_TAR_BZ2))
    TYPE_OP_MAP: Dict[str, Type[BaseOperation]] = {
        TYPE_RSYNC: RsyncOperation,
        TYPE_TAR_BZ2: TarBz2Operation,
    }

    # Main Fields
    backup_storage = models.ForeignKey(BackupStorage, on_delete=models.PROTECT)
    enabled = models.BooleanField(
        default=False,
        help_text="Indicates the backup is enabled, this"
        "can be automatically disabled on errors.",
    )
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=128)

    source = models.ForeignKey(Directory, on_delete=models.PROTECT)
    local_destination = models.CharField(
        max_length=128,
        help_text="This is a path relative to the root of the storage device.",
    )
    type = models.CharField(max_length=16, choices=OPERATION_TYPES)

    min_time_between_backups_ms = models.PositiveBigIntegerField(
        default=HOURS_24_IN_MS,
        help_text="The minimum time before retaking a backup in milliseconds.",
    )

    # Status Fields
    current_backup_start_time_ms = models.PositiveBigIntegerField(
        default=None,
        help_text="The start time of the current backup, None otherwise.",
        null=True,
        blank=True,
    )

    last_run_time_ms = models.PositiveBigIntegerField(
        default=0, help_text="The time of the last backup."
    )
    last_run_duration_ms = models.PositiveBigIntegerField(
        default=0, help_text="The duration of the last backup"
    )
    last_transfer_size = models.PositiveBigIntegerField(
        default=0, help_text="The amount of data transferred on the last backup"
    )
    last_backup_disk_usage = models.PositiveBigIntegerField(
        default=0,
        help_text="The amount of space used on the destination device from the last backup.",
    )
    last_time_since_backup_ms = models.PositiveBigIntegerField(
        default=0, help_text="Last time between backups"
    )
    last_stdout = models.TextField(null=True, blank=True)
    last_stderr = models.TextField(null=True, blank=True)
    last_ret_code = models.IntegerField(null=True, blank=True)

    # Behaviour Flags
    disable_on_error = models.BooleanField(
        default=True, help_text="Disables the backup if an error occurs."
    )

    def __str__(self):
        return f"{self.backup_storage} / {self.name}"

    @property
    def as_path(self) -> pathlib.Path:
        """
        The path to the backup
        """
        return (
            self.backup_storage.storage.mounted_root_directory.os_path().as_path
            / self.local_destination.lstrip("/")
        )

    @property
    def current_backup_duration_s(self) -> Optional[int]:
        """
        For the admin
        """
        if self.current_backup_start_time_ms:
            return (now_epoch_ms() - self.current_backup_start_time_ms) // 1000
        else:
            return None

    @property
    def last_run_duration_h_m_s(self) -> Optional[str]:
        """
        For the admin
        """
        if self.last_run_duration_ms:
            t = datetime.timedelta(milliseconds=self.last_run_duration_ms)
            return str(t)
        else:
            return None

    @property
    def last_run_time_dt(self) -> Optional[datetime.datetime]:
        """
        For the admin
        """
        if self.last_run_time_ms:
            return epoch_ms_to_datetime(self.last_run_time_ms)
        else:
            return None

    @property
    def next_run_time_dt(self) -> Optional[datetime.datetime]:
        """
        For the admin
        """
        if self.last_run_time_dt and self.min_time_between_backups_ms:
            return self.last_run_time_dt + datetime.timedelta(
                milliseconds=self.min_time_between_backups_ms
            )
        else:
            return None

    def estimate_storage_free_space(self) -> int:
        """
        This calculates the amount of free space expected on the destination
        storage not including the last backup.
        """
        storage_available_space = self.backup_storage.storage.free_space_estimate
        if storage_available_space is None:
            raise ValueError(
                "Unable to perform storage estimate, t"
                "he storage device has not been mounted and scanned before"
            )
        used_space = self.last_backup_disk_usage
        return min(
            storage_available_space + used_space, self.backup_storage.storage.size_bytes
        )

    def validate_storage_required(self):
        """
        Raises a runtime error if there is insufficient space or if the backup
        and destination folders are not recorded as mounted in the db.

        .. NOTE::
            The backup storage will be mounted if it is not already so during
            this validation.
        """
        operation = self.TYPE_OP_MAP[self.type](self)
        avail = self.estimate_storage_free_space()
        if operation.estimate_compression_factor:
            required = self.source.disk_usage
        elif (
            self.source.storage_block.is_sparse
            and self.backup_storage.storage.is_sparse
        ):
            required = self.source.disk_usage
        else:
            required = self.source.file_size

        if operation.estimate_compression_factor:
            required *= operation.estimate_compression_factor

        logger.info(
            "validate_storage_required requires %0.2f GiB and there is %0.2f GiB available",
            required / 1024**3,
            avail / 1024**3,
        )

        if required >= avail:
            raise RuntimeError(f"Not enough space for operation")

        if not self.backup_storage.storage.present:
            raise RuntimeError(f"The storage device is not present")

        if not self.source.os_path():
            raise RuntimeError(f"The storage device is not mounted or accessible")
        elif not self.source.os_path().as_path.exists():
            raise RuntimeError(f"The path '{self.source.os_path()}' does not exist")

        if not self.source.is_in_sync():
            raise RuntimeError(f"The storage device is not synchronised correctly")

        if not self.backup_storage.storage.mounted_root_directory:
            self.backup_storage.storage.mount()
            if not self.backup_storage.storage.mounted_root_directory:
                raise RuntimeError(f"The backup device can not be reached")
        elif not self.backup_storage.storage.mounted_root_directory.is_in_sync():
            raise RuntimeError(f"The backup device is not synchronised correctly")

    def clean(self):
        if self.type == self.TYPE_TAR_BZ2 and not self.local_destination.endswith(
            ".tar.bz2"
        ):
            self.local_destination += ".tar.bz2"

        if self.type == self.TYPE_RSYNC and self.local_destination.endswith(".tar.bz2"):
            self.local_destination = self.local_destination[:-8]

    @may_need_root
    def run(self):
        """Run the backup"""
        logger.info("Starting backup for %s", self)
        operation = self.TYPE_OP_MAP[self.type](self)
        try:
            self.validate_storage_required()
        except Exception as e:
            self.last_stderr = f"Error validating storage space: {e}"
            self.save()
            raise RuntimeError(str(e))
        self.current_backup_start_time_ms = now_epoch_ms()
        self.save()

        # While files can change during the operation, the safest approach
        # is to assume the file state at the start of the operation as this
        # is more likely to increase demand on a future backup.
        operation.execute_backup()
        self.last_time_since_backup_ms = operation.start_time_ms - self.last_run_time_ms
        self.last_run_time_ms = operation.start_time_ms
        self.last_run_duration_ms = operation.duration_ms
        self.last_transfer_size = operation.transfer_size
        self.last_backup_disk_usage = operation.backup_disk_usage
        self.last_stderr = operation.stderr
        self.last_stdout = operation.stdout
        self.last_ret_code = operation.ret_code
        self.current_backup_start_time_ms = None
        self.save()
        logger.info("Backup done for %s", self)

    def save(self, *args, **kwargs):
        new_model = self._state.adding
        self.full_clean()
        super().save(*args, **kwargs)
        if new_model:
            backup_operation_created.send(self)
