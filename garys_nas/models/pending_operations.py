# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import contextlib
import datetime
import json
import logging
from typing import Optional

from django.db import models

from garys_nas.time_conversion import datetime_to_epoch_ms, now_datetime


logger = logging.getLogger(__name__)


class CancelOperationException(Exception):
    """
    This exception, when raised, will close out or cancel the exception
    """


class OperationRetryException(Exception):
    """
    This exception, when raised, indicates it failed but will retry later
    """


class PendingOperation(models.Model):
    """
    This is a Task, but is not using the word Task to avoid ambiguity in asyncio and others.
    This models essentially acts as a communication queue between the
    web app and background tasks to indicated pending operations.  This
    is more explicit than obscurely placing data in models to trigger
    operations.
    """

    TYPE_BACKUP = "backup"
    TYPE_DIRECTORY_SCAN = "directory_scan"
    TYPE_EJECT = "eject"
    TYPE_LOCK = "lock"
    TYPE_UNLOCK = "unlock"
    TYPE_FORMAT = "format"
    TYPE_INIT_BLOCK = "init_storage_block"

    operation_type = models.CharField(
        max_length=32,
        choices=(
            (TYPE_BACKUP, "Backup"),
            (TYPE_FORMAT, "Format Device"),
            (TYPE_EJECT, "Eject Device"),
            (TYPE_LOCK, "Lock Device"),
            (TYPE_UNLOCK, "Unlock Device"),
            (TYPE_INIT_BLOCK, "Initialise the storage block"),
            (TYPE_DIRECTORY_SCAN, "Directory Scan"),
        ),
    )

    operation_data = models.TextField(blank=True, null=True)

    create_time = models.DateTimeField(auto_now_add=True)
    started_time = models.DateTimeField(blank=True, null=True)
    completed_time = models.DateTimeField(blank=True, null=True)
    completed_time_ms = models.PositiveBigIntegerField(
        default=0,
        help_text="While this correlates with completed_time, "
        "it is used for internal calculations.",
    )

    # Related Objects
    related_backup_operation = models.ForeignKey(
        "BackupOperation", on_delete=models.SET_NULL, blank=True, null=True
    )
    related_storage_block = models.ForeignKey(
        "StorageBlock", on_delete=models.SET_NULL, blank=True, null=True
    )
    related_directory = models.ForeignKey(
        "Directory", on_delete=models.SET_NULL, blank=True, null=True
    )

    @property
    def state(self) -> str:
        if self.completed_time:
            return "Completed"
        elif self.started_time:
            return "Started"
        else:
            return "Created"

    @property
    def elapsed_time(self) -> Optional[datetime.timedelta]:
        if self.completed_time:
            return self.completed_time - self.started_time
        else:
            return None

    def __str__(self):
        parts = [self.operation_type]
        if self.completed_time:
            parts.append(f"Completed [{self.completed_time}]")
        elif self.started_time:
            parts.append(f"Started [{self.started_time}]")
        else:
            parts.append(f"Created [{self.create_time}]")

        if self.related_storage_block:
            parts.append(f"Storage [{self.related_storage_block}]")

        if self.related_backup_operation:
            parts.append(f"Backup [{self.related_backup_operation}]")

        return f"<PendingOperation {' '.join(parts)}>"

    def load_operation_data(self) -> dict:
        if self.operation_data:
            return json.loads(self.operation_data)
        else:
            return {}

    def dump_operation_data(self, data: dict):
        self.operation_data = json.dumps(data)

    def save(self, *args, **kwargs):
        self.full_clean()
        if self.completed_time:
            ms = datetime_to_epoch_ms(self.completed_time)
            self.completed_time_ms = ms
        super().save(*args, **kwargs)
        return self

    @contextlib.contextmanager
    def executing(self, raise_exceptions=True):
        """
        This is a context manager that will update the save times of
        the operation as required or remove them if an exception is raised.
        """
        self.started_time = now_datetime()
        try:
            yield
            self.completed_time = now_datetime()
            if self.operation_data:
                d = self.load_operation_data()
                if "error" in d:
                    del d["error"]
                    self.dump_operation_data(d)
        except (OperationRetryException, CancelOperationException, Exception) as e:
            d = self.load_operation_data()
            d["error"] = str(e)
            self.dump_operation_data(d)
            if isinstance(e, CancelOperationException):
                self.completed_time = now_datetime()
            else:
                self.started_time = None
            if raise_exceptions:
                raise
            elif isinstance(e, OperationRetryException):
                logger.warning(
                    "Pending Operation %s failed to run with message %s.  See the operation data"
                    "for more information.",
                    self,
                    e,
                )
            else:
                logger.error(
                    "Pending Operation %s failed to run with message %s.  See the operation data"
                    "for more information.",
                    self,
                    e,
                    exc_info=True,
                )
        finally:
            self.save()
