# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from typing import Iterator

from django.db import models

from garys_nas.events.signals import backup_storage_updated
from garys_nas.models.storage_block import StorageBlock


class BackupStorage(models.Model):
    """
    The scope of this model is to manage the storage device that is
    used to store backup data.
    """

    storage = models.OneToOneField(StorageBlock, on_delete=models.PROTECT)
    description = models.CharField(max_length=128)

    auto_eject = models.BooleanField(
        default=False,
        help_text="Auto eject the backup storage once all backup operations have completed.",
    )
    backup_on_insertion = models.BooleanField(
        default=True,
        help_text="Backup operations are run on insertion of this device.",
    )
    pause_backup = models.BooleanField(
        default=False,
        help_text="Prevent backups from executing on this storage.",
    )

    def __str__(self):
        return str(self.storage)

    @classmethod
    def get_available_storage_devices(cls) -> Iterator["BackupStorage"]:
        """
        This yields all BackupStorage devices that are currently
        connected and available.
        """
        for x in cls.objects.filter(storage__present=True):
            yield x

    def save(self, *args, **kwargs):
        new_model = self._state.adding
        self.full_clean()
        super().save(*args, **kwargs)
        if new_model is False:
            backup_storage_updated.send(self)
