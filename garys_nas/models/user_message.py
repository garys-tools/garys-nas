"""
This does not do anything at the moment apart from log messages that,
in the future, will need to be presented to a user via the UI.

Having this function helps identify code that is needed for
raising to users awareness.

The django messages framework is not useful here as it requires a
request object and some of these messages are raised in locations where
a request object is not available.
"""
import logging
from typing import NoReturn

logger = logging.getLogger(__name__)


def error_message(*, msg: str) -> NoReturn:
    """
    All messages will need to be globally unique, however, the python
    inspect module can access the stack for this function call and generate
    a unique ID.  This can then capture repeating messages.
    """
    logger.error(msg)
