# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import contextlib
import logging
import os
import pathlib
import random
import time

from typing import Optional, TYPE_CHECKING, List, Any, Generator

import django.db.utils
from django.db import models
from django.db.models import Q

from garys_nas.events.signals import (
    storage_block_created,
    storage_block_reappeared,
    system_block_change,
)
from garys_nas.forms import NullableCharField
from nas_actions import (
    mapper_mount_path,
    open_storage,
    mount,
    umount,
    close_storage,
    apply_storage_format,
    eject,
    storage_requires_key,
)
from nas_sensors import BlockNode, BlockType, may_need_root, BlockSubType
import garys_nas.models

if TYPE_CHECKING:
    from garys_nas.models.directory import Directory

logger = logging.getLogger(__name__)


class StorageBlock(models.Model):
    """
    This model represents block devices relevant to system storage

    See BlockNode for a description of the fields.
    """

    EXCLUDE_BLOCK_TYPES = (BlockType.virtual,)

    # Attributes calculated from BlockNode
    file_system_uuid = NullableCharField(
        max_length=255,
        null=True,
        unique=False,
        help_text="This is the file system UUID or a volume uuid.  For MD it is repeated for each "
        "node that is part of the set.",
    )
    partition_uuid = NullableCharField(
        max_length=255,
        null=True,
        unique=True,
        help_text="This is the UUID that refers to the partition in the partition table",
    )

    root = models.BooleanField(
        default=False, help_text="This is a convenience field for queries"
    )
    parents = models.ManyToManyField(
        "StorageBlock",
        blank=True,
        related_name="children_set",
    )
    name = models.CharField(max_length=64, unique=True)
    kernel_name = models.CharField(max_length=64, unique=True)
    maj_min = models.CharField(max_length=64, unique=True)
    label = NullableCharField(
        max_length=128,
        unique=False,
        null=True,
    )  # This does not have to be unique!
    type = models.CharField(max_length=32)
    sub_type = models.CharField(max_length=32)
    size_bytes = models.PositiveBigIntegerField()

    # State
    last_scan_time_ms = models.PositiveBigIntegerField()
    last_present_time_ms = models.PositiveBigIntegerField()
    present = models.BooleanField(
        help_text="Indicates that on the most recent scan, the node was present."
    )

    # Attributes calculated separately
    file_system = models.CharField(max_length=16, default=None, null=True, blank=True)
    is_sparse = models.BooleanField(default=None, null=True, blank=True)

    # User information reporting
    detected_errors = models.TextField(null=True, blank=True)

    # User Configuration
    auto_mount_location = models.CharField(
        default="",
        max_length=128,
        blank=True,
        help_text="Auto mount the storage device to the given location.  "
        "Leave blank for no auto mount",
    )
    auto_mount = models.BooleanField(
        default=False,
        help_text="Automatically unlock/open/mounts the storage upon detection and availability.",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._last_present = self.present

    def __str__(self):
        if self.label:
            return f"{self.kernel_name} ({self.label})"
        else:
            return f"{self.kernel_name} ({self.name})"

    def set_absent(self):
        """
        Reset/clear unique OS fields so that they can be used by another
        device
        """
        self.present = False
        temp_unique_string = f"absent:{self.id}"
        self.name = temp_unique_string
        self.kernel_name = temp_unique_string
        self.maj_min = temp_unique_string
        self.save()

    @property
    def tree_roots(self) -> Generator["StorageBlock", None, None]:
        """
        Find the all the roots that lead to this node
        """
        if not self.parents.exists():
            yield self
        else:
            for p in self.parents.all():
                is_root_p = p.parents.count() == 0
                if is_root_p:
                    yield p
                else:
                    for other_p in p.tree_roots:
                        yield other_p

    @property
    def physically_present(self) -> bool:
        """
        Indicates the physical presence of this device based on the
        physical device being connected even if this block is not
        logically available.
        """
        return all(map(lambda x: x.present, self.tree_roots))

    @property
    def descendents(self) -> Generator["StorageBlock", None, None]:
        """
        Return all unique descendents of this node
        """
        all_nodes = set()
        for c in self.children_set.all():
            if c.id not in all_nodes:
                all_nodes.add(c.id)
                yield c
                for gc in c.descendents:
                    if gc.id not in all_nodes:
                        all_nodes.add(gc.id)
                        yield gc

    @property
    def connected_nodes(self) -> Generator["StorageBlock", None, None]:
        """
        Find all nodes connected to this including siblings.  If this
        is started at a root node then not all root nodes will be
        discovered.  There may be better algorithms out there, for now,
        this just gets the last descendant and starts from there.
        """
        all_nodes = set()
        d = self
        for d in self.descendents:
            pass
        for root in d.tree_roots:
            all_nodes.add(root.id)
            yield root
            for d in root.descendents:
                if d.id not in all_nodes:
                    all_nodes.add(d.id)
                    yield d

    @property
    def tree_roots_admin(self):
        """
        Used in Admin for testing
        """
        return list(self.tree_roots)

    @property
    def connected_nodes_admin(self):
        """
        Used in Admin for testing
        """
        return list(self.connected_nodes)

    @property
    def children_names(self) -> str:
        """Used in the admin"""
        if self.children_set.count() > 0:
            return ", ".join([x.name for x in self.children_set.all()])
        else:
            return ""

    @property
    def parent_names(self) -> str:
        """Used in the admin"""
        if self.parents.count() > 0:
            return ", ".join([x.name for x in self.parents.all()])
        else:
            return ""

    @property
    def mounted_root_directory(self) -> Optional["Directory"]:
        """
        For most measurements, a single mounted directory is all that
        is needed.  If this is mounted multiple times it will return
        the directory that is accessible in case another storage block
        is mounted on top of this one.
        """
        if self.present and self.directory_set.exists():
            root_dir = self.directory_set.get(relative_path="/")
            if root_dir.os_path():
                return root_dir
            else:
                return None
        else:
            return None

    @property
    def available_to_format(self) -> bool:
        """
        Indicates if this device is available for reformatting.  Only
        empty root devices or unmounted partitions are available
        """
        if self.root:
            return (
                self.present
                and self.parents.count() == 0
                and not self.children_set.exists()
            )
        else:
            return (
                self.present
                and not self.children_set.exists()
                and not self.mounted_root_directory
            )

    @property
    def dev_path(self) -> pathlib.Path:
        """
        A path to the device.
        :raises ValueError:  If the device does not exist
        """
        if self.present:
            p = pathlib.Path(f"/dev/{self.kernel_name}")
            if not p.exists():
                raise ValueError(f"Device path {p} does not exist")
            else:
                return p
        else:
            raise ValueError(f"Device is not mounted")

    @property
    def root_directory(self) -> "Directory":
        """
        Returns a copy of the root directory or raises an exception if
        one does not exist.  One should exist and will be created by
        sync_storage_blocks.
        """
        return self.directory_set.get(relative_path="/")

    @property
    def disk_usage_estimate(self) -> Optional[int]:
        """Returns an estimate of disk usage"""
        d = self.root_directory
        return d.disk_usage

    @property
    def reserved_space_estimate(self) -> Optional[int]:
        """
        Returns an estimate of how much space is required by files,
        which can be larger than the disk space for filesystems
        supporting sparse files.
        """
        d = self.root_directory
        return d.file_size

    @property
    def free_space_estimate(self) -> Optional[int]:
        """Returns an estimate of unused space on the block device"""
        if self.disk_usage_estimate is not None:
            return self.size_bytes - self.disk_usage_estimate
        else:
            return None

    @property
    def percent_used(self) -> Optional[int]:
        """Used in the admin - this assumes all devices are mounted"""
        v = self.disk_usage_estimate
        if v is not None:
            return v * 100 // self.size_bytes
        else:
            return None

    @property
    def percent_reserved(self) -> Optional[int]:
        """Used in the admin - this assumes all devices are mounted"""
        v = self.reserved_space_estimate
        if v is not None:
            return v * 100 // self.size_bytes
        else:
            return None

    @property
    def used_gib(self) -> Optional[int]:
        """Used in the admin - this assumes all devices are mounted"""
        v = self.disk_usage_estimate
        if v is not None:
            return v // 1024**3
        else:
            return None

    @property
    def reserved_gib(self) -> Optional[int]:
        """Used in the admin - this assumes all devices are mounted"""
        v = self.reserved_space_estimate
        if v is not None:
            return v // 1024**3
        else:
            return None

    @property
    def disk_free_gib(self) -> Optional[int]:
        """Used in the admin - this assumes all devices are mounted"""
        v = self.free_space_estimate
        if v is not None:
            return v // 1024**3
        else:
            return None

    @property
    def used_for_backups(self) -> bool:
        """Used in the admin"""
        return self.backupstorage is not None

    @property
    def is_luks(self) -> bool:
        """
        Returns if this storage is within a luks container
        """
        return self.sub_type == BlockSubType.luks

    @property
    def is_luks_open(self) -> bool:
        """
        If this is a luks device, returns true if the mapper
        for this exists.  If it is not a luks device then
        always returns false.
        """
        return (
            (self.inside_luks_container or self.is_luks)
            and self.mapper_dev_path
            and self.mapper_dev_path.exists()
        )

    @property
    def inside_luks_container(self) -> bool:
        """
        Returns if this storage is within a luks container
        """
        parent_count = self.parents.count()
        first_parent = self.parents.first()
        return parent_count == 1 and first_parent.is_luks

    @property
    def mapper_name(self) -> str:
        if self.label:
            return self.label
        else:
            # To avoid kernel name conflicts
            return f"mapper_{self.name}"

    @property
    def mapper_dev_path(self) -> Optional[pathlib.Path]:
        """
        Returns a calculated mount path for the corresponding mapper device.
        This can be applied to either the mounted filesystem block device
        or the parent open/closed luks device

        Returns None if this is not a mapper device or a name could not be
        calculated.

        Calculation notes:  In an ideal world, the mapper name would be
        stored in metadata of the luks device.  However, this is not the
        guaranteed and so if there is no label and the device is not open or
        mounted, then there is no way of determining its mapper path.

        see ``lsblk -o NAME,LABEL,PARTLABEL,PKNAME,MAJ:MIN,RM,SIZE,RO,TYPE,MOUNTPOINTS``
        """
        best_match = None
        if self.is_luks or self.inside_luks_container:
            p1 = mapper_mount_path(self.mapper_name)
            best_match = p1
            if not p1.exists():
                first_parent = self.parents.first()
                if first_parent:
                    parent_match = first_parent.mapper_dev_path
                    if parent_match:
                        best_match = parent_match
                first_child = self.children_set.first()
                if first_child:
                    # Do not call this property on the child as it can
                    # result on excessive recursion
                    p2 = mapper_mount_path(first_child.mapper_name)
                    best_match = p2
        return best_match

    def unlink_all_related(self):
        """
        This function unlinks all related entities, essentially causing them
        to "zombie" until relinked again.  This does not unlink parent
        references or Physical Storage.
        """
        logger.info("Unlinking all related devices for %s", self)
        # Remove any children references
        for c in self.children_set.all():
            logger.debug("Unlinking %s as a parent of %s", self, c)
            c.parents.remove(self)
        for d in self.directory_set.all():
            logger.info(
                "Unlinking a directory reference and deleting all directory meta data for %s/%s",
                self,
                d,
            )
            d.delete()
        # noinspection SpellCheckingInspection
        if hasattr(self, "backupstorage"):
            b = self.backupstorage
            logger.debug("Unlinking Backup Storage %s from %s", b, self)
            b.storage = None
            b.save()
        for k in self.key_set.all():
            logger.debug("Unlinking Key %s from %s", k, self)
            k.related_storage_block = None
            k.save()
        for k in self.external_key.all():
            logger.debug("Unlinking External Key %s from %s", k, self)
            k.external_key = None
            k.save()

    @classmethod
    def retrieve_from_block_node(cls, node: BlockNode) -> Optional["StorageBlock"]:
        """
        Retrieves a StorageBlock model given a BlockNode.  Returns None if they
        are not created.
        """
        is_root_node = not node.parents
        has_fs_uuid = node.fs_uuid is not None
        has_part_uuid = node.part_uuid is not None
        try:
            if is_root_node:
                assert node.type == BlockType.physical
                physical_dev = PhysicalDevice.retrieve_from_block_node(node)
                if physical_dev:
                    retrieved_block = physical_dev.block
                else:
                    retrieved_block = None
            elif has_part_uuid:
                p = StorageBlock.objects.get(partition_uuid=node.part_uuid)
                retrieved_block = p
            elif has_fs_uuid:
                q = StorageBlock.objects.filter(file_system_uuid=node.fs_uuid)
                if q.count() > 1:
                    raise ValueError(
                        f"Node {node}/[{q}] is missing a partition ID and the FS uuid is ambiguous."
                    )
                else:
                    p = q.get()
                    retrieved_block = p
            else:
                retrieved_block = None
        except StorageBlock.DoesNotExist:
            retrieved_block = None

        if retrieved_block is None:
            # A final best attempt at assigning a device
            possible_match_query = StorageBlock.objects.filter(
                kernel_name=node.kernel_name, maj_min=node.maj_min
            )
            if possible_match_query.exists():
                p = possible_match_query.get()
                # If this is unstable, considering inspecting a parent node
                if (
                    p.file_system_uuid is None
                    and p.partition_uuid is None
                    and p.type == node.type.value
                    and p.size_bytes == node.size_bytes
                ):
                    # Plausible match
                    retrieved_block = p
                    logger.info("Approximate match formed between %s and %s", p, node)

        if retrieved_block:
            if not retrieved_block.is_certain_match(node):
                logger.debug(
                    "Retrieved block %s is not a strong match for node %s",
                    retrieved_block,
                    node,
                )
                if has_part_uuid or has_fs_uuid:
                    if (
                        retrieved_block.partition_uuid == node.part_uuid
                        and retrieved_block.file_system_uuid != node.fs_uuid
                    ):
                        # The partition has been formatted.
                        if retrieved_block.file_system_uuid is not None:
                            # The previous file system likely had related objects
                            retrieved_block.unlink_all_related()
                        retrieved_block.file_system_uuid = node.fs_uuid
                        retrieved_block.save()
                    elif (
                        retrieved_block.partition_uuid != node.part_uuid
                        and retrieved_block.file_system_uuid == node.fs_uuid
                    ):
                        # The filesystem has been moved to another partition.
                        retrieved_block.partition_uuid = node.part_uuid
                        retrieved_block.save()
                    else:
                        raise RuntimeError(
                            f"Any retrieved block should be an absolute match, "
                            f"if this occurs there is a query issue: \n{retrieved_block}\n{node}",
                        )
                else:
                    raise RuntimeError(
                        f"Any retrieved block should be an absolute match, "
                        f"if this occurs there is a query issue: \n{retrieved_block}\n{node}",
                    )
        return retrieved_block

    @classmethod
    def clear_device_conflicts(cls, node: BlockNode, exclude_id=None):
        """
        :param node: A StorageBlock is searched for to match this node, if
            a StorageBlock matches this node, the StorageBlock will be set
            as absent.
        :param exclude_id:  Ignore the StorageBlock with this ID
        :raise: ValueError on any issues matching or clearing the device
        """
        conflicting_devices_qry = StorageBlock.objects.filter(
            Q(name=node.name)
            | Q(kernel_name=node.kernel_name)
            | Q(maj_min=node.maj_min)
        ).exclude(id=exclude_id)
        if conflicting_devices_qry.exists():
            logger.debug(
                "Clearing device conflicts for %s with %s exclusion",
                node,
                exclude_id,
            )
            conflicting_devices = list(conflicting_devices_qry.all())
            other_matches = any(x.is_certain_match(node) for x in conflicting_devices)
            if not other_matches:
                for other_s in conflicting_devices:
                    other_s.set_absent()
            else:
                raise ValueError("Unable to find an unambiguous match for storage")

    @classmethod
    def create_from_block_node(
        cls, node: BlockNode, scan_time: int = 0, present: bool = False
    ) -> "StorageBlock":
        """
        Create a StorageBlock model given a BlockNode.  The model is saved and
        a PhysicalDevice is created if required.
        """
        is_root_node = not node.parents
        if is_root_node and not (node.part_table_type and node.part_table_uuid):
            raise ValueError(
                f"Node {node} is missing a partition table type and/or UUID.  This can be resolved"
                f" by reformatting the device or by running {node.serial}"
            )

        b = StorageBlock(
            file_system_uuid=node.fs_uuid,
            partition_uuid=node.part_uuid,
            root=node.root,
            name=node.name,
            kernel_name=node.kernel_name,
            maj_min=node.maj_min,
            label=node.label,
            type=node.type.value,
            sub_type=node.sub_type.value,
            size_bytes=node.size_bytes,
            last_scan_time_ms=scan_time,
            present=present,
            last_present_time_ms=scan_time if present else 0,
        )
        cls.clear_device_conflicts(node)
        b.save()
        if is_root_node:
            p = PhysicalDevice(
                block=b,
                partition_table_type=node.part_table_type,
                partition_table_uuid=node.part_table_uuid,
                model=node.model,
                serial_number=node.serial,
            )
            try:
                p.save()
            except django.db.utils.IntegrityError:
                existing_physical_device = PhysicalDevice.objects.filter(
                    serial_number=node.serial
                )
                if existing_physical_device.exists():
                    # The physical device has been reconfigured
                    existing_physical_device = existing_physical_device.get()
                    existing_physical_device.serial_number = (
                        f"Reconfigured {existing_physical_device.id}: "
                        f"{existing_physical_device.serial_number}"
                    )
                    existing_physical_device.save()
                    p.save()
                else:
                    raise
        return b

    def is_certain_match(self, node: BlockNode) -> bool:
        if node.root and node.part_table_uuid:
            # noinspection SpellCheckingInspection
            return (
                hasattr(self, "physicaldevice")
                and self.physicaldevice.partition_table_uuid == node.part_table_uuid
            )
        else:
            return (
                self.file_system_uuid == node.fs_uuid
                and self.partition_uuid == node.part_uuid
            )

    def update_from_block_node(
        self, node: BlockNode, scan_time: int = 0, present: bool = False
    ):
        """
        Updates a StorageBlock model given a BlockNode.  The model is not saved when
        returned.

        Side effect:  If other devices are using key OS unique fields and this storage
            is a highly confident match for the Node then the other devices will be
            set to absent.
        """
        self.clear_device_conflicts(node, exclude_id=self.id)
        self.present = present
        self.kernel_name = node.kernel_name
        self.maj_min = node.maj_min
        self.name = node.name
        self.label = node.label
        if present:
            self.last_present_time_ms = scan_time
        self.last_scan_time_ms = scan_time
        self.size_bytes = node.size_bytes
        self.type = node.type.value
        self.sub_type = node.sub_type.value
        self.save()

        if hasattr(self, "physicaldevice") and not self.physicaldevice.lock_values:
            if (
                self.physicaldevice.serial_number is None
                or self.physicaldevice.serial_number == node.serial
            ):
                # Ensure physical device data is up to date
                self.physicaldevice.partition_table_type = node.part_table_type
                self.physicaldevice.partition_table_uuid = node.part_table_uuid
                self.physicaldevice.save()
            else:
                # Physical device change has occurred
                existing_device_qry = PhysicalDevice.objects.filter(
                    serial_number=node.serial
                )
                if existing_device_qry.exists():
                    new_p = existing_device_qry.get()
                    old_p = self.physicaldevice
                    old_p.block = None
                    old_p.partition_table_uuid = None
                    old_p.partition_table_type = "unknown"
                    old_p.save()
                    new_p.block = self
                    new_p.partition_table_type = node.part_table_type
                    new_p.partition_table_uuid = node.part_table_uuid
                    new_p.save()

    def save(self, *args, **kwargs):
        self.full_clean()
        new_model = self._state.adding
        super().save(*args, **kwargs)
        if new_model:
            logger.info("Storage block %s has been created", self)
            storage_block_created.send(self)
        elif self.present and not self._last_present:
            logger.info("Storage block %s has reappeared", self)
            storage_block_reappeared.send(self)

    @may_need_root
    def update_sparse_status(self):
        temp_file_name = f"garys_nas_sparse_check.tmp"
        tmp_path = self.mounted_root_directory.os_path().as_path / temp_file_name
        tmp_path.unlink(missing_ok=True)
        with tmp_path.open("wb") as f:
            f.seek(1000000)
            f.write(b"1")
        st = tmp_path.stat()
        is_sparse = st.st_size > (512 * st.st_blocks)
        tmp_path.unlink(missing_ok=True)
        self.is_sparse = is_sparse
        self.save()

    @contextlib.contextmanager
    def capture_errors(self, raise_exceptions=False):
        """
        This is a context manager that will update the storage block with
        processing errors raised.  If warnings are encountered, they are
        simply logged and the block exited.
        """

        try:
            yield
            self.detected_errors = None
        except RuntimeWarning as e:
            self.detected_errors = f"Warning:  {e}"
            logger.warning(
                "Storage block operation failed %s:  %s",
                self,
                e,
            )
        except Exception as e:
            self.detected_errors = f"Error:  {e}"
            logger.error(
                "Storage block operation failed %s:  %s",
                self,
                e,
                exc_info=True,
            )
            if raise_exceptions:
                raise
        finally:
            try:
                self.save()
            except Exception as e:
                logger.error(
                    "Unable to save error details on %s:  %s",
                    self,
                    e,
                    exc_info=True,
                )

    @staticmethod
    def sync_related_objects(delay=0):
        """
        Sync related objects due to system change
        """
        if delay:
            time.sleep(3)
            time.sleep(delay)  # Arbitrary OS settle time is needed for some options
        system_block_change.send(None)

    @may_need_root
    def open_storage(self):
        """
        Attempts to open storage for an encrypted device.  Raises an
        exception if this is not an appropriate device.  .
        """
        if not self.is_luks:
            raise ValueError(f"{self} is not a LUKS storage block")

        if not self.physically_present:
            raise ValueError(f"{self} is missing devices and can not be opened.")

        key = self.key_set.first()
        if not key:
            raise ValueError(f"Could not find a key for {self}")

        with self.capture_errors(raise_exceptions=True):
            open_storage(
                host_dev_path=self.dev_path,
                key_data=key.token,
                mapper_name=self.mapper_name,
            )
            self.sync_related_objects(delay=2)

    @may_need_root
    def mount(
        self,
        mount_path: pathlib.Path = None,
        alternative_dev_path: pathlib.Path = None,
        ro=False,
    ):
        logger.debug("Mount %s", self)
        if not self.present:
            raise ValueError("An absent device can not be mounted")

        with self.capture_errors(raise_exceptions=True):
            if not mount_path:
                from garys_nas.logic.auto_mount import MOUNT_ROOT

                mount_root = MOUNT_ROOT
                mount_path = (
                    mount_root / f"{int.from_bytes(random.randbytes(4), 'big'):#x}"[2:]
                )
                while mount_path.exists():
                    mount_path = (
                        mount_root
                        / f"{int.from_bytes(random.randbytes(4), 'big'):#x}"[2:]
                    )
                logger.info(
                    "Temporarily mounting %s for scanning at %s", self, mount_path
                )
            try:

                mount_path.mkdir(parents=True, exist_ok=True)
                mount(alternative_dev_path or self.dev_path, mount_path, ro=ro)
                self.sync_related_objects()
            except Exception:
                os.rmdir(mount_path)
                raise

    @may_need_root
    def umount(self):
        if not self.present:
            raise ValueError("An absent device can not be unmounted")

        with self.capture_errors(raise_exceptions=True):
            if self.mapper_dev_path and self.mapper_dev_path.exists():
                logger.debug("Umount %s at %s", self, self.mapper_dev_path)
                umount(self.mapper_dev_path)
            elif self.dev_path and self.dev_path.exists():
                logger.debug("Umount %s at %s", self, self.dev_path)
                umount(self.dev_path)
            else:
                raise ValueError(f"Could not find a device to umount for {self}")
            self.sync_related_objects()

    @may_need_root
    def close(self):
        if self.mapper_dev_path and self.mapper_dev_path.exists():
            with self.capture_errors(raise_exceptions=True):
                umount(self.mapper_dev_path)
                if self.is_luks:
                    close_storage(self.mapper_name)
                else:
                    close_storage(self.name)
                self.sync_related_objects(delay=2)
        else:
            raise ValueError("An absent device can not be closed")

    @may_need_root
    def eject(self):
        for root_dev in self.tree_roots:
            logger.debug("Ejecting tree root %s", root_dev)
            with self.capture_errors(raise_exceptions=True):
                eject(root_dev.dev_path)
                self.sync_related_objects(3)

    @may_need_root
    def apply_storage_format(
        self, format_type: str, partition_labels: List[str]
    ) -> Optional[Any]:
        with self.capture_errors(raise_exceptions=True):
            if storage_requires_key(format_type):
                key = garys_nas.models.Key.get_or_generate_key_for_storage(self)
                ret_data = apply_storage_format(
                    self.dev_path, format_type, partition_labels, key=key.token
                )
                # The key needs to move from the root device to the newly created
                # partition
                self.sync_related_objects(3)
                luks_devices = filter(lambda n: n.is_luks, self.connected_nodes)
                key_home = next(luks_devices)
                key.related_storage_block = key_home
                key.save()
                return ret_data
            else:
                return apply_storage_format(
                    self.dev_path, format_type, partition_labels
                )


class PhysicalDevice(models.Model):
    """
    These are special Storage blocks as they represent the physical device.
    The data collected here is used to find a match of a newly inserted
    device into the NAS.  Unfortunately, not all data is always available
    and while the device serial number would be ideal, if this is not
    available then device partition table UUIDs will also be used.
    """

    block = models.OneToOneField(StorageBlock, on_delete=models.CASCADE, null=True)

    # Treated as immutable
    partition_table_type = models.CharField(max_length=8)
    partition_table_uuid = NullableCharField(max_length=40, unique=True, null=True)

    # Fuzzy values - these may change depending on how the device is connected
    # but an attempt is made to preserve the most authentic value
    model = NullableCharField(max_length=40, null=True)
    serial_number = NullableCharField(max_length=40, unique=True, null=True)

    lock_values = models.BooleanField(
        default=False, help_text="Do not update the values to match the system."
    )

    # Mutable Values
    user_label = models.CharField(max_length=40, blank=True, null=True)
    user_comment = models.CharField(max_length=40, blank=True, null=True)

    @classmethod
    def retrieve_from_block_node(cls, node: BlockNode) -> Optional["PhysicalDevice"]:
        """
        Retrieves a StorageBlock model given a BlockNode.  Returns None if they
        are not created.
        """
        is_root_node = not node.parents
        has_part_table_uuid = node.part_table_uuid is not None
        # has_serial = node.serial is not None
        try:
            if not is_root_node:
                return None
            elif has_part_table_uuid:
                return PhysicalDevice.objects.get(
                    partition_table_uuid=node.part_table_uuid
                )
            else:
                return None
        except PhysicalDevice.DoesNotExist:
            return None

    def __str__(self):
        return str(self.block)
