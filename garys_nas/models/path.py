# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import pathlib
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from nas_sensors import may_need_root


class Path(models.Model):
    """
    Represents a logical path on the host and exists because multiple
    devices can be mounted to the same path.
    """

    absolute_path = models.CharField(max_length=256, unique=True)
    maj_min = models.CharField(max_length=12, blank=True, null=True)

    def __str__(self):
        return self.absolute_path

    @property
    def as_path(self) -> pathlib.Path:
        return pathlib.Path(self.absolute_path)

    @may_need_root
    def is_mount(self) -> bool:
        """Returns if this is a root mount point"""
        return self.as_path.is_mount()

    @may_need_root
    def mount_point(self) -> pathlib.Path:
        """
        Returns the path object corresponding to the mount point
        above this path
        """
        p = self.as_path
        while not p.is_mount():
            p = p.parent
        return p

    def clean(self):
        if self.absolute_path[0] == ".":
            raise ValidationError(
                {"absolute_path": _("Relative paths are not allows.")}
            )
        elif self.absolute_path[0] != "/":
            self.absolute_path = f"/{self.absolute_path}"

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
        return self
