# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import contextlib
import logging
import os
import time
import uuid

from garys_nas import clean_up
from django.db import models, transaction

from garys_nas.time_conversion import HOURS_24_IN_MS, now_epoch_ms

logger = logging.getLogger(__name__)


class DBLock(models.Model):
    """
    Acts as a multiprocess DB Lock.  As this app is probably going to
    use SQLITE, select_for_update will not do anything.  Also, we
    need to be careful when using atomic transactions because in SQLITE
    this locks the whole database.  I do not have time to investigate
    how the DB side of things work, so this is kind of a best-guess
    design.

    This has been very loosely tested in multiple python terminals
    """

    MAX_LOCK_TIME = HOURS_24_IN_MS
    MEDIUM_SLEEP = 3
    LOCK_BACKUP = "backup"
    LOCK_INIT = "init_scan"
    LOCK_CHOICES = (
        (LOCK_BACKUP, LOCK_BACKUP),
        (LOCK_INIT, LOCK_INIT),
    )

    name = models.CharField(max_length=16, choices=LOCK_CHOICES)
    token = models.CharField(
        max_length=42,
        null=True,
        blank=True,
    )
    pid = models.PositiveIntegerField()
    locked_time_ms = models.PositiveBigIntegerField()

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    @classmethod
    def expire_locks(cls):
        pids = set(cls.objects.all().values_list("pid", flat=True))
        non_existent_pids = []
        for p in pids:
            try:
                os.kill(p, 0)
            except OSError:
                non_existent_pids.append(p)
        # There is still a chance of a race condition causing an error
        # here, but the likelihood must be very low.
        cls.objects.filter(pid__in=non_existent_pids).delete()

        expire_time = now_epoch_ms() - cls.MAX_LOCK_TIME
        cls.objects.filter(locked_time_ms__lte=expire_time).delete()

    @classmethod
    def acquire(cls, name: str) -> "DBLock":
        """
        Blocks until a lock is acquired
        :param name: The name of the lock to acquire
        :raises RuntimeError:  On failure to acquire a lock
        """
        existing_lock_query = cls.objects.filter(name=name)
        while existing_lock_query.exists():
            time.sleep(cls.MEDIUM_SLEEP)

        try:
            with transaction.atomic():
                assert not existing_lock_query.exists()
                token = str(uuid.uuid4())
                new_lock = cls(
                    name=name,
                    token=token,
                    pid=os.getpid(),
                    locked_time_ms=now_epoch_ms(),
                )
                new_lock.save()
                db_token = existing_lock_query.get().token
                assert token == db_token, f"{token} == {db_token}"
                return new_lock
        except Exception as e:
            logger.debug("Error acquiring lock: %s", e)
            raise RuntimeError(f"Error acquiring lock {name}")

    def release(self):
        try:
            with transaction.atomic():
                self.delete()
        except Exception as e:
            logger.debug("Error releasing lock: %s", e)
            raise RuntimeError(f"Error releasing lock {self.name}")


@contextlib.contextmanager
def db_lock(lock_type: str):
    time.sleep(1)  # Allow other processes a chance to acquire the lock
    lock = DBLock.acquire(lock_type)
    clean_up.delete_models.append(lock)
    try:
        yield
    except Exception as e:
        lock.release()
        clean_up.delete_models.remove(lock)
        raise e
    lock.release()
    clean_up.delete_models.remove(lock)
