# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json
import os
import pathlib
import logging
import subprocess
import sys
from typing import Tuple, List, Iterator, Optional

from django.db import models

from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

from garys_nas.events.signals import directory_created
from garys_nas.models.storage_block import StorageBlock
from garys_nas.models.path import Path
from nas_sensors import BlockSubType, may_need_root, st_dev_to_maj_min

logger = logging.getLogger(__name__)

ManifestEntryType = Tuple[str, int, int, int, int]


class Directory(models.Model):
    """
    This model represents a directory stored on a block device.
    """

    EXCLUDE_BLOCK_SUB_TYPES = (BlockSubType.swap, BlockSubType.squashfs)

    os_paths = models.ManyToManyField(Path)
    relative_path = models.CharField(
        max_length=256, help_text="Relative from the block device, starting with a /"
    )
    storage_block = models.ForeignKey(StorageBlock, on_delete=models.PROTECT)

    file_count = models.PositiveBigIntegerField(
        null=True,
        blank=True,
    )
    file_size = models.PositiveBigIntegerField(
        null=True,
        blank=True,
        help_text="This is apparent size of all the files, which is the size reserved by the file.",
    )
    disk_usage = models.PositiveBigIntegerField(
        null=True,
        blank=True,
        help_text="This is the amount of data used in the filesystem.",
    )

    def __str__(self):
        p = self.os_path()
        if p:
            return p.absolute_path
        else:
            return f"[{self.storage_block}]: .{self.relative_path}"

    @property
    def is_root(self) -> bool:
        """
        Returns if this is a root folder on a block device
        """
        return self.relative_path == "/"

    def os_path(self) -> Optional[Path]:
        """
        Returns an OS Path object for this directory.
        """
        if self.storage_block.present:
            for p in self.os_paths.all():
                if p.maj_min == self.storage_block.maj_min:
                    return p
            return None
        else:
            return None

    @may_need_root
    def is_in_sync(self) -> bool:
        """
        This is a test to check that the Directory is correctly in
        sync
        """
        os_path = self.os_path()
        logger.debug("is_in_sync:  Found path %s", os_path)
        if os_path:
            all_maj_min = set(self.os_paths.all().values_list("maj_min", flat=True))
            maj_min = st_dev_to_maj_min(os_path.as_path.stat().st_dev)
            is_in_sync = (
                len(all_maj_min) == 1
                and maj_min == os_path.maj_min
                and maj_min == self.storage_block.maj_min
            )
            logger.debug(
                "is_in_sync:  %s, %s, %s, %s, %s",
                maj_min,
                all_maj_min,
                os_path.maj_min,
                self.storage_block.maj_min,
                is_in_sync,
            )
            return is_in_sync
        else:
            return False

    def absolute_from(self, root_path: pathlib.Path) -> pathlib.Path:
        """
        Returns the absolute path for this directory from a given root directory
        """
        return root_path / f".{self.relative_path}"

    def get_manifest(self) -> Optional[List[ManifestEntryType]]:
        if self.directorymanifest_set.exists():
            return json.loads(self.directorymanifest_set.first().file_manifest)
        else:
            return None

    def store_manifest(self, manifest: List[ManifestEntryType]):
        if self.directorymanifest_set.exists():
            d = self.directorymanifest_set.first()
            d.file_manifest = json.dumps(manifest)
        else:
            d = DirectoryManifest(file_manifest=json.dumps(manifest), directory=self)
        d.save()

    @may_need_root
    def update_meta_data(self):
        """
        Scans the directory to update the metadata if the directory is mounted
        """
        os_path = self.os_path()
        if os_path:
            logger.info("Scanning dir %s", os_path)
            manifest = list(get_one_file_system_listing(os_path))
            # There is no time different between the time taken for the declarative
            # approach below or an imperative approach (for loop) in real-world tests
            # on my pc.
            total_count = sum(x[1] for x in manifest)
            total_size = sum(x[2] for x in manifest)
            total_fs_usage = sum(x[4] for x in manifest)
            if total_fs_usage and total_size > (total_fs_usage * SPARSE_WARNING):
                logger.warning("Significant sparse data detected in %s", os_path)
            self.store_manifest(manifest)
            self.file_count = total_count
            self.file_size = total_size
            self.disk_usage = total_fs_usage
            self.save()

    def clean(self):
        if self.relative_path[0] == ".":
            raise ValidationError(
                {
                    "relative_path": _(
                        "Relative paths must be defined as an "
                        "absolute path relative to the block device"
                    )
                }
            )
        elif self.relative_path[0] != "/":
            self.relative_path = f"/{self.relative_path}"

    def save(self, *args, **kwargs):
        new_model = self.pk is None
        self.full_clean()
        super().save(*args, **kwargs)
        if new_model:
            directory_created.send(self)


class DirectoryManifest(models.Model):
    """
    This model is not intended to be interacted with directly but instead
    accessed via the Directory Model.  It is set up as a one to many connection
    but is used as a one to one for the time being.
    """

    file_manifest = models.TextField(
        help_text="A JSON blob stored for backup planning."
    )

    directory = models.ForeignKey(Directory, on_delete=models.CASCADE)


SPARSE_WARNING = 1000


def _list_directories(root: pathlib.Path) -> Iterator[str]:
    """
    See docs for get_one_file_system_listing

    Note:
    https://unix.stackexchange.com/questions/389640/why-is-find-xdev-showing-dirs-on-all-partitions
    """
    # noinspection SpellCheckingInspection
    cmd = [
        "find",
        str(root),
        "-xdev",
        "-type",
        "d",
        "-printf",
        # "%p\t%y\t%s\t%C@\t%T@\n",
        "%p\n",
    ]
    # Note:  Filenames can include the tab character
    proc = subprocess.run(cmd, capture_output=True)
    errors = proc.stderr.decode()
    if errors:
        errors = errors.split("\n")
        logger.warning("Directory scanning executed with %i errors", len(errors))
        for row in errors:
            logger.debug(row)
    data = proc.stdout.decode()
    logger.info(
        "File scan completed with %i MiB of data", sys.getsizeof(data) // 1024**2
    )
    data = data.split("\n")
    for row in data:
        if row:
            if row != str(root) and os.path.ismount(row):
                logger.info("Skipping mounted folder in dir scan: %s", row)
                continue
            else:
                yield row


def get_one_file_system_listing(
    root: pathlib.Path, include_symlinks=True
) -> Iterator[ManifestEntryType]:
    """
    Retrieves from the file system a directory summary:
    [directory_path, count, size_in_bytes, last_modified_time, actual_disk_usage_in_bytes]

    This has been validated against the Gnome disk usage analyser and is as close
    as I can get with the time I have spent on it.

    This includes all files and symlinks, but not broken links.  When comparing outputs to
    other tools, the kwargs can be modified to adjust behaviour for verification.
    It also turns out that this is more complex than anticipated and this function
    may need ongoing review (see tips below).

    Hard links are counted only once within the root path for disk usage but not
    for apparent size.  The reason being is that if a backup were performed,
    it is unknown if the copy operation would copy each hard link as a unique
    file.

    File counts includes directories whether empty or not.

    Disk usage can be less than apparent size for sparse files, otherwise disk usage is typically
    greater than apparent size as there needs to be enough allocated blocks for the file.

    Note:  I am concerned about the large amount of data that may be loaded, the
    docs (https://docs.python.org/3/library/subprocess.html#subprocess.PIPE) state
    that subprocess should not be used for large data (specifically, communicate).

    Note:  os.scandir appears to skip broken links

    Ideas:
    https://www.py4u.net/discuss/18884

    Some trials across my PCs:

    On my main-desktop
    find / -xdev -printf "%p\t%y\t%s\t%C@\t%T@\n" > test.txt  ==> 97 MiB
    find /home -xdev -printf "%p\t%y\t%s\t%C@\t%T@\n" > test.txt  ==> 297 MiB

    On my NAS
    df -i  (find high file counts)
    find /mnt/data -xdev -printf "%p\t%y\t%s\t%C@\t%T@\n" > test.txt  ==> 971 MiB
    find /var -xdev -printf "%p\t%y\t%s\t%C@\t%T@\n" > test.txt  ==> 519 MiB

    This will take up too much space.

    find /mnt/data -xdev -type d -printf "%p\t%y\t%s\t%C@\t%T@\n" > test.txt ==> 17 MiB
    find /mnt/data -xdev -type d > test.txt ==> 12 MiB

    This has been manually validated against find results for type f, l and d in find:
    find /home -xdev -type l | wc -l

    [x] Directory count matches bash results
    [x] File count closely matches bash results (and includes symlinks)
    [x] Total size is consistent with df, du, and disk usage analyser

    https://stackoverflow.com/questions/5694741/why-is-the-output-of-du-often-so-different-from-du-b

    More sizing validation notes:
    The "-S" param of du was causing du to double count.  Using:  du -bax /boot/efi/

    https://serverfault.com/questions/275206/disk-full-du-tells-different-how-to-further-investigate

    A great way to investigate this is to investigate this is to manually create
    directory objects using the admin tool and then execute as root, comparing
    to baobab as root.

    """
    errors = []
    hard_link_filter = set()
    for path_name in _list_directories(root):
        try:
            with os.scandir(path_name) as it:
                if include_symlinks:
                    # noinspection PyUnresolvedReferences
                    files = [x for x in it if x.is_file()]
                else:
                    # noinspection PyUnresolvedReferences
                    files = [x for x in it if x.is_file() and not x.is_symlink()]

                # noinspection PyUnresolvedReferences
                stats = [x.stat(follow_symlinks=False) for x in files]
            file_count = len(stats) + 1
            if file_count == 1:
                yield path_name, 1, 0, 0, os.path.getsize(path_name)
            else:
                hard_links = [
                    (x.st_ino, x.st_nlink, x.st_size, x.st_blocks)
                    for x in stats
                    if x.st_nlink > 1
                ]
                if hard_links:
                    counted_hard_links = [
                        x for x in hard_links if x[0] in hard_link_filter
                    ]
                    # duplicated_size = sum(x[2] for x in counted_hard_links)
                    duplicated_size = 0  # See docstring
                    duplicated_blocks = sum(x[3] for x in counted_hard_links)
                    hard_link_filter |= set(x[0] for x in hard_links)
                else:
                    duplicated_size = 0
                    duplicated_blocks = 0

                total_disk_usage = (
                    sum(x.st_blocks for x in stats) - duplicated_blocks
                ) * 512 + os.path.getsize(path_name)
                total_apparent_size = sum(x.st_size for x in stats) - duplicated_size
                # if total_disk_usage and total_apparent_size > (
                #     total_disk_usage * SPARSE_WARNING
                # ):
                #     logger.warning("Something is going on here")
                latest_mod_time = max(x.st_mtime_ns for x in stats)
                yield path_name, file_count, total_apparent_size, latest_mod_time, total_disk_usage
        except PermissionError:
            errors.append(path_name)
        except FileNotFoundError:
            # OS Changes during scan are unavoidable
            pass
    if errors:
        logger.warning("%i Errors in root path '%s' scan, see debug", len(errors), root)
        for e in errors:
            logger.debug("Permission error: %s", e)
