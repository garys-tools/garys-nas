# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import base64
import pathlib
import typing
import uuid
import logging

from django.db import models

from nas_actions import generate_key

if typing.TYPE_CHECKING:
    from garys_nas.models import StorageBlock


logger = logging.getLogger(__name__)


class Key(models.Model):
    """
    Represents an access key or token.

    To specify an external key, here called a KeyFob, the KeyFob must
    be a block formatted and encrypted with its own key.  The block
    needing to be locked by an external key will have a blank Key created
    where the external_key points to the KeyFob device.
    """

    description = models.CharField(max_length=128)
    binary_token = models.CharField(
        max_length=8192,
        null=True,
        blank=True,
        help_text="A base64 encoded key.",
    )
    passphrase = models.CharField(
        max_length=8192,
        null=True,
        blank=True,
        help_text="A text passphrase to unlock the device.",
    )

    external_key = models.ForeignKey(
        "StorageBlock",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text="This indicates that the key is not stored in the DB but "
        "instead exists on an external device.",
        related_name="external_key",
    )

    external_file_name = models.CharField(
        max_length=64,
        null=True,
        blank=True,
        help_text="The file on the external key",
    )

    # Related objects
    related_storage_block = models.ForeignKey(
        "StorageBlock", on_delete=models.SET_NULL, blank=True, null=True
    )

    def __str__(self):
        if self.external_key:
            return (
                f"ExternalKey("
                f"to_unlock={self.related_storage_block}, "
                f"unlocked_by={self.external_key}, "
                f"{self.description})"
            )
        elif self.related_storage_block:
            return (
                f"Key("
                f"to_unlock={self.related_storage_block}, "
                f"{self.description})"
            )
        else:
            return f"UnattachedKey({self.description})"

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    @staticmethod
    def get_or_generate_key_for_storage(storage: "StorageBlock") -> "Key":
        # Check if a key exists
        first_key = storage.key_set.first()
        if first_key:
            key = first_key
        else:
            logger.debug("Auto generating a key for storage %s", storage)
            key = Key(
                description=f"Auto generated key for {storage}",
                related_storage_block=storage,
            )
            key.save()

        # Check if the key has been initialised
        key.initialise_key()
        return key

    def get_external_key_path(self) -> pathlib.Path:
        if not self.external_key:
            raise RuntimeError(
                "This function should not be called for non external keys"
            )
        if not self.external_key.is_luks_open and self.external_key.physically_present:
            try:
                self.external_key.open_storage()
            except Exception as e:
                logger.info("Filed to open external key %s: %s", self, e)

        if self.external_key.is_luks:
            block_with_key_file = self.external_key.children_set.first()
        else:
            block_with_key_file = self.external_key
        if not block_with_key_file.physically_present:
            raise RuntimeWarning("Unlock key is not present")
        if not block_with_key_file.mounted_root_directory:
            block_with_key_file.mount()

        key_path = (
            block_with_key_file.mounted_root_directory.os_path().as_path
            / self.external_file_name
        )
        return key_path

    def initialise_key(self):
        """
        Will check that this key has a token somewhere and will generate
        one if required.
        """

        if not (self.passphrase or self.binary_token or self.external_key):
            logger.debug("Blank key found, generating random key for %s", self)
            self.binary_token = base64.b64encode(generate_key()).decode(
                encoding="ASCII"
            )
            self.save()

        if self.external_key:
            logger.debug("Checking external key for %s", self)
            # This is an external key in which case a separate key needs to
            # be generated on the external device, which is assumed to be
            # locked by this key
            if not self.external_file_name:
                self.external_file_name = str(uuid.uuid4())
                self.save()

            key_path = self.get_external_key_path()
            if not key_path.exists():
                with key_path.open("wb") as f:
                    f.write(generate_key())

    @property
    def token(self) -> bytes:
        if self.external_key:
            key_path = self.get_external_key_path()
            with key_path.open("rb") as f:
                return f.read()
        else:
            if self.binary_token:
                return base64.b64decode(self.binary_token)
            elif self.passphrase:
                return self.passphrase.encode(encoding="ASCII")
            else:
                raise RuntimeError("Key %s has no token or passphrase", self)
