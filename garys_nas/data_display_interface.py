# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
A series of functions to define interfaces for data display
"""
import decimal
from typing import List, TypedDict, Union, Dict

from nas_sensors.constants import Units
from nas_sensors.sensor_data_point import SensorDataPointBytes, SensorDataPoint


class DisplayDataTable(TypedDict):
    value: Union[int, decimal.Decimal]
    timestamp: int
    units: Units
    name: str
    description: str


def homogenous_sensor_data(
    data_structure: Dict[str, Union[SensorDataPointBytes, SensorDataPoint]],
    keys: List[str],
    units=None,
    title="Table",
    descriptions: dict = None,
) -> dict:
    """
    A simple table that describes a homogenous data set

    Data structure is a class with members of type SensorDataPoint

    TODO:  Maybe this table has consistent units applied across it?
    This is to avoid going to a full FE library and reduce development
    time.  I don't think this app needs significant FE logic, it's intended
    to be overall quite simple
    """
    data = []
    for k in keys:
        data_point = data_structure[k]
        if units:
            v = data_point.as_units(units).value
            u = units
        else:
            v = data_point.value
            u = data_point.units

        data.append(
            DisplayDataTable(
                value=v,
                timestamp=data_point.timestamp,
                units=u,
                name=k,
                description=descriptions and descriptions.get(k, "") or "",
            )
        )
    # Proof of concept
    # TODO:  Which entity has the responsibility of deciding on recommended units?
    # This really is a display thing, not a data processing so should go to the
    # front end.
    # Moving to Svelte earlier could make this easier?  But I wanted to keep it simple
    # table_units
    return {"title": title, "data": data}


def generic_table(
    data: List[dict],
    headings: List[str],
    value_map: dict = None,
    title="Table",
) -> dict:
    """
    A more flexible table structure that takes a dictionary of mapping
    functions to map values into a string format.  The default map is str
    """
    table_data = []
    for row in data:
        table_row = []
        table_data.append(table_row)
        for h in headings:
            v = row.get(h, "")
            v_map = value_map.get(h, str) if value_map else str
            table_row.append(v_map(v))

    return {"title": title, "headings": headings, "data": table_data}


network_map = {
    "addresses": lambda x_list: "\n".join(
        [f"{x['family'].value}:    {x['address']}  /  {x['network']}" for x in x_list]
    )
}
