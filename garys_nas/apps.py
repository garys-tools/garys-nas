# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from django.apps import AppConfig
import django.db.utils


class GarysNasConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "garys_nas"
    verbose_name = "Gary's Nas"

    def ready(self):
        # noinspection PyUnresolvedReferences
        import garys_nas.events.cause_and_effect  # Register receivers

        # noinspection PyUnresolvedReferences
        import garys_nas.clean_up  # Register signal handlers

        from garys_nas.models import DBLock

        try:
            DBLock.expire_locks()
        except django.db.utils.OperationalError:
            # Chances are the DB hasn't been migrated yet
            pass
