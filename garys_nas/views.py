# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import socket

from django.shortcuts import render

from garys_nas.data_display_interface import (
    homogenous_sensor_data,
    generic_table,
    network_map,
)
from nas_sensors import system, sensor_data_point, networking, is_tcp_port_open
from nas_sensors.constants import Units


def base(request):
    context = {}
    return render(request, "garys_nas/base.html", context)


def home(request):

    base_address = request.META["HTTP_HOST"]
    if ":" in base_address:
        assert base_address.count(":") == 1
        base_address = base_address.split(":")[0]
    cockpit_port = 9090
    if is_tcp_port_open(base_address, cockpit_port):
        cockpit_address = f"https://{base_address}:{cockpit_port}/"
    else:
        cockpit_address = None

    physical_memory_stats = system.get_current_physical_memory_stats()
    swap_memory_stats = system.get_current_swap_stats()
    load_stats = system.get_current_load_averages()
    temp_data = system.get_system_temperatures()
    network_data = networking.get_network_interface_data()
    network_list = [network_data[k] for k in sorted(network_data.keys())]

    context = {
        "cockpit_address": cockpit_address,
        "hostname": socket.gethostname(),
        "net_data": generic_table(
            data=network_list,
            headings=["name", "is_up", "full_duplex", "speed_MB", "addresses"],
            value_map=network_map,
            title="Network Interfaces",
        ),
        "mem_data": homogenous_sensor_data(
            physical_memory_stats,
            system.physical_memory_fields,
            title="Physical Memory",
            descriptions=system.memory_descriptions,
            units=sensor_data_point.choose_size_units(
                physical_memory_stats["total"].value
            ),
        ),
        "mem_chart": homogenous_sensor_data(
            physical_memory_stats,
            system.memory_usage_fields,
            title="Physical Memory",
            units=Units.MiB,
        ),
        "swap_data": homogenous_sensor_data(
            swap_memory_stats,
            system.swap_memory_fields,
            title="Swap Memory",
            descriptions=system.memory_descriptions,
            units=sensor_data_point.choose_size_units(swap_memory_stats["total"].value),
        ),
        "swap_chart": homogenous_sensor_data(
            swap_memory_stats,
            system.memory_usage_fields,
            title="Swap Memory",
            units=Units.MiB,
        ),
        "load_stats": homogenous_sensor_data(
            load_stats,
            system.load_fields,
            title="CPU Load",
            descriptions=system.load_descriptions,
            units=None,
        ),
        "temp_data": homogenous_sensor_data(
            temp_data,
            system.get_system_temperature_fields(),
            title="System Temperatures",
            descriptions=None,
            units=None,
        ),
    }
    return render(request, "garys_nas/home.html", context)
