# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Django recommends avoiding custom signals but in this case,
These are needed to break circular dependencies

A better design might see further decoupling of logic from the django
models, but this is good enough for now.
"""
from django.dispatch import Signal

backup_storage_updated = Signal()
directory_created = Signal()
storage_block_created = Signal()
system_block_change = Signal()
storage_block_reappeared = Signal()
backup_completed = Signal()
backup_operation_created = Signal()

__all__ = [
    "backup_storage_updated",
    "directory_created",
    "storage_block_created",
    "system_block_change",
    "storage_block_reappeared",
    "backup_completed",
    "backup_operation_created",
]
