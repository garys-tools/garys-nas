# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
There are a number of interacting components within this application
and before these links get out of hand the implementation of these
links is being re-conceptualised.

The purpose of this file is to make event driven connections more
explicit and reduce the interdependency between components.

While an event driven architecture is common in this type of application,
event interrelationships are implicit and this application is small enough
to centralise these relationships so that each component can remain an
isolated entity.

This is modelled off a cause and effect logic paradigm used in some
IACS platforms.  A more declarative approach has been considered,
but the simplicity of an imperative approach is preferred as there
are not that many connections required.

This module acts as a namespace for the relationships defined in
cause_and_effect.ods.
"""
import threading
import logging
from typing import Optional, List

from django.dispatch import receiver

from garys_nas.logic import (
    consume_backup_operations,
    consume_directory_scan_operations,
    consume_eject_operations,
    consume_format_operations,
    consume_init_block_operations,
    produce_auto_eject_operations,
    produce_due_backup_operations,
    create_pending_operation_for_backup,
    produce_due_directory_meta_scan_operations,
)
from garys_nas.logic.clean_up_bad_removal import clean_up_bad_removal
from garys_nas.logic.consume_lock_operations import (
    consume_lock_operations,
    consume_unlock_operations,
)
from garys_nas.logic.produce_lock_operations import (
    produce_lock_operation,
    produce_unlock_operation,
)
from garys_nas.models import (
    PendingOperation,
    StorageBlock,
    BackupOperation,
    Directory,
)
from garys_nas.logic import (
    sync_storage_blocks,
    sync_directories,
    sync_paths,
    auto_mount,
)
from nas_sensors import may_need_root
from garys_nas.events import signals

logger = logging.getLogger(__name__)
action_lock = threading.Lock()


def _create_pending_operation(
    operation_type: str,
    data: Optional[dict] = None,
    storage_block: Optional[StorageBlock] = None,
    backup_operation: Optional[BackupOperation] = None,
    directory: Optional[Directory] = None,
):
    p = PendingOperation(
        operation_type=operation_type,
        related_storage_block=storage_block,
        related_backup_operation=backup_operation,
        related_directory=directory,
    )
    if data:
        p.dump_operation_data(data)
    p.save()


def produce_pending_init_storage_block_operation(
    storage_block: StorageBlock,
):
    _create_pending_operation(
        operation_type=PendingOperation.TYPE_INIT_BLOCK,
        storage_block=storage_block,
    )


def sync_as_user():
    sync_storage_blocks()
    sync_directories()


def sync_as_root():
    sync_storage_blocks()
    sync_paths()
    sync_directories()


def on_scheduler_poll():
    """
    This is executed on a system poll where only user permissions are
    needed.
    """
    logger.info("Scheduled poll started")
    try:
        with action_lock:
            sync_as_user()
            produce_due_backup_operations()
            produce_due_directory_meta_scan_operations()
            produce_auto_eject_operations()
    except Exception as e:
        logging.exception("Scheduled poll caught an exception: %s", e)
    logger.info("Scheduled poll completed")


@may_need_root
def on_root_task_poll():
    """
    This is used to for consistency polling where root access is needed
    """
    logger.info("Root task poll started")
    try:
        with action_lock:
            sync_as_root()
            consume_init_block_operations()
            consume_lock_operations()
            consume_unlock_operations()
            consume_eject_operations()
            consume_format_operations()
            consume_directory_scan_operations()
            consume_backup_operations()
    except Exception as e:
        logging.exception("Root task poll caught an exception: %s", e)
    logger.info("Root task poll completed")


@may_need_root
def on_fast_root_poll():
    """
    Some actions are user initiated and need to fun at a faster rate.
    This is currently intended
    """
    try:
        with action_lock:
            sync_as_root()
            consume_lock_operations()
            consume_unlock_operations()
            consume_eject_operations()
            auto_mount()
    except Exception as e:
        logging.exception("Root task poll caught an exception: %s", e)
    logger.info("Root task poll completed")


@may_need_root
def on_add_device():
    """
    An external event will trigger this on a change to system devices
    """
    logger.info("Add device started")
    with action_lock:
        sync_as_root()
        auto_mount()
    logger.info("Add device completed")


@may_need_root
def on_rem_device():
    """
    An external event will trigger this on a change to system devices
    """
    logger.info("Remove device started")
    with action_lock:
        sync_as_root()
        clean_up_bad_removal()
    logger.info("Remove device completed")


@receiver(signals.system_block_change)
@may_need_root
def on_system_block_change(*_, **__):
    """
    An external event will trigger this on a change to system devices
    """
    logger.info("on_system_change started")
    sync_as_root()
    logger.info("on_system_change completed")


@receiver(signals.storage_block_created)
def on_storage_block_created(sender: StorageBlock, **_):
    produce_pending_init_storage_block_operation(sender)


@receiver(signals.storage_block_reappeared)
def on_storage_block_reappeared(sender: StorageBlock, **_):
    logger.info("on_storage_block_reappeared: %s", sender)
    for b in BackupOperation.objects.filter(backup_storage__storage=sender):
        create_pending_operation_for_backup(b)


def on_user_request_format(
    format_selection: str,
    device_labels: List[str],
    storage_block: StorageBlock,
):
    _create_pending_operation(
        operation_type=PendingOperation.TYPE_FORMAT,
        storage_block=storage_block,
        data={"format_selection": format_selection, "device_labels": device_labels},
    )


def on_user_eject_device(
    storage_block: StorageBlock,
):
    produce_auto_eject_operations(storage_block)


def on_user_lock_device(
    storage_block: StorageBlock,
):
    produce_lock_operation(storage_block)


def on_user_unlock_device(
    storage_block: StorageBlock,
):
    produce_unlock_operation(storage_block)


@receiver(signals.directory_created)
def on_directory_created(sender: Directory, **_):
    # Produce Sync Path Operation
    _create_pending_operation(
        operation_type=PendingOperation.TYPE_DIRECTORY_SCAN, directory=sender
    )


@receiver(signals.backup_operation_created)
def on_backup_operation_created(sender: BackupOperation, **_):
    logger.info("on_backup_operation_created: %s", sender)
    create_pending_operation_for_backup(sender)


@receiver(signals.backup_completed)
def on_backup_completed(sender: BackupOperation, **_):
    logger.info("on_backup_completed: %s", sender)
    produce_auto_eject_operations()
