# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
from typing import Optional

from garys_nas.models import PendingOperation, StorageBlock

logger = logging.getLogger(__name__)


def produce_auto_eject_operations(storage: Optional[StorageBlock] = None):
    """
    This will produce an auto eject task for the provided storage or will
    search all storages to find ones that are available for auto-eject.

    It does not block eject from executing for any reason, it just
    produces a request.
    """
    if storage:
        eject_storages = [storage]
    else:
        eject_storages = list(
            StorageBlock.objects.filter(present=True, backupstorage__auto_eject=True)
        )

    for storage_block in eject_storages:
        existing_pending = PendingOperation.objects.filter(
            operation_type=PendingOperation.TYPE_EJECT,
            related_storage_block=storage_block,
            started_time__isnull=True,
        )
        if not existing_pending.exists():
            logger.debug("generating eject operation for to %s", storage_block)
            p = PendingOperation(
                operation_type=PendingOperation.TYPE_EJECT,
                related_storage_block=storage_block,
            )
            p.save()
        else:
            logger.debug("Pending operations exist:  %s", existing_pending)
