# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from django.core.exceptions import ObjectDoesNotExist

from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.models.pending_operations import PendingOperation
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


@may_need_root
def consume_eject_operations():
    """
    A backup can be ejected if it is set to auto backup and no backup
    has been performed since the storage has been added.

    It is assumed that an eject operation will only be requested for a
    device that is present.  If, for any reason, an eject request exists
    for an absent device the request will be deleted.

    The devices to be ejected are those that are configured for auto eject
    with no 0 count backup operations (this needs to be negative
    logic because of the many-to-many field).

    Ejects the device:  Exceptions will be raised if there is an
    OS command issue.  A device is only ejected if the root device
    has no mounted children.
    """
    sync_storage_blocks()

    pending_eject_requests = PendingOperation.objects.filter(
        operation_type=PendingOperation.TYPE_EJECT,
        started_time__isnull=True,
    )

    redundant_eject_requests = pending_eject_requests.filter(
        related_storage_block__present=False
    )
    redundant_eject_requests.delete()

    for op in pending_eject_requests:
        # Init
        s = op.related_storage_block

        # Test for other pending operations
        other_pending_ops = s.pendingoperation_set.exclude(
            operation_type=PendingOperation.TYPE_EJECT,
        ).filter(
            started_time__isnull=True,
        )
        backup_storage_ops = PendingOperation.objects.filter(
            related_backup_operation__enabled=True,
            related_backup_operation__backup_storage__storage=s,
            started_time__isnull=True,
        )

        if other_pending_ops.exists() or backup_storage_ops.exists():
            logger.info(
                "Skipping auto ejecting device: %s, pending operations exist", s
            )
            logger.debug(other_pending_ops)
            continue

        logger.info("Auto ejecting device: %s", s)
        with op.executing():
            try:
                if s.backupstorage and s.mounted_root_directory:
                    logger.info("Scanning device before eject: %s", s)
                    s.mounted_root_directory.update_meta_data()
            except ObjectDoesNotExist:
                pass

            all_connected_nodes = list(s.connected_nodes)
            mounted = list(
                filter(
                    lambda n: n.mounted_root_directory is not None,
                    all_connected_nodes,
                )
            )

            for m in mounted:
                logger.debug("Unmounting %s", m)
                m.umount()

            open_in_luks = list(
                filter(
                    lambda n: n.inside_luks_container and n.is_luks_open,
                    all_connected_nodes,
                )
            )

            for m in open_in_luks:
                logger.debug("Closing %s", m)
                m.close()

            # Eject
            sync_storage_blocks()
            logger.info("Ejecting storage device %s", s)
            s.eject()
