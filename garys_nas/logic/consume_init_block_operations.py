# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.logic.sync_paths import sync_paths
from garys_nas.logic.sync_directories import sync_directories
from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.models import PhysicalDevice
from garys_nas.models.db_lock import db_lock, DBLock
from garys_nas.models.pending_operations import (
    PendingOperation,
    CancelOperationException,
    OperationRetryException,
)
from nas_sensors import may_need_root, BlockSubType

logger = logging.getLogger(__name__)


@may_need_root
def consume_init_block_operations():
    """
    When a new block is inserted and is mounted then this function will
    ensure that it is appropriately scanned for files and sparse status.
    This function does depend on filesystem mounting of a device, and so
    if the root of the block is not mounted it will not close the operation.
    """
    with db_lock(DBLock.LOCK_INIT):
        sync_storage_blocks()
        for op in PendingOperation.objects.filter(
            operation_type=PendingOperation.TYPE_INIT_BLOCK,
            started_time__isnull=True,
            related_storage_block__present=True,
        ):
            with op.executing(raise_exceptions=False):
                s = op.related_storage_block
                logger.info("Initial scan of %s starting", s)
                op_data = op.load_operation_data()
                retry_attempts = op_data.setdefault("retry_attempt", -1)
                retry_attempts += 1
                op_data["retry_attempt"] = retry_attempts

                if s.children_set.exists():
                    logger.debug("Skipping %s init as the block has children", s)
                    scan_complete = True
                    scan_response = "init as the block has children"
                elif PhysicalDevice.objects.filter(block=s).exists():
                    logger.debug("Skipping %s init as this is physical device", s)
                    scan_complete = True
                    scan_response = "this is a physical block"
                elif s.sub_type == BlockSubType.swap:
                    logger.debug("Skipping %s init as this is a swap block", s)
                    scan_complete = True
                    scan_response = "this is a swap block"
                elif s.sub_type == BlockSubType.luks:
                    logger.debug("Skipping %s init as this is a luks block", s)
                    scan_complete = True
                    scan_response = "this is a luks block"
                elif (
                    retry_attempts > 4
                    and not s.children_set.exists()
                    and not s.mounted_root_directory
                ):
                    # Try Mount the device temporarily
                    try:
                        s.mount()
                        sync_storage_blocks()
                        logger.info("3")
                        sync_directories()
                        logger.info("4")
                        sync_paths()
                        scan_complete = True
                        scan_response = "Temporarily mounted and scanned"
                    except Exception as e:
                        logging.exception(
                            "Exception occurred temporarily mounting %s: %s", s, e
                        )
                        scan_complete = False
                        scan_response = "Exception occurred temporarily mounting"
                    else:
                        d = s.mounted_root_directory
                        d.update_meta_data()
                        s.update_sparse_status()
                    finally:
                        s.umount()

                else:
                    root_found = False
                    # Update Sparse Status
                    logger.info("Directory set debug, %s", s.directory_set.all())
                    if s.is_sparse is None:
                        try:
                            s.update_sparse_status()
                        except AttributeError:
                            s.is_sparse = None

                    for d in s.directory_set.all():
                        if d.is_root:
                            root_found = True
                        if d.file_size is None:
                            d.update_meta_data()

                    scan_complete = root_found and s.is_sparse is not None
                    if scan_complete:
                        scan_response = "Success"
                    elif not root_found:
                        scan_response = "Can not find root"
                    elif s.is_sparse is None:
                        scan_response = "Could not detect sparse status"
                    else:
                        scan_response = "Unknown"

                if scan_complete:
                    op_data["scan_response"] = scan_response
                    op_data["error"] = None
                    op.dump_operation_data(op_data)
                    logger.info("Initial scan of %s completed", s)
                else:
                    last_response = op_data.get("scan_response")
                    if last_response:
                        op_data["last_response"] = last_response
                    op_data["scan_response"] = scan_response
                    op.dump_operation_data(op_data)
                    if retry_attempts > 10:
                        raise CancelOperationException(
                            "Too many scan attempts, closing task"
                        )
                    else:
                        raise OperationRetryException(
                            "Unable to scan device, will try again later."
                        )

        delete_ops = list(
            PendingOperation.objects.filter(
                operation_type=PendingOperation.TYPE_INIT_BLOCK,
                started_time__isnull=True,
                related_storage_block__isnull=True,
            )
        )
        for d in delete_ops:
            logger.info(
                "Deleting pending operation that has lost its storage link: %s", d
            )
            d.delete()
