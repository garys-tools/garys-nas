# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.models import PendingOperation, StorageBlock

logger = logging.getLogger(__name__)


def produce_lock_operation(storage: StorageBlock):
    """
    Creates a user lock operation
    """
    if storage.is_luks_open:
        existing_pending = PendingOperation.objects.filter(
            operation_type=PendingOperation.TYPE_LOCK,
            related_storage_block=storage,
            started_time__isnull=True,
        )
        if not existing_pending.exists():
            logger.debug("generating lock operation for to %s", storage)
            p = PendingOperation(
                operation_type=PendingOperation.TYPE_LOCK,
                related_storage_block=storage,
            )
            p.save()
        else:
            logger.debug("Pending operations exist:  %s", existing_pending)


def produce_unlock_operation(storage: StorageBlock):
    """
    Creates a user unlock operation - only
    """
    if (storage.is_luks or storage.inside_luks_container) and not storage.is_luks_open:
        existing_pending = PendingOperation.objects.filter(
            operation_type=PendingOperation.TYPE_UNLOCK,
            related_storage_block=storage,
            started_time__isnull=True,
        )
        if not existing_pending.exists():
            logger.debug("generating unlock operation for to %s", storage)
            p = PendingOperation(
                operation_type=PendingOperation.TYPE_UNLOCK,
                related_storage_block=storage,
            )
            p.save()
        else:
            logger.debug("Pending operations exist:  %s", existing_pending)
