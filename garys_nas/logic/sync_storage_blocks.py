# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import django.core.exceptions
import django.db.utils

from garys_nas.models.user_message import error_message
from nas_sensors import collect_data_nodes, BlockType

from garys_nas.models.storage_block import StorageBlock
from garys_nas.models.directory import Directory
from garys_nas.models.path import Path
from garys_nas.time_conversion import now_epoch_ms


logger = logging.getLogger(__name__)


def sync_storage_blocks():
    """
    This must be run to maintain consistency between the Storage Block models and the OS.
    As this function also knows the mount point, it will ensure that each mounted
    storage block contains a root directory object.
    """
    scanning_time = now_epoch_ms()
    block_nodes = collect_data_nodes()

    accepted_nodes = []
    processed_nodes = []

    # Reset Node data
    initial_storage_blocks = list(
        StorageBlock.objects.all().values_list("id", flat=True)
    )

    # Check Nodes
    for node in block_nodes:
        if node.type in StorageBlock.EXCLUDE_BLOCK_TYPES:
            continue
        elif node.type == BlockType.physical and not node.part_table_uuid:
            logger.info("Skipping node due to missing partition table uuid: %s", node)
            continue
        elif node in processed_nodes:
            logger.debug("Ignoring duplicate node in node list: %s", node)
            continue
        else:
            accepted_nodes.append(node)
        processed_nodes.append(node)
        storage = StorageBlock.retrieve_from_block_node(node)
        try:
            if storage:
                initial_storage_blocks.remove(storage.id)
                storage.update_from_block_node(node, scanning_time, True)
            else:
                StorageBlock.create_from_block_node(node, scanning_time, True)
        except (
            ValueError,
            django.core.exceptions.ValidationError,
            django.db.utils.IntegrityError,
        ) as e:
            error_message(
                msg=f"Skipping sync for node {node} due to validation errors: {e}.  "
                f"The device may need to be rescanned."
            )
            logger.debug(
                "Error syncing node %s to storage %s", node, storage, exc_info=True
            )
            if storage:
                logger.debug(
                    "Key storage attributes: %s %s %s %s %s",
                    storage.id,
                    storage.kernel_name,
                    storage.maj_min,
                    storage.file_system_uuid,
                    storage.partition_uuid,
                )
            accepted_nodes.remove(node)

    # Set absent status - note that the save method is intentionally called.
    for storage in StorageBlock.objects.filter(id__in=initial_storage_blocks):
        storage.last_scan_time_ms = scanning_time
        storage.set_absent()
        storage.save()

    # Check Node Relationships
    for node in accepted_nodes:
        storage = StorageBlock.retrieve_from_block_node(node)
        if node.children:
            # This will return a storage block (unless there is a parallel process editing db)
            children = [StorageBlock.retrieve_from_block_node(c) for c in node.children]
            if None in children:
                logger.debug(
                    "Node %s with children %s is resulting in the following retrieved blocks %s",
                    node,
                    "\n".join([str(x) for x in node.children]),
                    children,
                )
                raise RuntimeError(
                    "All children should be uniquely retrievable.  See debug logs for info"
                )
            for c in children:
                if not c.parents.contains(storage):
                    logger.debug("Adding %s to %s", c, storage)
                    c.parents.add(storage)
            # Reset all instances where this node is no longer a parent
            bad_children = StorageBlock.objects.filter(parents=storage).exclude(
                id__in=[x.id for x in children]
            )
            if bad_children.exists():
                for b in bad_children:
                    if b.inside_luks_container and not b.present:
                        # Luks device is closed, do not remove
                        pass
                    else:
                        logger.debug("Removing %s from %s in child case", b, storage)
                        b.parents.remove(storage)
        else:
            # Ensure this node is not a parent anywhere
            bad_children = StorageBlock.objects.filter(parents=storage)
            if bad_children.exists():
                for b in bad_children:
                    if b.inside_luks_container and not b.present:
                        # Luks device is closed, do not remove
                        pass
                    else:
                        logger.debug("Removing %s from %s", b, storage)
                        b.parents.remove(storage)

    # For any present device with absent parents (present parents should exist)
    # remove the parents as there is most likely a reconfiguration
    for b in StorageBlock.objects.filter(
        present=True,
        parents__present=False,
    ).all():
        assert b.parents.filter(
            present=True
        ).exists(), "Parents must be present if a block is present"
        for p in b.parents.filter(present=False):
            b.parents.remove(p)

    # Storage Blocks, Directories and Paths
    for node in accepted_nodes:
        storage = StorageBlock.retrieve_from_block_node(node)
        if storage.sub_type in Directory.EXCLUDE_BLOCK_SUB_TYPES:
            continue
        for mount_point in node.mounts:
            # Ensure the path exists
            existing_paths = Path.objects.filter(absolute_path=mount_point.name)
            if existing_paths.exists():
                root_path = existing_paths.get()
            else:
                root_path = Path(absolute_path=mount_point.name)
                root_path.save()

            # Ensure a root directory exists with a mounted path
            existing_root_dir = storage.directory_set.filter(relative_path="/")
            if existing_root_dir.exists():
                root_dir = existing_root_dir.get()
            else:
                root_dir = Directory(relative_path="/", storage_block=storage)
                root_dir.save()
            root_dir.os_paths.add(root_path)
