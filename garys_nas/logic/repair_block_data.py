# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This module includes scripts that can help repair database issues that can
arise from hardware or the operating system changing values used to
uniquely identify devices.
"""
import logging

from django.db.models import Q

from nas_sensors import collect_data_nodes

from garys_nas.models.storage_block import PhysicalDevice, StorageBlock

logger = logging.getLogger(__name__)


def update_uuids_by_serial_number():
    """
    Using the serial number found in the database, update the
    respective UUID information.
    """
    block_nodes = collect_data_nodes()

    # Check Nodes
    for node in block_nodes:
        if (
            node.serial
            and PhysicalDevice.objects.filter(serial_number=node.serial).exists()
        ):
            p = PhysicalDevice.objects.get(serial_number=node.serial)
            if not node.part_table_type:
                continue
            elif node.part_table_type and not node.part_table_uuid:
                raise ValueError(
                    f"Node {node} is missing a partition table UUID.  This can be resolved by "
                    f"reformatting the device or by running {node.serial}"
                )

            p.partition_table_type = node.part_table_type
            p.partition_table_uuid = node.part_table_uuid
            p.save()


def update_file_system_uuids():
    """
    Uses the partition UUID to update the filesystem UUID
    """
    block_nodes = collect_data_nodes()

    # Check Nodes
    for node in block_nodes:
        if (
            node.part_uuid
            and node.fs_uuid
            and StorageBlock.objects.filter(partition_uuid=node.part_uuid)
            .exclude(file_system_uuid=node.fs_uuid)
            .exists()
        ):
            p = StorageBlock.objects.get(partition_uuid=node.part_uuid)
            p.file_system_uuid = node.fs_uuid
            p.save()


def check_mixed_uuids():
    """
    Due to past operations the FS and partition UUIDs might be swapped
    resulting in duplicate devices being produced on sync.  This checks
    for unique matches of these UUIDs
    """
    block_nodes = collect_data_nodes()

    # Check Nodes
    for node in block_nodes:
        logger.info("check_mixed_uuids for %s", node)
        if node.fs_uuid and node.part_uuid:
            q = StorageBlock.objects.filter(
                Q(file_system_uuid=node.fs_uuid)
                | Q(file_system_uuid=node.part_uuid)
                | Q(partition_uuid=node.fs_uuid)
                | Q(partition_uuid=node.part_uuid)
            )
            # noinspection DuplicatedCode
            count = q.count()
            if count > 1:
                logger.info(
                    "Unable to check node %s due to ambiguous matching of uuids", node
                )
            elif count == 1:
                s = q.get()
                logger.info("Updating %s", s)
                s.file_system_uuid = node.fs_uuid or None
                s.partition_uuid = node.part_uuid or None
                s.save()
            continue

        if node.part_uuid or node.fs_uuid:
            check_uuid = node.part_uuid or node.fs_uuid
            q = StorageBlock.objects.filter(
                Q(file_system_uuid=check_uuid) | Q(partition_uuid=check_uuid)
            )
            # noinspection DuplicatedCode
            count = q.count()
            if count > 1:
                logger.info(
                    "Unable to check node %s due to ambiguous matching of uuids", node
                )
            elif count == 1:
                s = q.get()
                logger.info("Updating %s", s)
                s.file_system_uuid = node.fs_uuid or None
                s.partition_uuid = node.part_uuid or None
                s.save()
            continue
