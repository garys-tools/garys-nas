# Copyright: (c) 2022, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.models.pending_operations import PendingOperation
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


@may_need_root
def consume_lock_operations():
    """
    A simple consumer that locks or unlocks storages as required.
    """
    sync_storage_blocks()

    pending_lock_requests = PendingOperation.objects.filter(
        operation_type=PendingOperation.TYPE_LOCK,
        started_time__isnull=True,
    )

    for op in pending_lock_requests:
        s = op.related_storage_block
        with op.executing():
            if s.is_luks_open:
                logger.info("Attempting to lock device: %s", s)
                s.close()
            else:
                logger.info("Device already locked: %s", s)


@may_need_root
def consume_unlock_operations():
    """
    A simple consumer that locks or unlocks storages as required.
    """
    sync_storage_blocks()

    pending_unlock_requests = PendingOperation.objects.filter(
        operation_type=PendingOperation.TYPE_UNLOCK,
        started_time__isnull=True,
    )

    for op in pending_unlock_requests:
        s = op.related_storage_block
        with op.executing():
            if s.is_luks_open:
                logger.info("Device already unlocked: %s", s)
            elif s.is_luks:
                logger.info("Attempting to unlock device: %s", s)
                s.open_storage()
            elif s.inside_luks_container:
                logger.info("Attempting to unlock device: %s", s.parents.first())
                s.parents.first().open_storage()
            else:
                logger.info("Device is not a luks device: %s", s)
