# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from django.db.models import F, Subquery, OuterRef

from garys_nas.models import PendingOperation, BackupOperation
from garys_nas.time_conversion import now_epoch_ms

logger = logging.getLogger(__name__)


def produce_due_backup_operations():
    """
    Find any backups that are due and produce an associated backup operation.
    If a backup has never been performed before, ensure an operation is
    scheduled.
    """
    not_queued_operations = BackupOperation.objects.exclude(
        pendingoperation__operation_type=PendingOperation.TYPE_BACKUP,
        pendingoperation__completed_time__isnull=True,
    )

    logger.debug(
        "produce_due_backup_operations not_queued_operations: %s", not_queued_operations
    )
    no_pending_operations = BackupOperation.objects.filter(
        pendingoperation__isnull=True
    )
    if no_pending_operations.exists():
        logger.debug(
            "No pending operations exist for backups:  %s", no_pending_operations
        )
        due_operations = no_pending_operations
    else:
        last_scan = PendingOperation.objects.filter(
            related_backup_operation=OuterRef("pk")
        ).order_by("-completed_time_ms")
        due_operations = not_queued_operations.annotate(
            next_start=(
                Subquery(last_scan.values("completed_time_ms")[:1])
                + F("min_time_between_backups_ms")
            )
        )
        time_now = now_epoch_ms()
        logger.debug(
            "Querying for due backups (%i):  %s",
            time_now,
            due_operations.values_list("name", "last_run_time_ms", "next_start"),
        )
        due_operations = due_operations.filter(next_start__lte=time_now)
    logger.debug("produce_due_backup_operations due_operations: %s", due_operations)

    for op in due_operations:
        p = PendingOperation(
            operation_type=PendingOperation.TYPE_BACKUP, related_backup_operation=op
        )
        p.dump_operation_data({"trigger": "produce_due_backup_operations"})
        p.save()


def create_pending_operation_for_backup(op: BackupOperation):
    """
    This really just exists to make the app a little more responsive.
    """
    if (
        op.backup_storage.backup_on_insertion
        and not op.pendingoperation_set.filter(completed_time__isnull=True).exists()
    ):
        p = PendingOperation(
            operation_type=PendingOperation.TYPE_BACKUP, related_backup_operation=op
        )
        p.dump_operation_data({"trigger": "create_pending_operation_for_backup"})
        p.save()
