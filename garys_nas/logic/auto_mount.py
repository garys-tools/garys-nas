# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import os
import pathlib
import uuid

from django.db.models import Q

from garys_nas.models.storage_block import StorageBlock
from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from nas_sensors import BlockSubType, may_need_root

logger = logging.getLogger(__name__)

MOUNT_ROOT = pathlib.Path("/mnt/garys_nas")


def mount_backup_storage(s: StorageBlock, dev_path: pathlib.Path = None) -> bool:
    changes_made = False
    if not s.mounted_root_directory:
        logger.info("Auto mounting device: %s", s)
        if s.auto_mount_location:
            p = pathlib.Path(s.auto_mount_location)
        else:
            p = MOUNT_ROOT / s.name
            s.auto_mount_location = str(p)
            s.save()

        if p.exists() and (p.is_mount() or os.listdir(p)):
            rand_path = str(uuid.uuid4())
            p = MOUNT_ROOT / rand_path
            s.auto_mount_location = str(p)
            s.save()

        if not p.exists():
            logger.info("Creating mount point:  %s", p)
            p.mkdir(parents=True, exist_ok=True)
        if s.mounted_root_directory:
            raise ValueError(f"Device {s} is already mounted")

        s.mount(p, dev_path)

        changes_made = True
    return changes_made


@may_need_root
def auto_mount():
    """
    Auto mount devices that are due to be mounted.  The auto_mount flag
    on a storage block will enable automatic mounting.

    The following rules selects which luks devices to unlock:
    * Present luks devices with auto mount children, or
    * Present luks devices with no known children (as children may have
      been disowned by previous disk changes).

    The following rules selects which devices to mount:
    * A present partition that is not mounted with auto mount enabled.

    """
    sync_storage_blocks()

    # List encrypted devices that are not open.  Open devices are then
    # mounted in the query below this one.

    luks_devices = StorageBlock.objects.filter(
        Q(children_set__auto_mount=True) | Q(children_set__isnull=True),
        sub_type=BlockSubType.luks.value,
        present=True,
    )
    logger.info("Checking luks devices: %s", luks_devices)
    luks_devices = luks_devices.exclude(
        children_set__present=True,
    )
    logger.info("Checking luks devices with no children: %s", luks_devices)
    closed_devices = luks_devices
    for s in closed_devices:
        mapper = s.mapper_dev_path
        if mapper.exists():
            # lsblk will not list an open crypt device unless it is mounted.
            c = s.children_set.get()
            if c.mounted_root_directory:
                logger.info("Mapper device is already mounted %s", c)
            else:
                logger.info("Storage %s is open but not mounted. Mounting %s", s, c)
                mount_backup_storage(c, dev_path=mapper)
        else:
            try:
                s.open_storage()
            except (RuntimeError, ValueError) as e:
                logger.debug(
                    "Failed to open %s, likely a missing key.  Message: %s", s, e
                )

    # List devices with a backup storage object that has a mount path.
    auto_mount_devices = StorageBlock.objects.filter(
        present=True,
        auto_mount=True,
    )
    logger.info("Auto Mounting: %s", auto_mount_devices)
    for s in auto_mount_devices:
        mount_backup_storage(s)
