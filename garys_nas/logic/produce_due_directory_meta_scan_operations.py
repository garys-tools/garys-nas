# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from django.db.models import Subquery, OuterRef

from garys_nas.models import PendingOperation, Directory
from garys_nas.time_conversion import now_epoch_ms, HOURS_24_IN_MS


def produce_due_directory_meta_scan_operations():
    """
    Add a meta scan request
    """
    last_scan = PendingOperation.objects.filter(
        related_directory=OuterRef("pk")
    ).order_by("-completed_time_ms")

    not_queued_directories = Directory.objects.exclude(
        pendingoperation__operation_type=PendingOperation.TYPE_DIRECTORY_SCAN,
        pendingoperation__completed_time__isnull=True,
    )

    due_directories = not_queued_directories.annotate(
        next_start=Subquery(last_scan.values("completed_time_ms")[:1]) + HOURS_24_IN_MS
    ).filter(next_start__lte=now_epoch_ms())

    for op in due_directories:
        p = PendingOperation(
            operation_type=PendingOperation.TYPE_DIRECTORY_SCAN, related_directory=op
        )
        p.save()
