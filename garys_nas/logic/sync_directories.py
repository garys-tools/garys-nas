# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.models.storage_block import StorageBlock
from garys_nas.models.directory import Directory
from garys_nas.models.path import Path


logger = logging.getLogger(__name__)


def sync_directories():
    """
    This ensures all directories are mapped to known path objects.

    Ensures directory consistency by:
    * Removing paths with a different maj_min to the directory
    * Linking subdirectories where directories exist
    * Creating OS Path objects for subdirectories with no paths
    """

    # Check paths are still valid
    for d in Directory.objects.all():
        d.os_paths.remove(*list(d.os_paths.exclude(maj_min=d.storage_block.maj_min)))

    # Link paths that are not root paths (root paths are linked by sync_storage_blocks)
    sub_paths = list(
        Path.objects.filter(maj_min__isnull=False).exclude(directory__relative_path="/")
    )
    for p in sub_paths:
        try:
            associated_block = StorageBlock.objects.get(maj_min=p.maj_min)
            block_root_dir = associated_block.root_directory
            block_root_path = block_root_dir.os_path()
            relative_path = f"/{p.as_path.relative_to(block_root_path.as_path)}"
            relative_directory = associated_block.directory_set.filter(
                relative_path=str(relative_path)
            )
            if relative_directory.exists():
                d = relative_directory.get()
                d.os_paths.add(p)
            else:
                logger.debug(
                    "No directory found for %s: %s", associated_block, relative_path
                )
                p.delete()
        except StorageBlock.DoesNotExist:
            logger.warning("No Storage Block exists for %s maj_min %s", p, p.maj_min)

    unlinked_directories = list(
        Directory.objects.exclude(relative_path="/").filter(os_paths__isnull=True)
    )
    for s in unlinked_directories:
        block_root_dir = s.storage_block.mounted_root_directory
        if block_root_dir:
            absolute_path = s.absolute_from(block_root_dir.os_path().as_path)
            sub_paths = Path.objects.filter(absolute_path=str(absolute_path))
            if sub_paths.exists():
                p = sub_paths.get()
            else:
                p = Path(absolute_path=str(absolute_path))
                p.save()
            s.os_paths.add(p)
