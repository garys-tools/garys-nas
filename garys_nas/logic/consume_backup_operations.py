# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import warnings

from garys_nas.events.signals import backup_completed
from garys_nas.logic import auto_mount
from garys_nas.models.backup_operation import BackupOperation
from garys_nas.models.db_lock import db_lock, DBLock
from garys_nas.models.pending_operations import PendingOperation
from garys_nas.time_conversion import now_datetime
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


@may_need_root
def consume_backup_operations(force=False):
    with db_lock(DBLock.LOCK_BACKUP):
        try:
            auto_mount()

            if force:
                # This is not really used and the access to op.start_time is inconsistent.
                warnings.warn("Forcing of backups will be removed", DeprecationWarning)
                available_backups = BackupOperation.objects.filter(
                    backup_storage__storage__present=True,
                    backup_storage__pause_backup=False,
                )
                for backup_op in available_backups:
                    pending_ops = list(
                        backup_op.pendingoperation_set.filter(
                            operation_type=PendingOperation.TYPE_BACKUP,
                            started_time__isnull=True,
                            completed_time__isnull=True,
                        )
                    )
                    for op in pending_ops:
                        op.started_time = now_datetime()
                        op.save()
                    backup_op.run()
                    for op in pending_ops:
                        op.completed_time = now_datetime()
                        op.save()
                    backup_completed.send(sender=backup_op)
            else:
                pending_backup_operations = PendingOperation.objects.filter(
                    operation_type=PendingOperation.TYPE_BACKUP,
                    started_time__isnull=True,
                    related_backup_operation__backup_storage__pause_backup=False,
                    related_backup_operation__backup_storage__storage__present=True,
                    related_backup_operation__enabled=True,
                )
                for op in pending_backup_operations:
                    backup_op = op.related_backup_operation
                    # noinspection PyUnusedLocal
                    ret_code = None
                    with op.executing(raise_exceptions=False):
                        backup_op.run()
                        ret_code = backup_op.last_ret_code
                        if ret_code != 0:
                            raise RuntimeError(
                                f"Backup {backup_op} failed to run: returned {ret_code}"
                            )
                    if ret_code == 0:
                        backup_completed.send(sender=backup_op)
                    else:
                        # Pause backups due to an error
                        if backup_op.disable_on_error:
                            backup_op.enabled = False
                            backup_op.save()
        except RuntimeError as e:
            logger.error("Backups failed to run: %s", e)
