# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.logic.auto_mount import auto_mount
from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.models.pending_operations import PendingOperation
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


@may_need_root
def consume_format_operations():
    """
    Execute auto format operations
    """
    sync_storage_blocks()
    changes_made = False
    # List devices with a backup storage object that has a mount path.
    format_operations = PendingOperation.objects.filter(
        operation_type=PendingOperation.TYPE_FORMAT,
        started_time__isnull=True,
        related_storage_block__present=True,
    )
    for op in format_operations:
        logger.info("Format Operation found and being processed")
        with op.executing():
            storage = op.related_storage_block
            data = op.load_operation_data()
            format_type = data["format_selection"]
            device_labels = [x.strip() for x in data["device_labels"].split("\n")]
            ret = storage.apply_storage_format(format_type, device_labels)
            if ret:
                data["ret"] = ret
                op.dump_operation_data(data)
        changes_made = True

    if changes_made:
        auto_mount()
