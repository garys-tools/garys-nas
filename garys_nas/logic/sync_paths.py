# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from nas_sensors import may_need_root, st_dev_to_maj_min
from garys_nas.models import Path


@may_need_root
def sync_paths():
    """
    Ensures path consistency by:
    * Updating all maj_min values of the Path objects
    """
    paths_to_update = []
    deleted_paths = []

    for p in Path.objects.all():
        if p.as_path.exists():
            maj_min = st_dev_to_maj_min(p.as_path.stat().st_dev)
            p.maj_min = maj_min
            paths_to_update.append(p)
        else:
            deleted_paths.append(p)

    Path.objects.bulk_update(paths_to_update, ("maj_min",))
    for p in deleted_paths:
        p.delete()
