# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
There are a number of functions related to the models that just do not
seem to fit well with the model classed.  I prefer that a model class
focuses on modelling the behaviour or entity that the model is designed
around, not just being an arbitrary namespace.  This is more than just
an MVC type application as there is background functionality and events
That need processing on the backend.

This is the home of that logic.
"""
from garys_nas.logic.auto_mount import auto_mount
from garys_nas.logic.consume_backup_operations import consume_backup_operations
from garys_nas.logic.consume_directory_scan_operations import (
    consume_directory_scan_operations,
)
from garys_nas.logic.consume_eject_operations import consume_eject_operations
from garys_nas.logic.consume_format_operations import consume_format_operations
from garys_nas.logic.consume_init_block_operations import consume_init_block_operations
from garys_nas.logic.produce_auto_eject_operations import produce_auto_eject_operations
from garys_nas.logic.produce_due_backup_operations import (
    produce_due_backup_operations,
    create_pending_operation_for_backup,
)
from garys_nas.logic.produce_due_directory_meta_scan_operations import (
    produce_due_directory_meta_scan_operations,
)
from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.logic.sync_directories import sync_directories
from garys_nas.logic.sync_paths import sync_paths

__all__ = [
    "auto_mount",
    "consume_backup_operations",
    "consume_directory_scan_operations",
    "consume_eject_operations",
    "consume_format_operations",
    "consume_init_block_operations",
    "produce_auto_eject_operations",
    "produce_due_backup_operations",
    "produce_due_directory_meta_scan_operations",
    "create_pending_operation_for_backup",
    "sync_storage_blocks",
    "sync_directories",
    "sync_paths",
]
