# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.logic.sync_paths import sync_paths
from garys_nas.logic.sync_directories import sync_directories
from garys_nas.logic.sync_storage_blocks import sync_storage_blocks
from garys_nas.models.pending_operations import PendingOperation
from nas_sensors import may_need_root

logger = logging.getLogger(__name__)


@may_need_root
def consume_directory_scan_operations():
    """
    Scans a directory and updates associated metadata while also
    checking consistency of the associated path objects.
    """
    sync_storage_blocks()
    for op in PendingOperation.objects.filter(
        operation_type=PendingOperation.TYPE_DIRECTORY_SCAN,
        started_time__isnull=True,
        related_directory__storage_block__present=True,
    ):
        d = op.related_directory
        logger.info("Initial scan of %s starting", d)
        with op.executing(raise_exceptions=False):
            # Check path consistency
            if not d.os_paths.all().exists():
                sync_directories()

            if not d.is_in_sync():
                sync_paths()

            # Scan Directory
            d.update_meta_data()

    delete_ops = list(
        PendingOperation.objects.filter(
            operation_type=PendingOperation.TYPE_DIRECTORY_SCAN,
            started_time__isnull=True,
            related_directory__isnull=True,
        )
    )
    for d in delete_ops:
        logger.info(
            "Deleting pending operation that has lost its directory link: %s", d
        )
        d.delete()
