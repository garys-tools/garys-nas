# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging

from garys_nas.logic import sync_storage_blocks
from nas_actions import umount, close_storage
from nas_sensors import may_need_root, BlockSubType

from garys_nas.models.storage_block import StorageBlock


logger = logging.getLogger(__name__)


@may_need_root
def clean_up_bad_removal():
    """
    It appears that if a LUKS device is removed without being closed
    properly, the mounted filesystem and mapper device can stay hanging,
    with the removed device no longer appearing in lsblk.  This function
    checks any auto mounted locations for a spurious maj_min value that
    needs to be removed as well as a mapper device.
    """
    sync_storage_blocks()

    # Check if a mapper dev exists for an absent block
    dev_paths = [
        s.mapper_dev_path
        for s in StorageBlock.objects.filter(sub_type=BlockSubType.luks, present=False)
    ]
    for p in dev_paths:
        if p and p.exists():
            logger.info("Discovered a DEV path for an absent block:  %s", p)
            umount(p)
            # Close
            close_storage(p.name)
