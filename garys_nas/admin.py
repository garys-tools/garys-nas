# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
from django.contrib import admin

from garys_nas.forms.storage_block_form import StorageBlockForm
from garys_nas.models import (
    StorageBlock,
    PhysicalDevice,
    Directory,
    BackupStorage,
    BackupOperation,
    Path,
    PendingOperation,
    Key,
    DBLock,
)


class AvailableTreeRootsFilter(admin.SimpleListFilter):
    """
    This is a very inefficient filter to process as the
    filtering needs to be done in memory and can't
    currently be done in the database easily (unless I
    used something like MPTT assuming it supports a DAG).
    """

    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = "Present Tree Roots"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "tree_roots"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            StorageBlock.objects.filter(root=True, present=True)
            .values_list("id", "name")
            .order_by("name")
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = self.value()
        if v:
            root = StorageBlock.objects.get(id=v)
            return queryset.filter(id__in=[x.id for x in root.connected_nodes])
        else:
            return queryset


class PresentParentsFilter(admin.SimpleListFilter):
    """
    This is a very inefficient filter to process as the
    filtering needs to be done in memory and can't
    currently be done in the database easily (unless I
    used something like MPTT assuming it supports a DAG).
    """

    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = "Present Parent"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "present_parent"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return sorted(
            set(
                StorageBlock.objects.filter(
                    present=True, children_set__isnull=False
                ).values_list("id", "name")
            )
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        v = self.value()
        if v:
            return queryset.filter(parents=v)
        else:
            return queryset


class StorageBlockAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "present",
        "maj_min",
        "file_system_uuid",
        "partition_uuid",
        "label",
        "type",
        "sub_type",
        "parent_names",
        "children_names",
        "mapper_dev_path",
        "size_bytes",
        "percent_used",
        "percent_reserved",
        "used_gib",
        "reserved_gib",
        "disk_free_gib",
        "used_for_backups",
        "is_sparse",
        "auto_mount",
        "mounted_root_directory",
        "available_to_format",
        "tree_roots_admin",
        "connected_nodes_admin",
        "is_luks",
        "inside_luks_container",
        "is_luks_open",
    )
    list_filter = (
        AvailableTreeRootsFilter,
        PresentParentsFilter,
        "present",
        "auto_mount",
        # "root",
        # "parents",
        "type",
        "sub_type",
    )
    form = StorageBlockForm


class PhysicalDeviceAdmin(admin.ModelAdmin):
    fields = (
        "block",
        "user_label",
        "user_comment",
        "lock_values",
    )
    list_display = (
        "block",
        "user_label",
        "user_comment",
        "partition_table_type",
        "partition_table_uuid",
        "model",
        "serial_number",
    )


class DirectoryAdmin(admin.ModelAdmin):
    fields = (
        "storage_block",
        "relative_path",
        "os_paths",
    )
    list_display = (
        "os_path",
        "storage_block",
        "file_count",
        "file_size",
        "disk_usage",
    )


class PathAdmin(admin.ModelAdmin):
    fields = ("absolute_path", "maj_min")
    list_display = ("absolute_path", "maj_min")


class BackupOperationAdmin(admin.ModelAdmin):
    fields = (
        "enabled",
        "backup_storage",
        "source",
        "name",
        "description",
        "local_destination",
        "type",
        "min_time_between_backups_ms",
        "last_stdout",
        "last_stderr",
        "last_ret_code",
        "disable_on_error",
    )
    list_display = (
        "name",
        "enabled",
        "last_ret_code",
        "current_backup_duration_s",
        "backup_storage",
        "source",
        "local_destination",
        "type",
        "min_time_between_backups_ms",
        "next_run_time_dt",
        "last_run_time_dt",
        "last_run_duration_h_m_s",
        "last_transfer_size",
        "last_time_since_backup_ms",
    )


class BackupStorageAdmin(admin.ModelAdmin):
    fields = (
        "storage",
        "description",
        "auto_eject",
        "backup_on_insertion",
        "pause_backup",
    )
    list_display = fields


class PendingOperationAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "state",
        "operation_type",
        "related_backup_operation",
        "related_storage_block",
        "related_directory",
        "elapsed_time",
    )
    list_filter = ("operation_type", "started_time", "completed_time")


class KeyAdmin(admin.ModelAdmin):
    list_display = ("__str__", "description", "related_storage_block", "external_key")


admin.site.register(StorageBlock, StorageBlockAdmin)
admin.site.register(PhysicalDevice, PhysicalDeviceAdmin)
admin.site.register(Directory, DirectoryAdmin)
admin.site.register(BackupStorage, BackupStorageAdmin)
admin.site.register(Key, KeyAdmin)
admin.site.register(DBLock)
admin.site.register(BackupOperation, BackupOperationAdmin)
admin.site.register(Path, PathAdmin)
admin.site.register(PendingOperation, PendingOperationAdmin)
