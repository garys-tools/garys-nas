==========
Gary's NAS
==========

Introduction
============

This is an opinionated project, that is, one that manages my own NAS
and related backups in a very specific way.  The goal of the project
was to help automate my backups and data transfers, which is what
this project focuses on.

I use this at home for my own purposes and am posting it here in case
anyone else might find it as a useful reference.

Status
======

While I use this in production, the UI is mostly driven using a standard
Django admin with some dirty hacks.  I will eventually create a more
user friendly UI so that my family can help maintain our off-site backups.

Key Features
============

* A simple dashboard reviewing NAS health
* Automatic linking to Cockpit if this is found locally
* Definition of multiple backup storage devices and backup schemes for
  those devices
* A significant lack of documentation

Documentation
=============

The documentation can be explores in the docs folder.  This also includes
my own thoughts and project planning.  Unfortunately, I do not have any
useful how-to information as I plan to do this once I built a better UI.

License
=======

GPL-3.0-or-later
