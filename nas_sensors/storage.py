# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import dataclasses
import enum
import json
import logging
import subprocess
from typing import Optional, List, Iterator, Dict, ClassVar

logger = logging.getLogger(__name__)


class MountPoint:
    """
    As multiple BlockNodes can reference the same mount point,
    this is a simple chain class used to connect blocks.
    """

    _mount_points: Dict[str, "MountPoint"] = {}

    def __new__(cls, *args, **kwargs):
        name = args[0]
        block = args[1]
        if name in cls._mount_points:
            obj = cls._mount_points[name]
            obj.blocks.append(block)
        else:
            obj = super().__new__(cls)
            obj.__init__(*args, **kwargs)
            cls._mount_points[name] = obj
        assert obj.name is not None
        return obj

    def __init__(self, name, block):
        self.name = name
        self.blocks = [block]

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<MountPoint: {self.name}>"

    @classmethod
    def all_mount_points(cls) -> Iterator["MountPoint"]:
        for p in cls._mount_points.values():
            yield p

    @classmethod
    def reset_mount_points(cls):
        cls._mount_points = {}


class BlockType(str, enum.Enum):
    """
    :cvar physical: This indicates a physical device
    :cvar virtual:  This is a device that is arbitrarily created like a
                   ISO or COW file, ram drive, or loop-back
    :cvar partition:  This is part of a device
    """

    physical = "physical"
    virtual = "virtual"
    partition = "partition"


class BlockSubType(str, enum.Enum):
    unknown = "unknown"

    # Physical Sub Types
    # These are not planned for use and are just added for completeness
    rotational = "rotational"
    solid_state = "solid_state"

    # Virtual Sub Types
    # These are not really used
    loop = "loop"
    ram = "ram"
    # file = "file"

    # Partition Sub Types
    partition = "partition"
    lvm_physical = "lvm_physical"
    lvm_vg = "lvm_vg"
    lvm_lv = "lvm_lv"
    luks = "luks"
    md_raid = "md_raid"
    dm_unknown = "dm_unknown"

    # Filesystems
    squashfs = "squashfs"
    vfat = "vfat"
    exfat = "exfat"
    xfs = "xfs"
    swap = "swap"
    ext2 = "ext2"
    ext4 = "ext4"
    ntfs = "ntfs"

    # noinspection SpellCheckingInspection
    @classmethod
    def get_part_subtype(cls, n: dict) -> Optional["BlockSubType"]:
        """
        Returns the subtype of a partition by inspecting the block properties
        """
        # physical_partition_types = ("gpt", "dos")
        physical_partition_type_ids = ("0x5",)

        if n[LsblkKeys.SERIAL] and n[LsblkKeys.ROTATIONAL] in (True, "1"):
            return cls.rotational
        elif n[LsblkKeys.SERIAL] and n[LsblkKeys.ROTATIONAL] in (False, "0"):
            return cls.solid_state
        # if n[LsblkKeys.PART_TYPE] in physical_partition_types:
        #     return cls.physical
        elif n[LsblkKeys.TYPE] == LsblkTypes.lvm and n[LsblkKeys.FS_TYPE]:
            return cls.lvm_lv
        elif n[LsblkKeys.KNAME][:4] == "zram":
            return cls.ram
        elif n[LsblkKeys.FS_TYPE] in FS_MAP:
            return FS_MAP[n[LsblkKeys.FS_TYPE]]
        elif n[LsblkKeys.KNAME][:2] == "dm":
            return cls.dm_unknown
        elif n[LsblkKeys.PART_TYPE_UUID] in physical_partition_type_ids:
            return cls.partition
        else:
            logger.warning(f"Do not know how to class: %s", n)
            return cls.unknown


FS_MAP = {
    "crypto_LUKS": BlockSubType.luks,
    "LVM2_member": BlockSubType.lvm_physical,
    "linux_raid_member": BlockSubType.md_raid,
    "squashfs": BlockSubType.squashfs,
    "vfat": BlockSubType.vfat,
    "exfat": BlockSubType.exfat,
    "xfs": BlockSubType.xfs,
    "swap": BlockSubType.swap,
    "ext2": BlockSubType.ext2,
    "ext4": BlockSubType.ext4,
    "ntfs": BlockSubType.ntfs,
}


# noinspection SpellCheckingInspection
class LsblkKeys:
    """
    This is a namespace for keys used in the lsblk return data.  See
    lsblk --help for field details.
    """

    # General Fields
    CHILDREN = "children"
    SIZE = "size"
    STATE = "state"
    NAME = "name"
    KNAME = "kname"
    MAJ_MIN = "maj:min"
    UUID = "uuid"  # This appears to be the file system uuid
    TYPE = "type"
    DEV = "maj:min"
    LABEL = "label"
    MOUNTPOINT = "mountpoint"  # lsblk < 2.37.2
    # MOUNTS = "mountpoints" # lsblk >= 2.37.2 not supported on NAS.
    # PATH = "path" # lsblk >= 2.37.2

    # Disk Fields
    MODEL = "model"
    SERIAL = "serial"
    HOTPLUG = "hotplug"
    ROTATIONAL = "rota"

    # Partition Fields
    FS_TYPE = "fstype"
    PART_UUID = "partuuid"
    # PART_TYPE = "pttype" # lsblk >= 2.37.2
    PART_TYPE_UUID = "parttype"
    PARTITION_TABLE_NAME = "partlabel"

    # UDEV Keys
    PART_TABLE_TYPE = "ID_PART_TABLE_TYPE"
    PART_TABLE_UUID = "ID_PART_TABLE_UUID"


# noinspection SpellCheckingInspection
class LsblkTypes:
    crypt = "crypt"
    disk = "disk"
    loop = "loop"
    lvm = "lvm"
    part = "part"
    raid0 = "raid0"
    raid5 = "raid5"
    raid10 = "raid10"

    # Calculated Types
    ZRAM = "zram"

    virtual_types = (loop, ZRAM)
    partition_types = (crypt, part, lvm, raid5, raid10, raid0)

    @staticmethod
    def get_blk_type(n: dict) -> str:
        kname = n[LsblkKeys.KNAME]
        if "zram" in kname:
            return LsblkTypes.ZRAM
        else:
            t = n[LsblkKeys.TYPE]
            assert t in dir(LsblkTypes)
            return t


@dataclasses.dataclass
class BlockNode:
    """
    Only fields that have a purpose will be retrieved.

    :cvar fs_uuid:  The file system or volume uuid
    :cvar part_uuid:  The partition UUID
    :cvar part_table_type:  The type of partition table the disk is formatted with
    :cvar part_table_uuid:  The partition table uuid
    :cvar root:  This is useful for generating graph data
    :cvar parents:  This is needed to link storage devices and understand connections
    :cvar name:  This is useful to identify the device internally
    :cvar kernel_name:  This is may be needed to further identify subtypes
    :cvar maj_min:  This is used for mapping paths to devices
    :cvar label:  This is another useful way to identify the device
    :cvar type:  Device type is needed for tracking later
    :cvar sub_type:  Sub type is needed to identify relevant admin commands are needed
                    to manage the device
    :cvar mounts:  This is needed to connect backups of physical devices to logical storage.  It is
                  only ever None on instantiation but will be assigned in the class factory method.
    :cvar size_bytes:  This is needed to track assets and plan expansions
    """

    fs_uuid: Optional[str]
    model: Optional[str]
    serial: Optional[str]
    part_uuid: Optional[str]
    part_table_type: Optional[str]
    part_table_uuid: Optional[str]
    root: bool
    parents: List["BlockNode"]
    children: List["BlockNode"]
    name: str
    kernel_name: str
    maj_min: str
    label: Optional[str]
    type: BlockType
    sub_type: BlockSubType
    mounts: Optional[List[MountPoint]]
    size_bytes: int

    defined_blocks: ClassVar[Dict[str, "BlockNode"]] = {}

    @staticmethod
    def session_id_from_node_data(node_type: str, node: dict) -> str:
        """
        Returns an ID that should be safe to use for this session but
        is not stable long term
        """

        if node_type in [
            LsblkTypes.crypt,
            LsblkTypes.lvm,
            LsblkTypes.raid0,
            LsblkTypes.raid5,
            LsblkTypes.raid10,
        ]:
            return node[LsblkKeys.UUID]
        elif node_type == LsblkTypes.disk:
            return "__".join([node[LsblkKeys.MODEL], node[LsblkKeys.SERIAL]])
        elif node_type == LsblkTypes.part:
            return node[LsblkKeys.PART_UUID] or node[LsblkKeys.UUID]
        elif node_type == LsblkTypes.ZRAM:
            return node[LsblkKeys.KNAME]
        else:
            raise ValueError(f"Unsupported block type {node_type}.")

    def __str__(self):
        return (
            f"BlockNode({self.kernel_name}, "
            f"{self.maj_min}, "
            f"FSUUID {self.fs_uuid}, "
            f"P_UUID {self.part_uuid}, "
            f"{len(self.parents)} parents, "
            f"{len(self.children)} children, "
            f"{self.size_bytes//1024**3}GiB, "
            f"{self.type}, "
            f"{self.sub_type})."
        )

    @classmethod
    def reset(cls):
        cls.defined_blocks = {}

    @classmethod
    def from_ls_blk(cls, node: dict, parent=None) -> Optional["BlockNode"]:
        """
        Creates the block node or appends parents if the node already
        exists.  This function also ensures consistency of the graph.

        Mutation warning:  This mutates the ``parent.children`` and
        indirectly the MountPoint globals.
        """
        # General Fields
        node_type = LsblkTypes.get_blk_type(node)
        if node_type == LsblkTypes.loop:
            # Loop devices are not supported at the moment.
            return None

        # Determine Block Type
        if node_type == LsblkTypes.disk:
            block_type = BlockType.physical
        elif node_type in LsblkTypes.virtual_types:
            block_type = BlockType.virtual
        elif node_type in LsblkTypes.partition_types:
            block_type = BlockType.partition
        else:
            raise NotImplementedError(f"Unknown node type {node_type}")

        # Augment node data
        if block_type == BlockType.physical:
            info = udev_info(f"/dev/{node[LsblkKeys.KNAME]}")
            node[LsblkKeys.PART_TABLE_TYPE] = info.get(LsblkKeys.PART_TABLE_TYPE)
            node[LsblkKeys.PART_TABLE_UUID] = info.get(LsblkKeys.PART_TABLE_UUID)

        session_id = cls.session_id_from_node_data(node_type, node)

        if session_id in cls.defined_blocks:
            obj: BlockNode = cls.defined_blocks[session_id]
            existing_mounts = set(x.name for x in obj.mounts)
            # block_mounts = set(node[LsblkKeys.MOUNTS])
            block_mounts = set(
                [node[LsblkKeys.MOUNTPOINT]] if node[LsblkKeys.MOUNTPOINT] else []
            )
            assert (
                block_mounts == existing_mounts
            ), f"Node {session_id} mounts are not consistent across the graph"
            assert (
                parent
            ), f"Expected a repeated block in lsblk will always have a parent {obj}"
            obj.parents.append(parent)
        else:
            obj = cls(
                root=parent is None,
                fs_uuid=node[LsblkKeys.UUID],
                model=node[LsblkKeys.MODEL],
                serial=node[LsblkKeys.SERIAL],
                part_uuid=node.get(LsblkKeys.PART_UUID),
                part_table_type=node.get(LsblkKeys.PART_TABLE_TYPE),
                part_table_uuid=node.get(LsblkKeys.PART_TABLE_UUID),
                parents=[parent] if parent else [],
                children=[],
                name=node[LsblkKeys.NAME],
                kernel_name=node[LsblkKeys.KNAME],
                maj_min=node[LsblkKeys.MAJ_MIN],
                label=node[LsblkKeys.LABEL] or node[LsblkKeys.PARTITION_TABLE_NAME],
                type=block_type,
                sub_type=BlockSubType.get_part_subtype(node),
                mounts=None,
                size_bytes=int(node[LsblkKeys.SIZE]),
            )
            # mounts = [MountPoint(x, obj) for x in node[LsblkKeys.MOUNTS]]
            if node[LsblkKeys.MOUNTPOINT]:
                mounts = [MountPoint(node[LsblkKeys.MOUNTPOINT], obj)]
                obj.mounts = mounts
            else:
                obj.mounts = []
            cls.defined_blocks[session_id] = obj

        if parent:
            parent.children.append(obj)

        return obj


def ls_block() -> dict:
    # Development Debugging purposes
    # import pathlib
    # debug_path = (
    #     pathlib.Path(__file__).parent.parent
    #     / "docs"
    #     / "design"
    #     / "lsblk"
    #     / "thonas.json"
    # )
    # with debug_path.open("r") as f:
    #     return json.load(f)
    out = subprocess.run(
        ["lsblk", "-J", "-b", "-O"], check=True, stdout=subprocess.PIPE
    ).stdout.decode()
    return json.loads(out)


def udev_info(dev_path: str) -> dict:
    # noinspection SpellCheckingInspection
    out = subprocess.run(
        ["udevadm", "info", "--query", "property", "--name", dev_path],
        check=True,
        stdout=subprocess.PIPE,
    ).stdout.decode()
    k_v = [x.split("=", maxsplit=1) for x in out.split("\n") if x]
    # noinspection PyTypeChecker
    return dict(k_v)


# noinspection SpellCheckingInspection
def _collect_data_nodes(
    _parent: BlockNode = None, _blocks: List[dict] = None
) -> Iterator[BlockNode]:
    """
    This is a recursive generator, it is assumed the lslk data is not
    too deep.  The purpose of this is to create nodes while also flattening
    the structure.

    .. ATTENTION::
        This is a generator but must be completely iterated through for
        the BlockNode graph to be fully resolved.
    """

    if _parent is None:
        ls = ls_block()
        assert len(ls) == 1, "Assume only blockdevices key"
        blocks = ls["blockdevices"]
    else:
        blocks = _blocks

    for node in blocks:
        b = BlockNode.from_ls_blk(node, parent=_parent)
        if b:
            yield b
        else:
            continue
        children = node.get(LsblkKeys.CHILDREN)
        if children:
            for c in _collect_data_nodes(b, _blocks=children):
                yield c


def collect_data_nodes() -> List[BlockNode]:
    # Reset global counters
    MountPoint.reset_mount_points()
    BlockNode.reset()
    return list(_collect_data_nodes())


# noinspection SpellCheckingInspection,PyUnusedLocal
def _plot_node_graph(nodes: Dict[str, BlockNode]):
    """
    For Dev purposes only.  Although, the result is pretty good so far,
    the next step will be to write my own layout code that allows better
    sizing and spacing so more data can fit in each node (also colour
    code nodes etc...).  The following code has some interesting examples:
    https://stackoverflow.com/questions/29586520/can-one-get-hierarchical-graphs-from-networkx-with-python-3/29597209#29597209

    https://plantuml.com/component-diagram
    """
    # This code block is not maintained but exists as a future reference
    # ##################################################################
    # import matplotlib.pyplot as plt
    # import networkx as nx
    #
    # # noinspection PyUnresolvedReferences
    # import pydot
    # from networkx.drawing.nx_pydot import graphviz_layout
    #
    # g = nx.DiGraph()
    # g.add_nodes_from([(n.kernel_name, {"type": n.type}) for n in nodes.values()])
    # # noinspection PyProtectedMember
    # g.add_nodes_from([n.name for n in MountPoint._mount_points.values()])
    #
    # for n in nodes.values():
    #     g.add_edges_from([(n.kernel_name, nodes[c].kernel_name) for c in n.children])
    #
    # # noinspection PyProtectedMember
    # for m in MountPoint._mount_points.values():
    #     g.add_edges_from([(n.kernel_name, m.name) for n in m.blocks])
    #
    # plt.figure(figsize=(40, 10))  # Size in Inches
    # # nx.draw(
    # #     g,
    # #     with_labels=True,
    # #     node_size=1600,
    # #     node_shape="s",
    # #     pos=nx.spring_layout(g, k=0.5, iterations=20),
    # # )
    # # To Explore:
    # # https://stackoverflow.com/questions/57512155/how-to-draw-a-tree-more-beautifully-in-networkx
    # pos = graphviz_layout(g, prog="dot")
    # # pos = nx.random_layout(g)
    # nx.draw(
    #     g,
    #     with_labels=True,
    #     node_size=1600,
    #     node_shape="s",
    #     pos=pos,
    # )
    # plt.show()


def main():
    for n in collect_data_nodes():
        print(n)

    # noinspection PyProtectedMember
    for m in MountPoint._mount_points.values():
        print(f"{repr(m)} has {len(m.blocks)} blocks")

    # all_nodes = {n.uuid: n for n in collect_data_nodes()}
    # _plot_node_graph(all_nodes)


if __name__ == "__main__":
    main()
