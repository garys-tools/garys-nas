# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
def st_dev_to_maj_min(v: int) -> str:
    """
    Converts an os.stat.st_dev value into a major/minor string

    https://docs.huihoo.com/doxygen/linux/kernel/3.7/include_2linux_2types_8h.html
    https://www.kernel.org/doc/Documentation/admin-guide/devices.txt
    https://www.oreilly.com/library/view/linux-device-drivers/0596000081/ch03s02.html

    A kernel dev_t is a 32 bit unsigned integer (value returned by st_dev)
    where the minor value is an unsigned 8 bit number (0 to 255).

    """
    v_bytes = v.to_bytes(4, byteorder="big", signed=False)
    minor = v_bytes[3]
    major = int.from_bytes(v_bytes[0:3], byteorder="big", signed=False)
    return f"{major}:{minor}"


def may_need_root(function_to_decorate):
    def wrapper_function(*args, **kwargs):
        try:
            return function_to_decorate(*args, **kwargs)
        except PermissionError:
            raise PermissionError(
                "There was a permission error running a function, this call "
                "will need super user access to function."
            )

    return wrapper_function
