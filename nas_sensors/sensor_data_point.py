# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import decimal
from typing import NamedTuple, Union

from nas_sensors import constants
from nas_sensors.constants import Units


class SensorDataPoint(NamedTuple):
    value: Union[int, float, decimal.Decimal]
    timestamp: int
    units: Units


class SensorDataPointBytes:
    value: int
    timestamp: int
    units: Units
    valid_units = [Units.B, Units.KiB, Units.MiB, Units.GiB, Units.TiB]

    def __init__(self, value: int, timestamp: int):
        self.value = value
        self.timestamp = timestamp
        self.units = Units.B

    def as_units(self, units: Units) -> SensorDataPoint:
        return SensorDataPoint(
            value=bytes_as_units(self.value, units),
            timestamp=self.timestamp,
            units=units,
        )


def choose_size_units(bytes_size: int) -> constants.Units:
    """
    Given a value in bytes, chooses a size unit to display in
    """
    cut_off = bytes_size // 2
    if cut_off < constants.KiB:
        return constants.Units.B
    elif cut_off < constants.MiB:
        return constants.Units.KiB
    elif cut_off < constants.GiB:
        return constants.Units.MiB
    elif cut_off < constants.TiB:
        return constants.Units.GiB
    else:
        return constants.Units.TiB


def bytes_as_units(bytes_size: int, bytes_units: constants.Units) -> decimal.Decimal:
    """
    Convert the bytes value into a different units without lost precision
    """
    val = decimal.Decimal(bytes_size)

    if bytes_units == constants.Units.B:
        return val
    elif bytes_units == constants.Units.KiB:
        return val / constants.KiB
    elif bytes_units == constants.Units.MiB:
        return val / constants.MiB
    elif bytes_units == constants.Units.GiB:
        return val / constants.GiB
    elif bytes_units == constants.Units.TiB:
        return val / constants.TiB
    else:
        raise NotImplementedError
