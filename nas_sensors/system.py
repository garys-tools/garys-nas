# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import decimal
import functools
from typing import Optional, TypedDict, List, Dict

import psutil

from garys_nas.time_conversion import now_epoch_ms
from nas_sensors.constants import Units
from nas_sensors.sensor_data_point import SensorDataPointBytes, SensorDataPoint


class MemoryData(TypedDict):
    total: SensorDataPointBytes
    available: SensorDataPointBytes
    unavailable: SensorDataPointBytes
    free: SensorDataPointBytes
    used: SensorDataPointBytes
    cached: Optional[SensorDataPointBytes]


class LoadData(TypedDict):
    cpu_1_min_avg: SensorDataPoint
    cpu_5_min_avg: SensorDataPoint
    cpu_15_min_avg: SensorDataPoint


GenericData = Dict[str, SensorDataPoint]


memory_descriptions = {
    "total": "Total physical RAM",
    "available": "Quantity that can be allocated instantly to a process",
    "unavailable": "This is total-available",
    "free": "Completely unused / zeroed memory",
    "used": "Memory in use, not the same as total - free",
    "cached": "OS Cache",
}

load_descriptions = {
    "cpu_1_min_avg": "1 minute average percent of all CPUs",
    "cpu_5_min_avg": "5 minute average percent of all CPUs",
    "cpu_15_min_avg": "15 minute average percent of all CPUs",
}


physical_memory_fields = ("total", "available", "unavailable", "free", "used", "cached")
swap_memory_fields = ("total", "free", "used")
memory_usage_fields = ("unavailable", "available")
load_fields = ("cpu_1_min_avg", "cpu_5_min_avg", "cpu_15_min_avg")


def get_current_physical_memory_stats() -> MemoryData:
    t = now_epoch_ms()
    mem_data = psutil.virtual_memory()
    return MemoryData(
        total=SensorDataPointBytes(mem_data.total, t),
        available=SensorDataPointBytes(mem_data.available, t),
        unavailable=SensorDataPointBytes(mem_data.total - mem_data.available, t),
        free=SensorDataPointBytes(mem_data.free, t),
        used=SensorDataPointBytes(mem_data.used, t),
        cached=SensorDataPointBytes(mem_data.cached, t),
    )


def get_current_swap_stats() -> MemoryData:
    t = now_epoch_ms()
    mem_data = psutil.swap_memory()
    return MemoryData(
        total=SensorDataPointBytes(mem_data.total, t),
        available=SensorDataPointBytes(mem_data.free, t),
        unavailable=SensorDataPointBytes(mem_data.total - mem_data.free, t),
        free=SensorDataPointBytes(mem_data.free, t),
        used=SensorDataPointBytes(mem_data.used, t),
        cached=None,
    )


def get_current_load_averages() -> LoadData:
    t = now_epoch_ms()
    load_data = psutil.getloadavg()
    cpu_count = psutil.cpu_count()
    return LoadData(
        cpu_1_min_avg=SensorDataPoint(
            value=decimal.Decimal(load_data[0]) / cpu_count * 100,
            timestamp=t,
            units=Units.percent,
        ),
        cpu_5_min_avg=SensorDataPoint(
            value=decimal.Decimal(load_data[1]) / cpu_count * 100,
            timestamp=t,
            units=Units.percent,
        ),
        cpu_15_min_avg=SensorDataPoint(
            value=decimal.Decimal(load_data[2]) / cpu_count * 100,
            timestamp=t,
            units=Units.percent,
        ),
    )


@functools.lru_cache
def get_system_temperature_fields() -> List[str]:
    return list(sorted(get_system_temperatures().keys()))


def get_system_temperatures() -> GenericData:
    t = now_epoch_ms()
    temps = psutil.sensors_temperatures()
    results = {}
    for sys_name, sensors in temps.items():
        for s in sensors:
            sensor_name = f"{sys_name} - {s.label}"
            results[sensor_name] = SensorDataPoint(
                value=s.current, timestamp=t, units=Units.celsius
            )
    return results
