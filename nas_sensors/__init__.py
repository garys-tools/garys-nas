# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
A consistent Interface for retrieving NAS statistics
"""
from nas_sensors.storage import (
    collect_data_nodes,
    MountPoint,
    BlockType,
    BlockSubType,
    BlockNode,
)
from nas_sensors.utilities import st_dev_to_maj_min, may_need_root
from nas_sensors.networking import is_tcp_port_open

__all__ = [
    "collect_data_nodes",
    "MountPoint",
    "BlockType",
    "BlockSubType",
    "BlockNode",
    "st_dev_to_maj_min",
    "is_tcp_port_open",
    "may_need_root",
]
