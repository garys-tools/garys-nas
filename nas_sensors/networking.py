# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import enum
import socket
from socket import AddressFamily
from typing import TypedDict, List, Dict

import psutil


class NicAddressFamily(enum.Enum):
    IPv4 = "IPv4"
    IPv6 = "IPv6"
    MAC = "MAC"


class NetworkInterfaceAddress(TypedDict):
    address: str
    network: str
    broadcast: str
    family: NicAddressFamily


class NetworkInterface(TypedDict):
    name: str
    is_up: bool
    full_duplex: bool
    speed_MB: int
    mtu_b: int
    addresses: List[NetworkInterfaceAddress]


# noinspection PyProtectedMember
def _map_nic_stat(
    name: str,
    nic_data: psutil._common.snicstats,
    addresses: List[NetworkInterfaceAddress],
) -> NetworkInterface:
    return NetworkInterface(
        name=name,
        is_up=nic_data.isup,
        full_duplex=nic_data.duplex == psutil.NIC_DUPLEX_FULL,
        speed_MB=nic_data.speed,
        mtu_b=nic_data.mtu,
        addresses=addresses,
    )


# noinspection PyProtectedMember
def _map_nic_addr(nic_data: psutil._common.snicaddr) -> NetworkInterfaceAddress:
    if nic_data.family == AddressFamily.AF_INET:
        f = NicAddressFamily.IPv4
    elif nic_data.family == AddressFamily.AF_INET6:
        f = NicAddressFamily.IPv6
    elif nic_data.family == psutil.AF_LINK:
        f = NicAddressFamily.MAC
    else:
        raise NotImplementedError(nic_data)

    return NetworkInterfaceAddress(
        address=nic_data.address,
        network=nic_data.netmask,
        broadcast=nic_data.broadcast,
        family=f,
    )


def get_network_interface_data() -> Dict[str, NetworkInterface]:
    interfaces = psutil.net_if_stats()
    addresses = psutil.net_if_addrs()
    ret = {}
    for i_name, i_data in interfaces.items():
        i_addr = [_map_nic_addr(x) for x in addresses.get(i_name, [])]
        ret[i_name] = _map_nic_stat(i_name, i_data, i_addr)
    return ret


def is_tcp_port_open(host: str, port: int) -> bool:
    """
    The function checks if a requested TCP Port is open.  This has an
    aggressive timeout as it is assumed the test is for a service that is
    reasonable local.
    """

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.settimeout(1.0)
        r = s.connect_ex((host, port))
        if r == 0:
            return True
        else:
            return False
