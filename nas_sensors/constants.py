# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import enum

# https://psutil.readthedocs.io/en/latest/

BYTE = 1
KiB = BYTE * 1024
MiB = KiB * 1024
GiB = MiB * 1024
TiB = GiB * 1024


class Units(enum.Enum):
    B = "B"
    KiB = "KiB"
    MiB = "MiB"
    GiB = "GiB"
    TiB = "TiB"

    percent = "%"

    celsius = "℃"


class SystemSensors:
    TOTAL_PHYSICAL_MEMORY = "Total Physical Memory"
    PHYSICAL_MEMORY_AVAILABLE = "Physical Memory Available"
    PHYSICAL_MEMORY_USED = "Physical Memory Used"
    PHYSICAL_MEMORY_CACHED = "Physical Memory Used as Cache"

    SWAP_TOTAL = "Total Swap"
    SWAP_USED = "Swap Used"
    SWAP_FREE = "Swap Free"

    CPU_COUNT = "Number of CPUs"
    # CPU_x_NAME = "CPU_%i"
    # CPU_NAME = "CPU_%i"
    CPU_LOAD_1MIN_AVG = "CPU Load 1 Minute Average"
    CPU_LOAD_5MIN_AVG = "CPU Load 5 Minute Average"
    CPU_LOAD_15MIN_AVG = "CPU Load 15 Minute Average"
