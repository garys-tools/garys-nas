#!/usr/bin/env python
# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
"""Django's command-line utility for administrative tasks."""
import logging
import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "garys_nas_site.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(f"Application exception {e}")
        logging.exception("Exception")
        import garys_nas.clean_up

        garys_nas.clean_up.clean_up("Exception", None)
        raise
