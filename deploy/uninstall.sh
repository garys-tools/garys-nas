#!/bin/bash

BASE_ENV=./base_env.sh
if [ ! -f $BASE_ENV ]; then
  echo "This script must be called next to the base_env.sh file."
  exit 1
fi

. ./base_env.sh

echo "Warning:  This script will delete all installation and user files of this package."

read -p "Type DELETE or KEEP_VAR to continue:" confirm

if [[ $confirm == "DELETE" ]]; then
  echo "Deleting all data"
elif [[ $confirm == "KEEP_VAR" ]]; then
  echo "Keeping data files"
else
  echo "Aborting"
  exit 1
fi

echo "Stopping services"
sudo systemctl stop $PACKAGE_NAME
sudo systemctl disable $PACKAGE_NAME
sudo rm /etc/systemd/system/$PACKAGE_NAME.service
sudo systemctl stop $PACKAGE_NAME-scheduler
sudo systemctl disable $PACKAGE_NAME-scheduler
sudo rm /etc/systemd/system/$PACKAGE_NAME-scheduler.service
sudo rm /etc/systemd/system/$PACKAGE_NAME-add@.service
sudo rm /etc/udev/rules.d/80-$PACKAGE_NAME.rules

sudo udevadm control --reload

# Delete paths
echo "Delete paths."
if [[ $confirm == "DELETE" ]]; then
sudo --preserve-env=PACKAGE_ROOT,PACKAGE_VAR,PACKAGE_LOG,PROVIDER_ROOT,PROVIDER_VAR_ROOT bash <<EOF
  echo "Removing $PACKAGE_ROOT"
  rm -fr $PACKAGE_ROOT
  echo "Removing $PACKAGE_VAR"
  rm -fr $PACKAGE_VAR
  if [ -z "$(ls -A $PROVIDER_ROOT)" ]; then
    echo "Removing $PROVIDER_ROOT"
    rm -fr $PROVIDER_ROOT
  fi
  if [ -z "$(ls -A $PROVIDER_VAR_ROOT)" ]; then
    echo "Removing $PROVIDER_VAR_ROOT"
    rm -fr $PROVIDER_VAR_ROOT
  fi
EOF
elif [[ $confirm == "KEEP_VAR" ]]; then
sudo --preserve-env=PACKAGE_ROOT,PACKAGE_VAR,PACKAGE_LOG,PROVIDER_ROOT bash <<EOF
  echo "Removing $PACKAGE_ROOT"
  rm -fr $PACKAGE_ROOT
  echo "Keeping $PACKAGE_VAR"
  if [ -z "$(ls -A $PROVIDER_ROOT)" ]; then
    echo "Removing $PROVIDER_ROOT"
    rm -fr $PROVIDER_ROOT
  fi
EOF
fi


echo "Remove Users."
sudo --preserve-env=PACKAGE_USER bash <<EOF
  id -u "$PACKAGE_USER" &>/dev/null && userdel "$PACKAGE_USER"
EOF
