#!/bin/bash

# ./install.sh /home/gary/src/garys_nas/
INIT_DIR=$(pwd)
GIT_PATH=$1

if [ -z "$GIT_PATH" ]; then
  GIT_PATH="https://gitlab.com/garys-tools/garys-nas.git"
fi

echo "Git Path is '$GIT_PATH'"

BASE_ENV=./base_env.sh
if [ ! -f $BASE_ENV ]; then
  echo "This script must be called next to the base_env.sh file."
  exit 1
fi

. ./base_env.sh

echo "This script will require sudo priveleges and may ask for a password multiple times"

echo "Installing dependencies."
sudo --preserve-env=PACKAGE_ROOT,GIT_PATH bash <<EOF
  dnf install -yq python3 python3-pip sqlite git
EOF

echo "Creating users."
sudo --preserve-env=PACKAGE_USER,PACKAGE_ROOT bash <<EOF
  id -u "$PACKAGE_USER" &>/dev/null || useradd -rMd "$PACKAGE_ROOT" "$PACKAGE_USER"
EOF

echo "Creating paths."
sudo --preserve-env=PACKAGE_ROOT,PACKAGE_VAR,PACKAGE_LOG bash <<EOF
  mkdir -p $PACKAGE_ROOT
  mkdir -p $PACKAGE_VAR
  mkdir -p $PACKAGE_LOG
EOF

cd $PACKAGE_ROOT

echo "Cloning."
sudo --preserve-env=PACKAGE_ROOT,GIT_PATH,PACKAGE_USER_BIN bash <<EOF
  cd $PACKAGE_ROOT
  git clone "$GIT_PATH" .
  git checkout production
EOF

if [ $? -ne 0 ]; then
  echo "Something went wrong".
  exit 1
fi


echo "Setting permissions."
cd $INIT_DIR
./set_perms.sh

echo "Installing Poetry."
# Install poetry as the garys_nas user only - I do not feel comfortable
# running this as root.
sudo --preserve-env=PACKAGE_USER,PACKAGE_USER_LOCAL,PACKAGE_USER_CACHE bash <<EOF
  mkdir -p $PACKAGE_USER_LOCAL
  mkdir -p $PACKAGE_USER_CACHE
  chown $PACKAGE_USER:$PACKAGE_USER $PACKAGE_USER_LOCAL
  chown $PACKAGE_USER:$PACKAGE_USER $PACKAGE_USER_CACHE
EOF

sudo --preserve-env=PACKAGE_ROOT,GIT_PATH -u $PACKAGE_USER bash <<EOF
  curl -sSL https://install.python-poetry.org | python3.10 -
EOF
if [ $? -ne 0 ]; then
  echo "Something went wrong".
  exit 1
fi

echo "Creating VENV."
sudo --preserve-env=PACKAGE_ROOT,POETRY_PATH  -u $PACKAGE_USER bash <<EOF
  cd $PACKAGE_ROOT
  $POETRY_PATH install
EOF
if [ $? -ne 0 ]; then
  echo "Something went wrong with poetry".
  exit 1
fi

echo "Locking the VENV."
sudo --preserve-env=PACKAGE_USER,PACKAGE_USER_LOCAL,PACKAGE_USER_CACHE bash <<EOF
  chown -R root:$PACKAGE_USER $PACKAGE_USER_LOCAL
  chown -R root:$PACKAGE_USER $PACKAGE_USER_CACHE
EOF


echo "Providing an initial configuration."
cd $PACKAGE_ROOT
VENV_PATH=`sudo -u $PACKAGE_USER $POETRY_PATH env info -p`
if [ $? -ne 0 ]; then
  echo "Something went wrong with poetry".
  exit 1
fi
if [ -z "$VENV_PATH" ]; then
  echo "Failed to find VENV";
  exit 1
else
  echo "VENV PATH '$VENV_PATH'"
fi
ACTIVATION_PATH=$VENV_PATH/bin/activate
export ACTIVATION_PATH
cd $PACKAGE_ROOT/deploy
sudo --preserve-env=POETRY_PATH,ACTIVATION_PATH,PACKAGE_VAR,PACKAGE_LOG,PACKAGE_USER ./run_conf.sh

echo "Init app."
cd $PACKAGE_ROOT
sudo --preserve-env=ACTIVATION_PATH,POETRY_PATH  -u $PACKAGE_USER bash <<EOF
  . $ACTIVATION_PATH
  . _load_env.sh
  ./scripts/run_migrate.sh
  python manage.py init_app
EOF

echo "Creating Systemd Services"

# This is the main webserver
sudo --preserve-env=PACKAGE_NAME,PACKAGE_USER,PACKAGE_ROOT bash <<EOF
echo "[Unit]
Description=Gary Nas Webserver
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=$PACKAGE_USER
WorkingDirectory=$PACKAGE_ROOT
ExecStart=$PACKAGE_ROOT/serve.sh

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/$PACKAGE_NAME.service
EOF

# This is a background task that maintains the database
sudo --preserve-env=PACKAGE_NAME,PACKAGE_USER,PACKAGE_ROOT bash <<EOF
echo "[Unit]
Description=Gary Nas Scheduler
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
WorkingDirectory=$PACKAGE_ROOT
ExecStart=$PACKAGE_ROOT/run_scheduler.sh

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/$PACKAGE_NAME-scheduler.service
EOF

# Add Device
sudo --preserve-env=PACKAGE_NAME,PACKAGE_USER,PACKAGE_ROOT bash <<EOF
echo "[Unit]
Description=Gary Nas Hardware Update
BindTo=%i.device
After=%i.device
StopWhenUnneeded=yes

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
WorkingDirectory=$PACKAGE_ROOT
ExecStart=$PACKAGE_ROOT/scripts/on_add_device.sh
ExecStop=$PACKAGE_ROOT/scripts/on_rem_device.sh

" > /etc/systemd/system/$PACKAGE_NAME-add@.service
EOF

UDEV_RULES="\
SUBSYSTEM==\"block\", ACTION==\"add\", ENV{ID_TYPE}=\"disk\", ENV{DEVTYPE}==\"disk\", \
PROGRAM=\"/usr/bin/systemd-escape -p --template=$PACKAGE_NAME-add@.service \$env{DEVNAME}\", \
ENV{SYSTEMD_WANTS}+=\"%c\"
"
echo "$UDEV_RULES" | sudo tee /etc/udev/rules.d/80-$PACKAGE_NAME.rules > /dev/null

sudo udevadm control --reload

sudo systemctl enable $PACKAGE_NAME
sudo systemctl enable $PACKAGE_NAME-scheduler

echo "Run sudo systemctl start $PACKAGE_NAME"
echo "Run sudo systemctl start $PACKAGE_NAME-scheduler"
