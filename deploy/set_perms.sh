#!/bin/bash

BASE_ENV=./base_env.sh
if [ ! -f $BASE_ENV ]; then
  echo "This script must be called next to the base_env.sh file."
  exit 1
fi

. ./base_env.sh

sudo --preserve-env=PACKAGE_USER,PACKAGE_ROOT,PACKAGE_VAR,PACKAGE_STATIC bash <<EOF
  mkdir -p $PACKAGE_STATIC

  chown -R root:$PACKAGE_USER $PACKAGE_ROOT
  chown -R $PACKAGE_USER:$PACKAGE_USER $PACKAGE_STATIC
  chmod -R 0640 $PACKAGE_ROOT
  find $PACKAGE_ROOT -type d -exec chmod u+rx,g+rx,o+rx {} \;

  chmod 0740 $PACKAGE_ROOT/scripts/run_backups.sh
  chmod 0740 $PACKAGE_ROOT/scripts/run_root_tasks.sh
  chmod 0750 $PACKAGE_ROOT/scripts/run_quick_scan.sh
  chmod 0750 $PACKAGE_ROOT/scripts/run_migrate.sh

  chmod 0740 $PACKAGE_ROOT/run_scheduler.sh
  chmod 0740 $PACKAGE_ROOT/scripts/on_add_device.sh
  chmod 0740 $PACKAGE_ROOT/scripts/on_rem_device.sh
  chmod 0750 $PACKAGE_ROOT/serve.sh

  chmod 0750 $PACKAGE_ROOT/deploy/*

  chown -R $PACKAGE_USER:$PACKAGE_USER $PACKAGE_VAR
  chmod -R 0640 $PACKAGE_VAR
  find $PACKAGE_VAR -type d -exec chmod u+rx,g+rx,o+rx {} \;

EOF

