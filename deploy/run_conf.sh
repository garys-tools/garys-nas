#!/bin/bash
# This can be run in dev or via sudo as part of install
if [ -z "$ACTIVATION_PATH" ]; then
  echo "No activation path has been provided, will search in cwd.";
  VENV_PATH=`$POETRY_PATH env info -p`
  ACTIVATION_PATH=$VENV_PATH/bin/activate
  export ACTIVATION_PATH
else
  echo "Using $ACTIVATION_PATH"
fi
. $ACTIVATION_PATH
echo "Running initial_configuration"
python ./initial_configuration.py
