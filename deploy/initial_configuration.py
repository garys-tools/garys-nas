# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Why not just use setup.py?

This is an interim stage.  My preference is always to be running from a
VENV and never actually integrate into the system environment.  When I
get a chance to understand how this deployment works and how others do it,
then this will be remodelled.  For now, it's a simple process.
"""
import grp
import os
import pathlib
import pwd
import socket
import stat
from typing import Optional

BASE_DIR = pathlib.Path(__file__).parent.parent
CONF_DIR = BASE_DIR / "conf"


def create_env_vars(
    conf_path: pathlib.Path,
    secret_key: str,
    host_name: str,
    sqlite_path: pathlib.Path,
    bind_ip_address: Optional[str],
    listen_port: Optional[int],
    poetry_path: pathlib.Path,
    log_folder: pathlib.Path,
    static_root: pathlib.Path,
    package_user: Optional[str],
):
    """
    This procedure populates all the required environment variables
    into an env.vars file in the conf folder.
    """
    secret_key_path = conf_path / "secret.key"
    env_path = conf_path / "env.vars"

    var = {
        "SECRET_KEY": secret_key_path,
        "HOST_NAME": host_name,
        "SQLITE_DB": sqlite_path,
        "BIND_IP_ADDRESS": bind_ip_address,
        "LISTEN_PORT": listen_port,
        "ACTIVATION_PATH": poetry_path,
        "LOG_PATH": log_folder,
        "STATIC_ROOT": static_root,
        # "NAS_LOG_LEVEL": "INFO",
        # "NAS_DEBUG": "False",
    }
    with env_path.open("w") as f:
        for k, v in var.items():
            if v is not None:
                f.write(f"export {k}=${{{k}:-{v}}}\n")
    with secret_key_path.open("w") as f:
        f.write(secret_key)

    os.chmod(env_path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP)
    os.chmod(secret_key_path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP)
    if package_user:
        uid = pwd.getpwnam("root").pw_uid
        gid = grp.getgrnam(package_user).gr_gid
        os.chown(env_path, uid, gid)
        os.chown(secret_key_path, uid, gid)


def generate_secret_key() -> str:
    from django.core.management.utils import get_random_secret_key

    return get_random_secret_key()


if __name__ == "__main__":

    def main():

        host_name = socket.getfqdn(socket.gethostname())
        user_input = input(f"Specify host_name [{host_name}]: ")
        if user_input:
            host_name = user_input

        user_input = input(f"Specify bind address [127.0.0.1]: ")
        if user_input:
            bind_address = user_input
        else:
            bind_address = "127.0.0.1"

        user_input = input(f"Specify listen port [8080]: ")
        if user_input:
            listen_port = user_input
        else:
            listen_port = 8080

        # Get data from current environment
        if "PACKAGE_VAR" in os.environ:
            var_root = os.environ["PACKAGE_VAR"]
            print(f"Found PACKAGE_VAR: {var_root}")
            assert var_root
            var_root = pathlib.Path(var_root)
        else:
            var_root = BASE_DIR / "var"
        var_root.mkdir(parents=True, exist_ok=True)

        # Get data from current environment
        if "PACKAGE_USER" in os.environ:
            user = os.environ["PACKAGE_USER"]
            print(f"Found PACKAGE_USER: {user}")
            assert user
        else:
            user = None

        if "PACKAGE_LOG" in os.environ:
            log_root = os.environ["PACKAGE_LOG"]
            print(f"Found PACKAGE_LOG: {log_root}")
            assert log_root
            log_root = pathlib.Path(var_root)
        else:
            log_root = var_root / "log"
        log_root.mkdir(parents=True, exist_ok=True)

        if "PACKAGE_STATIC" in os.environ:
            static_root = os.environ["PACKAGE_STATIC"]
            print(f"Found PACKAGE_STATIC: {static_root}")
            assert static_root
            static_root = pathlib.Path(var_root)
        else:
            static_root = BASE_DIR / "static"
        static_root.mkdir(parents=True, exist_ok=True)
        CONF_DIR.mkdir(parents=True, exist_ok=True)

        poetry_path = os.environ["ACTIVATION_PATH"]
        assert poetry_path
        poetry_path = pathlib.Path(poetry_path)
        assert poetry_path.exists()

        create_env_vars(
            CONF_DIR,
            secret_key=generate_secret_key(),
            host_name=host_name,
            sqlite_path=var_root / "sqlite.db",
            bind_ip_address=bind_address,
            listen_port=listen_port,
            poetry_path=poetry_path,
            log_folder=log_root,
            package_user=user,
            static_root=static_root,
        )

    main()
