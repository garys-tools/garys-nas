=================
Storage Modelling
=================

Overview
========

* StorageBlock:  Represents a block device, mirroring the OS.
* Directory:  Represents a directory on a block device, it always stays
              connected to the block devices and exists even when the block
              device is removed.  This is not a mounted path, but a directory
              of interest on the block device.
* Path:  These are OS Paths, that is, paths seen when browsing the operating system.
         These exist to help track and map directories to the OS.

Model
=====

Physical Structures
-------------------

Primary types
* Physical disks (HDD, SSD)
* Virtual disks (ram, zip)
* partition (part of a physical disk)

Technologies
* LVM
* MD
* DM

Combinations
* Linear / Standard
* Tree - Disk with standard partitions
* DAG - RAID: disk to parts to volume
* LVM - similar to raid

Sub Types
* Physical: magnetic, solid_state
* Virtual: ram, file
* Partitions:  physical, lvm-physical, lvm-vg, lvm-lv, luks, md-raid

Getting Information
-------------------

Associated commands:
* lsblk <-- GOLD
* lsblk -o NAME,KNAME,FSTYPE,MOUNTPOINT,LABEL,UUID,PARTLABEL,PARTUUID,RA,RO,RM,MODEL,SERIAL,SIZE,STATE,MODE,ALIGNMENT,ROTA,TYPE,PKNAME,HCTL,REV,VENDOR
* MD health data can be retrieved from SYSFS
* psutil partitions
* LVM2:  pvs, vgs, lvs
* Device Mapper: dmsetup ls, dmsetup info
* https://github.com/truveris/py-mdstat/blob/master/mdstat/__init__.py
* sudo mdadm --detail /dev/md126

Additional notes:  Some RAID bad block data in sysfs/rd0 folders

https://unix.stackexchange.com/questions/28636/how-to-check-mdadm-raids-while-running

lsblk
-----

This produces a json output that is worth modelling.

Data Representation
-------------------

Refer to sample lsblk outputs (lsblk -JbO).

See storage.BlockNode


Development Notes
=================

Check advise for using lsblk here:

https://man7.org/linux/man-pages/man8/lsblk.8.html#:~:text=lsblk%20lists%20information%20about%20all,types%20from%20the%20block%20device.

Combine with psutil.