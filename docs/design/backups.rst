=======
Backups
=======

Model
=====

Previous backups were taken as either an rsync operation (a filesystem snapshot),
or a tar.gz operation (an archive).  Backups were defined based on the external
storage device and path that contained the backup copy as well as which folders
were included as part of a backup.  A single storage device could backup
multiple locations using a mixture of rsync or tar.bz2.

If required, the external storage devices would be decrypted prior to the backup
as well as an attempt is performed to check how much space is needed for a backup.

As such, two models are needed:  One whose scope is to monitor and manage
the backup storage device and another that is to manage the backups stored
on that device.


Key Issues
==========

.. ATTENTION::
    Backups should not begin unless all source devices are properly mounted.  This
    has led to accidental deletion of all backup data.

Configuration
=============

.. NOTE::
    Backups are always constrained to a single filesystem

Preconditions:

* A source directory object needs to exist and be assigned an os path.

This is an interim configuration using the django admin and scripts.

1.  Create a Backup Storage object in the admin and configure
2.  Create a Backup operation object in the admin

Notes:

* min_time_between_backups_ms:  This is a very simplistic form of scheduling,
  with more advanced scheduling not needed at the moment (but could be added in
  the future).


Workflows
=========

Now that a successful backup has been taken and the foundations are in
place, I need to take a minute to consider workflows.

Configure/Create a new backup storage device
--------------------------------------------

Precondition:

* A Block device in installed with an empty GPT partition table.

Trigger:

* Block scan  (block scan can be scheduled and/or triggered off udev in the future), and
* a User action

User action details:

* User goes to a view and enables selects to configure the backup device from a
  list of templates (ntfs, ext4, luks, dual-partition)

Sequence:

* A background task re-formats device as per the user selected template
* For encrypted devices:  An automatically generated key is used
* A storage scan is performed to update the database
* backup storage classes are automatically created as per the template


Add/Remove/Download luks keys
-----------------------------

This is performed using Cockpit with the storage plugin.

The automatically generated key can be downloaded from the UI.

Execute a backup
----------------

Precondition:

* A backup operation has been configured
* The associated backup storage devices are present

Trigger:

* Operations polling - a background task that polls for available scheduled scans

Sequence:

* A background task polls for available backup operations that are due/overdue
  where the backup storage is present and mounted.
* On detection a backup is performed
* On completion post-backup actions are taken
