====
UUID
====

The UUID decision is challenging because every block device is different.

The following is a brief summary of how it works:

====================  ===============================================
 Device               Source
====================  ===============================================
Physical Disk         serial
LVM Node              uuid
Physical Partition    partuuid
MD RAID Device        uuid
LUKS Partition        uuid
FileSystem            uuid
====================  ===============================================
