=========
Decisions
=========


Tech Stack
==========

Running on the host:

* Needs access to host files, container info, etc...

Using Svelte, Bulma etc... as I've found this useful on my Lattice project.


Tracking data changes
=====================

Tracking changes on a file by file basis (without hashing, so not a byte by byte
basis) will still require a lot of RAM.

As such, tracking will be based on directories (see get_one_file_system_listing).

Deployment and Scheduling
=========================

A cron job would be the easy way out, but creating a scheduler using something like:

https://schedule.readthedocs.io/en/stable/

Could then morph into a system service and be more transparent to the OS.  This
is also more docker friendly.

For systemd support:

https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files

https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6

Caching Data
============

Data that can be easily retrieved from the host is being cached so that
not all functions of the app require SU access.  Examples of this data
include:

* The StorageBlock Model
* Directory Model (relative path)
* Path model and Path.maj_min

It is assumed that OS sync will require SU access.

