===========
Development
===========

Gitlab Overview
===============

This file is a quick reference on working with default GitLab configuration.

* Gitlab uses a feature branching model
* Assign an issue to yourself
* Use Gitlab to create a draft merge request and branch for the issue
* Checkout the latest production on local
* Work on the issue, ensuring te issue number is in the commits
* When ready to merge, update the draft merge request and push your commits to the
  automatically created branch on GitLab
* Comment / review / update the merge request
* Once the merge request has been approved, it will be merged in

Merge Checklist
===============

[ ] Are tests needed/created?
[ ] Has the version number been updated?


Branch flows
============

At the moment I am creating issue branches and merging these straight into
production.
