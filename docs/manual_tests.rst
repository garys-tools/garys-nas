============
Manual Tests
============

Backups
=======

Pass
Fail

[P] Run a backup where the backup destination has been mounted on top of - expected error
[P] Run a backup where the source path has been unmounted - expected error
[P] Run a backup where the backup destination has been unmounted - expected error
[P] Run a backup where the source has changed mount point - expect to find proper mount point (after rescanning)
[P] Run a backup where the destination has changed mount point - expect to find proper mount point (after rescanning)

Spot Check in Dev (tests are in sequence)
=========================================

Sequence 1:  Init the app
[P] Run `scripts/run_dev_migrate.sh` (or normal migrate) - expect db to be created and static files collected and admin login
[P] Run `serve.sh` - Expect web app to be responding
[P] Run `scripts/run_quick_scan.sh` - expect Storage Blocks to be populated without sizes (verify in web app)
[P] Run `run_scheduler.sh` - expect directories and paths to be created with meta data updated over time (see pending operations)

Preconditions:  Webserver and scheduler are running

Sequence 2:  Format a disk
[P] Insert a spare USB drive - expect to see storage blocks created (after scheduler restarts)
[P] Wipe spare drive with `parted /dev/sdxxx mklabel gpt` - expected to see storage blocks updated (after scheduler restarts)
[P] Delete absent partitions that no longer exist on drive using admin - expect block to now be available for format
[P] On the Empty SB device, in the admin, select format device as luks and provide a label - expect device to be formatted and mounted by scheduler

Sequence 3:  Creating a first backup
[P] Create a backup storage object in the admin and assign it a blank block and auto mount point
    and auto eject - expected device to be mounted with an OS path created.  Not expecting it to auto
    eject until after a backup is performed (wait for scheduler to verify)
[P] Create a backup operation for a small local directory (for texting) as a tar.gz2 - expected scheduler to run backup (verify in pending operations and on disk) and disk to be ejected
[P] Disable auto-eject and remove disk (wait for scan to confirm), reinsert - expected a backup to execute on insert (no eject)
[P] Change minimum backup time and wait - expecting scheduler to run an additional backup (verify in pending operations and on disk), with a short backup time it will continue to run backups.
[P] Disable auto-backup and continuous backups should stop.

Sequence backup_on_insertion:
[ ] Disable backup_on_insertion and insert drive - expect no pending operations created
[ ] Enable backup_on_insertion with drive inserted - expect pending operation and backup
[ ] Remove drive and re-insert - expect pending operation and backup

Dev Install Test
================

[P] Monitor logs, insert disk - expect udev operations to respond
[ ] Monitor logs, remove disk - expect udev operations to respond
[ ] Issue eject command through admin for mounted partition - expect partition to umount and disk
    to eject (for a single partition disk).

.. NOTE::
    Removal of a open luks drive leaves the mapper device in tact?  This must
    be a udev or os issue.  It seems to occur if mounted.  On reinsertion the
    previous mapper device just reports an error always.
