=================
Storage Templates
=================

Flat EXT4
=========

A flat EXT4 is a GPT disk with a single partition.  It requires one label
to be provided as the partition label.

Flat LUKS
=========

A flat LUKS is a GPT disk with a single partition that is encrypted with LUKS
and then formatted with ext4.  It requires one label to be provided as the partition label.
