===
WIP
===

.. ATTENTION::
    The development status is such that this only works for known real world
    use cases and that detection/classification logic is not intended to
    be able to resolve all possible situations.

High Level Roadmap
==================

[x]  0.1 Scaffolding:  Basic static web page presenting live health data
[...]  0.5 Establish backups as a manual operation
[ ]  0.6 Interact with backup operations via a UI
[ ]  Integrate backup configuration and supervision
[ ]  Capture health data
[ ]  Implement health warnings
[ ]  Create trend views of important information
[ ]  Create maintenance predictions based on historical data

Stories
=======
NOTE:  Cockpit takes over system administration, this app now focuses
more on storage reporting and backups.

As a NAS administrator:

[ ] I want to know the health status of my physical disks so that I feel confident that my data is safe.
    this includes bad sectors and other health parameters.
[ ] I want to forecast the health of my physical disks so that I can plan for expansion and maintenance
[ ] Note: Review old hbaut functions and look at MDADM expansion functions
[x] I want to track storage assets that may be offline or removed
[x] I want to configure backup schemes to protect my data
[x] I want to manage the addition, connection, and replacement of backup devices

As a NAS User:

[ ] I want to be able to facilitate backups
[ ] I want feedback on how much data is unprotected (hint:  pass timestamps to folder scan and
    add another return value that indicates files above that timestamp). Also:  Investigate
    size scanning issues.
[ ] I want to transfer my own data from my phone into predefined storage locations
[ ] I want to receive notifications of backup progress and completion.



Current Story
=============

Story (Move the story here)
---------------------------
As a NAS administrator:

[...] I want semi-automated backups to occur on my NAS


Story Tasks (move tasks here, move back when done)
--------------------------------------------------

[x] Determine how this will be initially deployed and executed to the NAS securely
[x] Determine a migration for when the NAS is rebuilt - how to preserve the app
[x] Modify the application in preparation of deployment - create deployment scripts.
[...] Deploy to the NAS
[ ] Perform the first full scan and backup
[ ] Trigger run_backups on udev - model off the old system
[ ] Migrate all backups and archive the old backup system


Misc Ideas
==========

[ ]  Add a time stats table from chronyc
[ ]  Add resolvectl output
[ ]  On backup completion, regenerate manifest files for backup drives and update drive usage (before eject)
[ ]  Build backup supervision views
[ ]  Build backup configuration view
[ ]  Check out os.statvfs for definitive disk usage data?
[ ]  Add a backup schedule
[ ]  Add a file field for upload/download of the encryption key in binary format as a convenience
[ ]  Create a live view of hardware status (no DB required, CPU, RAM, Networking, Storage)
[ ]  Create models to track represent basic hardware info
[ ]  Create data acquisition layer to update current status in DB with management command
     with status just being a couple of basics of hw info that properly decodes
     mem usage, cpu usage etc...  see psutil.
[ ]  Update deployment to consider running in a docker container, although I'm
     not sure how that will work with system monitoring and the permissions and
     file access this needs.
[ ] Move all env vars to the DB and set up DB overwrites that are updated on app init?

User Backup view notes:
* Configure storage to allow user eject, then have an eject button appear
  in the backup UI
* Flag devices in the UI to pause before backup on the next device detection
