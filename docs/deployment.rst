==========
Deployment
==========

Scope
=====

As this is under heavy development, the initial deployment will be someone
ad-hoc and git-based.  In the future, more formal deployment strategies
such as online hosting and delivery will be considered.

Procedure
=========

* Note that my NAS is running CentOS Stream 8 with a manually installed
  python3.10 symlinked into /usr/bin.
* For the above, I followed this guide: https://tecadmin.net/how-to-install-python-3-10-on-centos-rhel-8-fedora/
* download/copy deploy folder to the NAS as a wheel user.
* run `install.sh`
* Use cockpit to add 8080/tcp to the appropriate firewall zones

Minor Updates
-------------

sudo git pull
sudo systemctl restart garys_nas-scheduler
sudo systemctl restart garys_nas

Monitoring:

* journalctl --unit garys_nas-scheduler
* journalctl --unit garys_nas

Migration Updates
-----------------

sudo systemctl stop garys_nas-scheduler
sudo systemctl stop garys_nas
sudo git pull
sudo ./scripts/run_migrate.sh
sudo systemctl start garys_nas-scheduler
sudo systemctl start garys_nas

Architecture
============

File System
-----------

The application will be installed in /opt/gary/garys_nas as per:

https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/opt.html
https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard

For the moment an sqlite database is sufficient for the intended use
of this application.  This file will be stored in /var/opt/gary/garys_nas
along with log files.

Although configuration files are often stored in /etc/opt, I have not
seen this in practice with most web applications maintaining their
storage in the opt path.  The aim for this application is to configure
by environment variables and db variables to improve compatibility with
tools like docker.

Variables:
* PROVIDER_ROOT=/opt/gary
* PACKAGE_ROOT=/opt/gary/garys_nas
* PACKAGE_VAR=/var/opt/gary/garys_nas
* PACKAGE_LOG=/var/opt/gary/garys_nas/log

Package configuration will be stored in PACKAGE_ROOT/conf

Ownership
---------

users:  root, garys_nas
group:  garys_nas

PROVIDER_ROOT and PACKAGE_ROOT:
* will be root:garys_nas with 0640 permissions.
* Executable files will be 0740 or 0770 depending on who needs access

PACKAGE_VAR and PACKAGE_LOG will be garys_nas:garys_nas with 0640

Scheduling
----------

run_backups.sh:  This can run every five minutes and ideally also on a udev event.

run_quick_scan.sh: This can run every thirty minutes or on udev events?

run_root_tasks.sh:  This can run every five minutes and ideally also on a udev event.

serve.sh:  This will need a systemd script

Deployment
==========

Ideally deployment belongs in a separate project, but for now will be included
in this.

A bash automation script will be created to create the appropriate folders,
import dependencies, check out the repository, and initialise the application.


Backup
======

The application should be backed up simply by copying the PACKAGE_VAR contents and
PACKAGE_ROOT/conf paths.


UDEV Integration
================

Thank you!!
https://opensource.com/article/18/11/udev
https://wiki.archlinux.org/title/udev#Spawning_long-running_processes
https://blog.fraggod.net/2012/06/16/proper-ish-way-to-start-long-running-systemd-service-on-udev-event-device-hotplug.html
https://blog.fraggod.net/2015/01/12/starting-systemd-service-instance-for-device-from-udev.html

Quick notes investigating the final link above:
* "@" is used to indicate a service unit, something triggered internally (https://unix.stackexchange.com/questions/588629/why-do-some-unit-filenames-end-with)
* %U is the user manager UID (https://www.freedesktop.org/software/systemd/man/systemd.unit.html)
* %i is a string after "@" for an instantiated unit?  I think this just takes the $env bit as an argument
* SYSTEMD_WANTS %c?
* SYSTEMD_WANTS specifies a dependency
* %c - https://linux.die.net/man/8/udev.  I think what is happening here is that systemd-escape is
  dynamically creating a unit based on the template and the name of the unit is %c.
* Why remove does not work:  https://bugzilla.redhat.com/show_bug.cgi?id=871074

Useful command:
udevadm info --query=all --name=/dev/xvda
sudo udevadm monitor --env
sudo udevadm control --log-priority=debug
sudo udevadm control --log-priority=info
sudo udevadm control --reload