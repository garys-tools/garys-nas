==========
References
==========

This document captures useful references used for creating this project
or documentation.

`Restructured Text Basics <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_

`Licensing projects <https://www.fosslife.org/how-apply-license-your-open-source-project>`_

