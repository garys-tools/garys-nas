==========
Usage Tips
==========

Backups
=======

If a backup error occurs, the backup will be set to pause so that an investigate
can occur.  Auto unmounting won't occur as a result.

Disk Usage
==========

If directory usage is unusually high, the following command will report
the largest apparent files on disk::

    du -ax -BG --apparent-size / | sort -n -r | head -n 20


Connecting a forgotten or third party encrypted device
======================================================

Encryption keys are applied to the luks sub-type storage block.

To unlock one of these devices, perform the following:

* Insert the device and wait for it to be scanned
* Find the luks volume and create a key for that volume
* Wait for further scanning and the device will be opened and mounted
