========
Overview
========

Note
====

Be aware that multiprocess synchronisation makes a few assumptions,
mostly that no single operation (like a backup) will take longer than
24 hours.  If this is the case then extend the minimum backup time.

General
=======

This is a very opinionated NAS monitoring and management collection
of scripts.  It's the next level in the evolution of a series of
management scripts that I have created for about a decade.

This is also intended to work on other devices to monitor health metrics
that are either not using cockpit.  As a guiding rule, anything that can
be done with cockpit will not be replicated here.

Outcome
=======

To monitor, manage, and automate my NAS.  The NAS combines the following
functions to serve as a backbone to our home:

Primary Function
----------------

* Redundancy and performance:  a combination of direct disk, RAID5, and
  RAID10 storage
* Security:  Encrypted volumes with external decryption keys for security
* Peace of mind:  Backups

Why?
----

For our modern home, network storage is central to a technological life.
Cloud is not appropriate due to the volume of data we create and our
values in having control and ownership over our data.  I also need a
server to act as central access to the many projects I enjoy creating
and deploying.

Design Philosophies
===================

* Only do what needs to be done, avoid putting in things that might be nice.
* Ruthlessly remove functionality that is not used, memories can be archived in
  documents if so desired, but do not obscure primary documentation.
* Consciously model the entity in the appropriate format:  objects are objects,
  events are events.
* Keep things as small as necessary
* It is safe to assume that a user action has an intended consequence.  For example,
  when inserting a removable drive, it is okay for this to trigger a backup.
* Any automated action needs to have an explicit configuration option to disable it, but
  it can default to enabled

Tech Stack
==========

This is designed to run on the host directly.

Alternatives
============

There aren't any alternatives that focus on the core concepts of sensors,
health monitoring and backups.  Since I generally stick with CentOS or
Fedora it is important to note that the latest CentOS comes with Cockpit.

HOLD THE PRESS:  cockpit-storaged and cockpipt-networkmanager solve most
of my problems...  Although, I don't have the health data I want... but
what else do I need?  cockpit-podman will work great if I can move away
from docker containers

Before creating my own management chain, what else is out there?  Excluding
BSD based options as this will restrict VM and docker.

* QNAP - This seems to do most of what I want, although I'm concerned
  about automating some of the tasks (like backups).  Also, cost is a
  major issue, costing three times more than building my own.  This then
  pulls into question data access - if all data access is somewhat
  more proprietary then recovery can be an issue later (although probably
  a moot point).
* OpenMediaVault - Looks promising.  Ecosystem of plugins.  Luks will require
  web based lock / unlock, although should be able to automate.  Reasonable
  backup solutions, although would be easier just to create an online sync host.
  Good docker integration.

Considering how much I want to do, it doesn't necessarily make sense to
invest in creating a GUI for my NAS when there are NAS solutions out there.
However, there is a stubborn part of me that just doesn't want to.  The
reason is partly ownership, while also knowing that after all these years
I know exactly what I want.  Whatever path I take, QNAP, OMV, my own
solution, only I can maintain it as no one else I in my family at the moment
could take it over and adopt it.  Most of these other solutions are general,
they support all possibilities whereas I have a strong opinion about what
I want the system to do based on my experience.  I don't need all those
features and could make something much simpler and much more rigid.

Decision:  I'll write my own set of scripts / monitoring tools.

Why?  Because I want something my family can help maintain and existing
solutions are designed for IT admins, not general users.


Deployment
==========

This is deployed in PyCharm at the moment.  The remote is a CENTOS machine
and the deployment is to an administrative account (that has some restrictions).

Running
=======

Currently, this app is designed to run just on port 80 as a non-privileged
user using the "serve.sh" script.  Privileged commands will run as
management commands and will be documented below.

Architecture
============

Here is a good read that is worth considering, even at this low level:

https://www.cosmicpython.com/book/chapter_11_external_events.html

NAS Sensors
-----------

The intention is for a modular architecture with clear separation.

The main view offers a dynamic live view that constructs based on
available live information.  The structure for this view is:

[View] -> [Display Composition Logic] -> [Sensor Interface] -> [Sensor Logic]

Where historical data is available, the structure would be:

1) [Sensor Scanning] -> [Sensor Interface] -> [Compression] -> [Database]
2) [View] -> [Display Composition Logic] -> [Virtual Sensor Interface] -> [Database]


Backups
-------

Backup modellings requirements are briefly described in the backups design document.

This system is currently modelling the entities that are being tracked within
the operating system.  However, this is a model of Nouns which is appropriate
for data representation but these objects should not be overly coerced to
represent operations and actions.  Instead, events are intended to be modelled
as events and objects modelled as objects.  As such, the pending operations model
has been created as a queue (with history) to capture events within the system and
care must be taken that the objects are not over modelled on the events and that
events themselves are kept isolated.


Workflows and Events
====================

As this is intended to be an automated app, it might be worth understanding
what are some of the common workflows and events.  The app is becoming a little
more complex then first expected and so this review is to better understand
if it can be simplified.

Typical backup:
* Insert Drive
* Sys Blocks update
* App syncs with sys blocks
* Backup Device Detected
* Backup Process triggered
* Device Opened/Mounted (App syncs with sys blocks)
* Backup Performed
* Device Closed/Ejected (App syncs with sys blocks)

Detect hardware:
* UDEV, init, or poll event
* Sys Blocks update
* App syncs with sys blocks
* New hardware detected
* App scans devices and updates meta data

Update Metadata:
* init or poll event
* App scans devices and updates meta data

Format a new drive
* Insert drive
* mklabel gpt
* Sys Blocks update
* App syncs with sys blocks
* User sets drive template
* App formats device (App syncs with sys blocks)
* App mounts device (App syncs with sys blocks)

see cause_end_effect.ods which also unpacks this.
