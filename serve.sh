#!/bin/bash
# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
. _load_env.sh
. $ACTIVATION_PATH

BIND="${BIND_IP_ADDRESS:-127.0.0.1}"
PORT="${LISTEN_PORT:-8080}"

python manage.py migrate
daphne -v 0 -b $BIND -p $PORT garys_nas_site.asgi:application
