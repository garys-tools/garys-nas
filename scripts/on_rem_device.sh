#!/bin/bash
# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
. ./_load_env.sh
. $ACTIVATION_PATH
python ./manage.py trigger --rem_device
