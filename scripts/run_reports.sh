#!/bin/bash
# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
echo "This is expected to be called from the project root (./scripts/run..."

. ./_load_env.sh

sudo bash <<EOF
  . ./_load_env.sh
  . $ACTIVATION_PATH
  export NAS_LOG_LEVEL="DEBUG"
  python ./manage.py run_reports
EOF
