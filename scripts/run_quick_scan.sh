#!/bin/bash
# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
echo "This is expected to be called from the project root (./scripts/run..."

. ./_load_env.sh
. $ACTIVATION_PATH
python ./manage.py trigger --sync_blocks
