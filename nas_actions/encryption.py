# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This module captures encryption and decryption functions.
"""
import contextlib
import logging
import pathlib
import random
import subprocess
import tempfile

logger = logging.getLogger(__name__)


def generate_key() -> bytes:
    r = random.SystemRandom()
    return r.randbytes(2048)


@contextlib.contextmanager
def temporary_key_file(key_data: bytes) -> pathlib.Path:
    with tempfile.NamedTemporaryFile() as temp_f:
        temp_f.write(key_data)
        temp_f.flush()
        yield pathlib.Path(temp_f.name)


def mapper_mount_path(mapper_name: str) -> pathlib.Path:
    return pathlib.Path("/dev/mapper") / mapper_name


def open_storage(
    host_dev_path: pathlib.Path,
    key_data: bytes,
    mapper_name: str,
) -> pathlib.Path:
    """
    Decrypts a storage and mounts it

    :param host_dev_path: The dev containing encrypted data
    :param key_data:  The encryption key in b64 format
    :param mapper_name:  The name to give the device.  This is expected to be a safe name to use
                         and has been validated upstream.
    :return: The path to the mapper device
    """
    logger.info("Opening %s.", host_dev_path)
    with temporary_key_file(key_data) as key_path:
        results = subprocess.run(
            [
                "/sbin/cryptsetup",
                "-q",
                "--key-file",
                str(key_path),
                "open",
                str(host_dev_path),
                mapper_name,
            ],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=False,
        )
        if results.stderr:
            logger.debug("cryptsetup stderr: %s", results.stderr.decode())
        if results.stdout:
            logger.debug("cryptsetup stdout: %s", results.stdout.decode())
        if results.returncode:
            logger.warning(
                "Unable to open %s with exit status %i.  Message: %s",
                host_dev_path,
                results.returncode,
                results.stderr.decode(),
            )
            raise RuntimeError(f"LUKS Open Failed for {host_dev_path}")
    mapper = mapper_mount_path(mapper_name)
    if not mapper.exists():
        raise RuntimeError(f"Can not find expected mapper location {mapper}")
    else:
        return mapper


def close_storage(
    mapper_name: str,
):
    """
    Closes the encrypted storage
    """
    subprocess.run(
        ["/sbin/cryptsetup", "close", mapper_name],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        check=True,
    )


def prepare_encrypted_storage(
    host_dev_path: pathlib.Path, key_data: bytes, fill_with_random_data=True
):
    """
    Note:  Frandom seems to be deprecated by many because of the use
    of RC4.  Frandom is what I used to use for large disks.

    https://wiki.archlinux.org/title/frandom
    https://wiki.archlinux.org/title/Securely_wipe_disk#Overwrite_the_target
    https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#2-setup

    Note that on my desktop urandom generates data at about 60 MB/s (Ryzen) while
    my NAS (an older i5) generates at about 276 MB/s
    """
    if fill_with_random_data:
        logger.info("Filling %s with random data.", host_dev_path)
        results = subprocess.run(
            ["dd", "if=/dev/urandom", f"of={host_dev_path}", "bs=4096"],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=False,
        )
        dd_err = results.stderr.decode()
        if dd_err:
            logger.debug("dd stderr: %s", dd_err)
        if results.stdout:
            logger.debug("dd stdout: %s", results.stdout.decode())
        if results.returncode == 1 and "No space left on device" in dd_err:
            logger.debug("dd has successfully filled up %s", host_dev_path)
        elif results.returncode:
            logger.warning(
                "Unable to prepare %s with dd exit status %i",
                host_dev_path,
                results.returncode,
            )
            raise RuntimeError("DD Failed to prepare disk")
    else:
        logger.info(
            "Caution: %s is not being initialised with random data.", host_dev_path
        )

    logger.info("Creating dm-crypt partition %s.", host_dev_path)
    with temporary_key_file(key_data) as key_path:
        results = subprocess.run(
            ["cryptsetup", "-q", "luksFormat", str(host_dev_path), str(key_path)],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=False,
        )
        if results.stderr:
            logger.info("cryptsetup stderr: %s", results.stderr.decode())
        if results.stdout:
            logger.debug("cryptsetup stdout: %s", results.stdout.decode())
        if results.returncode:
            logger.warning(
                "Unable to LUKS Format %s with exit status %i",
                host_dev_path,
                results.returncode,
            )
            raise RuntimeError("LUKS Format Failed to prepare disk")
