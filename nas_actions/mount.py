# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import pathlib
import subprocess


def umount(path: pathlib.Path):
    """
    Unmounts the device.  Exceptions will be raised if the device
    can not be found or is not mounted.
    """
    try:
        subprocess.run(
            ["umount", "-A", str(path)],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=True,
        )
    except subprocess.CalledProcessError as e:
        if "not mounted" in e.stderr.decode():
            pass
        else:
            raise


def mount(dev_path: pathlib.Path, root_path: pathlib.Path, ro=False):
    """
    Mounts the device, raises an error if it is already mounted
    """
    if root_path.is_mount():
        raise RuntimeError(f"Path already mounted: {root_path}")
    if ro:
        cmd = ["mount", "-o", "ro", str(dev_path), str(root_path)]
    else:
        cmd = ["mount", str(dev_path), str(root_path)]
    subprocess.run(
        cmd,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        check=True,
    )


def eject(dev_path: pathlib.Path):
    """
    Tries to eject the device
    """
    # noinspection SpellCheckingInspection
    subprocess.run(
        ["udisksctl", "power-off", "-b", str(dev_path)],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        check=True,
    )
