# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import re
import subprocess
from functools import cache
from typing import List, NamedTuple, Optional, Any
import pathlib

try:
    from nas_actions.encryption import (
        prepare_encrypted_storage,
        generate_key,
        open_storage,
    )
except ImportError:
    from encryption import prepare_encrypted_storage, generate_key, open_storage

logger = logging.getLogger(__name__)

RE_SAFE_LABEL = re.compile(r"^[A-Za-z0-9_]+$")


class PartTable(NamedTuple):
    disk: dict
    partitions: List[dict]
    free_space: List[dict]


class StorageTemplate:
    """
    This is the base class for encapsulating storage format templates
    that can be applied to bare metal disks.
    """

    DISK_ALIGNMENT_BYTES = 1024**2

    TEMPLATE_FLAT_EXT4 = "flat_ext4"
    TEMPLATE_FLAT_LUKS = "flat_luks"
    TEMPLATE_LUKS_PARTITION = "partition_luks"

    FIELD_PARTITION_TYPE = "partition_type"
    FIELD_FLAGS = "flags"
    FIELD_FILE_SYSTEM = "file_system"
    FIELD_START = "start"
    FIELD_END = "end"
    FIELD_NUMBER = "number"

    FS_TYPE_EXT4 = "ext4"
    FS_TYPE_FREE_SPACE = "free"

    requires_key = False
    validate_partition_table = True

    def __init__(self, dev_path: pathlib.Path, device_labels: List[str]):
        self.device_labels = device_labels
        self.dev_path = dev_path
        self.pre_validate()

    def run_parted(self, main_cmd: str, *args):
        results = subprocess.run(
            ["parted", "-s", "-m", str(self.dev_path), main_cmd, *args],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=True,
        )
        return results

    @staticmethod
    def run_format_partition(dev_path: str, label: str):
        logger.warning("Formatting device %s as a ext4.", dev_path)
        # noinspection SpellCheckingInspection
        results = subprocess.run(
            ["mkfs.ext4", "-q", "-F", "-L", label, dev_path],
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            check=True,
        )
        return results

    @classmethod
    def disk_row_to_dict(cls, row: str) -> dict:
        """
        Example:
        /dev/sda:256GB:scsi:512:512:gpt:ATA Samsung SSD 850:;
        """
        row = row.strip(";").split(":")
        return {
            "dev": row[0],
            "size": row[1],
            "bus": row[2],
            "sector_logical": row[3],
            "sector_physical": row[4],
            cls.FIELD_PARTITION_TYPE: row[5],
            "model": row[6],
        }

    @classmethod
    def part_row_to_dict(cls, row: str) -> dict:
        """
        Example:
        5:1049kB:211MB:210MB:fat16:EFI System Partition:boot, esp;
        1:34s:488374238s:488374205s:free;
        """
        row = row.strip(";").split(":")
        if len(row) == 5:
            return {
                cls.FIELD_NUMBER: row[0],
                cls.FIELD_START: row[1],
                cls.FIELD_END: row[2],
                "size": row[3],
                cls.FIELD_FILE_SYSTEM: row[4],
                "name": None,
                cls.FIELD_FLAGS: None,
            }
        elif len(row) == 7:
            return {
                cls.FIELD_NUMBER: row[0],
                cls.FIELD_START: row[1],
                cls.FIELD_END: row[2],
                "size": row[3],
                cls.FIELD_FILE_SYSTEM: row[4],
                "name": row[5],
                cls.FIELD_FLAGS: row[6],
            }
        else:
            raise NotImplementedError(row)

    @cache
    def get_partition_table(self) -> PartTable:
        results = [
            row
            for row in self.run_parted("unit", "B", "print", "free")
            .stdout.decode()
            .split("\n")
            if row
        ]
        assert results[0] == "BYT;"
        disk = self.disk_row_to_dict(results[1])
        parts = [self.part_row_to_dict(row) for row in results[2:]]
        return PartTable(
            disk=disk,
            partitions=[
                row
                for row in parts
                if self.FS_TYPE_FREE_SPACE != row[self.FIELD_FILE_SYSTEM]
            ],
            free_space=[
                row
                for row in parts
                if self.FS_TYPE_FREE_SPACE == row[self.FIELD_FILE_SYSTEM]
            ],
        )

    def get_partition_table_type(self) -> str:
        t = self.get_partition_table()
        return t.disk[self.FIELD_PARTITION_TYPE]

    def pre_validate(self):
        """
        Ensures that this is possibly a safe operation by checking the
        target device:
        * exists
        * Already has a GPT partition table
        * There are no defined partitions

        As well as if the labels are safe.
        """
        for label in self.device_labels:
            if not RE_SAFE_LABEL.match(label):
                raise RuntimeError(f"Bad Label {label}.")

        if not self.dev_path.exists():
            raise RuntimeError(f"Device {self.dev_path} does not exist.")

        if self.validate_partition_table:
            part_type = self.get_partition_table_type()
            if part_type != "gpt":
                raise RuntimeError(
                    f"Device {self.dev_path} must be prepared with an "
                    f"empty gpt partition table ({part_type})."
                )

            if self.get_partition_table()[1]:
                raise RuntimeError(
                    f"Device {self.dev_path} does not contain an empty partition table."
                )

    def get_most_recent_partition_number(self) -> str:
        """
        The most recent partition is assumed to be the highest partition number,
        this seems to work okay.
        """
        t = self.get_partition_table()
        return max([x[self.FIELD_NUMBER] for x in t.partitions])

    def get_device_for_partition(self, partition_number: str) -> pathlib.Path:
        possible_paths = [
            p
            for p in self.dev_path.parent.glob(f"{self.dev_path.name}*")
            if str(p).endswith(partition_number)
        ]
        assert len(possible_paths) == 1, possible_paths
        return possible_paths[0]

    @classmethod
    def align_bytes(cls, pos: str, next_val=True) -> str:
        """
        :param pos: The position string from parted
        :param next_val: True means get a larger aligned value, false means smaller.
        """
        assert pos[-1] == "B", "Only BYTE units are supported"
        pos_bytes = int(pos[:-1])
        pos_alignment_units = pos_bytes // cls.DISK_ALIGNMENT_BYTES
        if next_val:
            pos_alignment_units += 1
        return f"{pos_alignment_units * cls.DISK_ALIGNMENT_BYTES}B"

    def create_partition_remaining_space(
        self, label: str, fs_type: Optional[str]
    ) -> pathlib.Path:
        """
        Creates a partition using all remaining space and returns the partition path.
        Note that alignment of partitions is assumed to occur automatically:

        https://wiki.archlinux.org/title/partitioning#Partition_alignment

        It turns out this is used for soft units, not hard units:
        https://unix.stackexchange.com/questions/38164/create-partition-aligned-using-parted
        """
        t = self.get_partition_table()
        start = self.align_bytes(t.free_space[-1][self.FIELD_START], next_val=True)
        end = self.align_bytes(t.free_space[-1][self.FIELD_END], next_val=False)

        logger.warning(
            "Making remaining partition in %s: %s, %s, %s",
            self.dev_path,
            start,
            end,
            fs_type,
        )
        if fs_type:
            # noinspection SpellCheckingInspection
            self.run_parted("mkpart", label, fs_type, start, end)
        else:
            # noinspection SpellCheckingInspection
            self.run_parted("mkpart", label, start, end)
        self.clear_caches()

        part_number = self.get_most_recent_partition_number()
        # This will raise an error if there is an alignment issue.
        try:
            self.run_parted("align-check", "optimal", str(part_number))
        except subprocess.CalledProcessError:
            msg = f"Partitioning failed to produce an optimal alignment for {self.dev_path}"
            raise RuntimeError(msg)

        return self.get_device_for_partition(part_number)

    def clear_caches(self):
        self.get_partition_table.cache_clear()

    def apply(self, *_):
        raise NotImplementedError


class FlatExt4(StorageTemplate):
    def apply(self):
        logger.warning("Formatting device %s as a Flat ext4 Template.", self.dev_path)
        part_path = self.create_partition_remaining_space(
            label=self.device_labels[0], fs_type=self.FS_TYPE_EXT4
        )
        assert part_path.exists()
        self.run_format_partition(str(part_path), self.device_labels[0])
        return self.device_labels[0]


class FlatLuks(StorageTemplate):
    requires_key = True

    def apply(self, key: bytes) -> str:
        logger.warning(
            "Formatting device %s as a Flat LUKS ext4 Template.", self.dev_path
        )
        mapper_name = self.device_labels[0]
        part_path = self.create_partition_remaining_space(
            label=mapper_name, fs_type=None
        )
        assert part_path.exists()
        prepare_encrypted_storage(part_path, key)
        map_path = open_storage(part_path, key, mapper_name)
        self.run_format_partition(str(map_path), mapper_name)
        return mapper_name


class PartitionLuks(FlatLuks):
    requires_key = True
    validate_partition_table = False

    def apply(self, key: bytes) -> str:
        logger.warning(
            "Formatting device %s as a Flat LUKS ext4 Template.", self.dev_path
        )
        mapper_name = self.device_labels[0]
        skip_random_fill = "skip_fill" in self.device_labels
        prepare_encrypted_storage(
            self.dev_path, key, fill_with_random_data=not skip_random_fill
        )
        map_path = open_storage(self.dev_path, key, mapper_name)
        self.run_format_partition(str(map_path), mapper_name)
        return mapper_name


STORAGE_FORMATS = {
    StorageTemplate.TEMPLATE_FLAT_LUKS: FlatLuks,
    StorageTemplate.TEMPLATE_FLAT_EXT4: FlatExt4,
    StorageTemplate.TEMPLATE_LUKS_PARTITION: PartitionLuks,
}


def storage_requires_key(format_type: str) -> bool:
    return STORAGE_FORMATS[format_type].requires_key


def apply_storage_format(
    dev_path: pathlib.Path,
    format_type: str,
    partition_labels: List[str],
    key: Optional[bytes] = None,
) -> Optional[Any]:
    if format_type in STORAGE_FORMATS:
        cls = STORAGE_FORMATS[format_type]
    else:
        raise NotImplementedError(f"Unknown Format Template Type {format_type}")

    template = cls(dev_path, partition_labels)
    if cls.requires_key:
        ret = template.apply(key)
    else:
        ret = template.apply()
    return ret


if __name__ == "__main__":

    def main():
        logging.basicConfig(level=logging.DEBUG)
        apply_storage_format(
            pathlib.Path("/dev/sdc"), StorageTemplate.TEMPLATE_FLAT_LUKS, ["trial1234"]
        )

    main()
