# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This package is for encapsulating actions that can be performed
programmatically to the NAS.
"""
from nas_actions.storage_template import (
    StorageTemplate,
    apply_storage_format,
    storage_requires_key,
)

from nas_actions.mount import mount, umount, eject
from nas_actions.encryption import (
    open_storage,
    close_storage,
    mapper_mount_path,
    generate_key,
)

__all__ = [
    "StorageTemplate",
    "apply_storage_format",
    "storage_requires_key",
    "open_storage",
    "mount",
    "umount",
    "close_storage",
    "mapper_mount_path",
    "eject",
    "generate_key",
]
