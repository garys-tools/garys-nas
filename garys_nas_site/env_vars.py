# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
"""
This files handles environment variable processing.
"""
import os
import pathlib

env_var_path = pathlib.Path(__file__).parent.parent / "conf" / "env.vars"

# Directly process env.vars if they have not been loaded
if "SECRET_KEY" not in os.environ and env_var_path.exists():
    import re

    val_match = re.compile(
        r"^export (?P<key>[A-Z_]+)=\${[A-Z_]+:-(?P<val>.*)}", re.MULTILINE
    )
    with env_var_path.open("r") as f:
        text = f.read()

    _vars = {x["key"]: x["val"] for x in val_match.finditer(text)}
    os.environ.update(_vars)

# Paths are processed in settings_paths.py
if "SECRET_KEY" in os.environ:
    v = os.environ.get("SECRET_KEY")
    if v[0] == "/" or v[:2] == "./":
        with open(v) as f:
            SECRET_KEY = f.read().strip()
    else:
        SECRET_KEY = v
else:
    from django.core.management.utils import get_random_secret_key

    SECRET_KEY = get_random_secret_key()

HOST_NAME = os.environ.get("HOST_NAME")
LOG_LEVEL = os.environ.get("NAS_LOG_LEVEL", "INFO")
DEBUG = os.environ.get("NAS_DEBUG", False) in ("TRUE", "True", "true", "1")
REMOTE_DEBUG = os.environ.get("NAS_REMOTE_DEBUG", False) in (
    "TRUE",
    "True",
    "true",
    "1",
)

PSQL_HOST = False
PSQL_NAME = False
PSQL_USER = False
PSQL_PASSWORD = False
PSQL_PORT = 0

if REMOTE_DEBUG:
    import pydevd_pycharm

    pydevd_pycharm.settrace(
        "localhost", port=51234, stdoutToServer=True, stderrToServer=True, suspend=True
    )
