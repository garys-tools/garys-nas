# Copyright: (c) 2021, Gary Thompson <coding@garythompson.id.au>
# SPDX-License-Identifier: MIT
import os
from pathlib import Path

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = Path(__file__).resolve().parent.parent


SQLITE_DB = Path(os.environ.get("SQLITE_DB", BASE_DIR / "db.sqlite3"))
LOG_PATH = Path(os.environ.get("LOG_PATH", BASE_DIR / "var" / "log"))
STATIC_ROOT = Path(os.environ.get("STATIC_ROOT", BASE_DIR / "static"))
